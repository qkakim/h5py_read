import os
import sys
import glob

import subprocess
import threading

'''Input run Number'''
var1 = sys.argv[1]
runNum = var1
#path = '/data/AMoRE/users/sckim/RODY/AMoRE_ADC/RAW/2019{0}/'.format(runNum)
path = '/data/AMoRE/users/kimwootae/work/version_test/test_data/'
#prodfiles = path+'*light*{0}*root*'.format(var1)
prodfiles = path+'*heat*{0}*root*'.format(var1)

exepath = "/data/AMoRE/users/kimwootae/work/version_test/unpack_v2/prod_v2.0.0/Shell/"

def MakebashSh(path, subrunNum) :
    shName = '{0}'.format(runNum)
    shName = exepath+'prod{0}_{1:05d}.sh'.format(shName,subrunNum)
    input_file = path+'heat_{0}_{1:05d}.root'.format(runNum, subrunNum)
    print (input_file)
    with open(shName, 'w') as rsh :
        rsh.write('''\
##!/bin/bash
    
path={0}
subrunNum={1}
source /share/amore/anaconda3/etc/profile.d/conda.sh
source /share/amore/ROOT/6.14.04-v01.01/bin/thisroot.sh

conda activate v01
    
work_dir=/data/AMoRE/users/kimwootae/work/version_test/unpack_v2/prod_v2.0.0/
executable_name="prod_v2.py"
log_file=/data/AMoRE/users/kimwootae/work/version_test/unpack_v2/prod_v2.0.0./LOGS/test_{2}.log
python /data/AMoRE/users/kimwootae/work/version_test/unpack_v2/prod_v2.0.0/prod_v2.py {0} {1}

'''.format(input_file, subrunNum, subrunNum))
    return shName

'''run bash script of QJOB'''
def JobShell(execute) :
    #subprocess.Popen(["python", "{0}".format(execute), "{0}".format(path), "{0}".format(subrunNum)])
    subprocess.Popen(["qsub -q very_short {0}".format(execute)],shell=True)

'''threading function for parallel running script'''
def Threading(execute) :
    my_thread = threading.Thread(target=JobShell(execute))
    my_thread.start()

'''Sorting the files'''
def SortingFile(gzfiles) :
    files = sorted(glob.glob(gzfiles), key=os.path.getmtime)
    n = len(files)
    infiles = []
    tfileGrup = []
    i = 0
    for filename in files :
        infiles.append(filename)
        if(len(infiles) == 50) :
            i+=1
            tfileGrup.append(infiles)
            infiles = []
        elif((len(files)//50) == i and (len(files)%50 == len(infiles))) :
            tfileGrup.append(infiles)
    return tfileGrup

'''running function'''
def run() :
    files = sorted(glob.glob(prodfiles))
    n = len(files)
    print (files)
    shell_array = []
    for i in range(len(files)) :
        a = MakebashSh(path, i)
        shell_array.append(a)
    
    for i in range(len(files)) :
        Threading(shell_array[i])
    
    print (shell_array)    
if __name__ == "__main__" :
    run()
