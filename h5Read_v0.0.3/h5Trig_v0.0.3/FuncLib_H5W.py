import numpy as np
import h5py, os, glob, datetime, time, subprocess, threading, sys
import pandas as pd

zero = np.array([0], dtype=np.bool_)

'''Sort files'''


def SORTINGFILE(n_files, mergeN):
    # files = sorted(glob.glob(n_files), key=os.path.getmtime)
    files = sorted(glob.glob(n_files))
    n = len(files)
    infiles = []
    tfileGrup = []
    i = 0
    for filename in files:
        infiles.append(filename)
        if (len(infiles) == mergeN):
            i += 1
            tfileGrup.append(infiles)
            infiles = []
        elif ((len(files) // mergeN) == i and (len(files) % mergeN == len(infiles))):
            tfileGrup.append(infiles)
    return tfileGrup


'''Automatically find file name'''


def FILE_NAME(path):
    files = sorted(glob.glob(path))
    return files[0][:-5]


def init_pd(nch):
    zero = np.array([0], dtype=np.bool_)

    pd_heat = [pd.DataFrame({'time': zero, 'xhpeak': zero, 'hpeak': zero, 'npeaks': zero
                                , 'hrt90': zero, 'hrt70': zero, 'hrt50': zero, 'hrt30': zero, 'hrt20': zero,
                             'hrt15': zero, 'hrt10': zero, 'hrt5': zero
                                , 'hrt90_h': zero, 'hrt70_h': zero, 'hrt50_h': zero, 'hrt30_h': zero, 'hrt20_h': zero,
                             'hrt15_h': zero, 'hrt10_h': zero, 'hrt5_h': zero
                                , 'hdt90': zero, 'hdt70': zero, 'hdt50': zero, 'hdt30': zero, 'hdt10': zero
                                , 'hdt90_h': zero, 'hdt70_h': zero, 'hdt50_h': zero, 'hdt30_h': zero, 'hdt10_h': zero
                                , 'hpedestal': zero, 'hendline': zero, 'hendlineRMS': zero
                                , 'hbaseline': zero, 'hbaselineRMS': zero
                                , 'qsum90': zero, 'qsum70': zero, 'qsum50': zero, 'qsum30': zero, 'qsum20': zero,
                             'qsum15': zero, 'qsum10': zero, 'qsum5': zero
                                , 'qsum100': zero, 'qsum190': zero, 'qsum170': zero, 'qsum150': zero, 'qsum130': zero
                                , 'xhfpeak1': zero, 'hfpeak1': zero, 'hfmin1': zero, 'xhfmin1': zero,
                             'hfbaseline1': zero, 'hfbaselineRMS1': zero, 'nhfpeaks1': zero, 'htemp1_Slp': zero,
                             'htemp1_err': zero
                                , 'xhfpeak2': zero, 'hfpeak2': zero, 'hfmin2': zero, 'xhfmin2': zero,
                             'hfbaseline2': zero, 'hfbaselineRMS2': zero, 'nhfpeaks2': zero, 'htemp2_Slp': zero,
                             'htemp2_err': zero
                                , 'xhfpeak3': zero, 'hfpeak3': zero, 'hfmin3': zero, 'xhfmin3': zero,
                             'hfbaseline3': zero, 'hfbaselineRMS3': zero, 'nhfpeaks3': zero, 'htemp3_Slp': zero,
                             'htemp3_err': zero
                                , 'subrunNum': zero, 'n_arr': zero, 'n_arr_x': zero, 'nn': zero
                                , 'amp_OF1': zero, 'amp_OF2': zero, 'amp_OF3': zero, 'oft_amp': zero, 'ofte_amp': zero})
               for _ in range(nch)]

    pd_light = [pd.DataFrame({'time': zero, 'xlpeak': zero, 'lpeak': zero
                                 , 'lrt_90': zero, 'lrt_50': zero, 'lrt_10': zero
                                 , 'lrt90_x': zero, 'lrt50_x': zero, 'lrt10_x': zero
                                 , 'lpedestal': zero, 'lendline': zero, 'lendlineRMS': zero
                                 , 'lbaseline': zero, 'lbaselineRMS': zero
                                 , 'xlfpeak1': zero, 'lfpeak1': zero, 'xlfmin1': zero, 'lfmin1': zero,
                              'lfbaseline1': zero, 'lfbaselineRMS1': zero, 'ltemp1_Slp': zero, 'ltemp1_err': zero
                                 , 'xlfpeak2': zero, 'lfpeak2': zero, 'xlfmin2': zero, 'lfmin2': zero,
                              'lfbaseline2': zero, 'lfbaselineRMS2': zero, 'ltemp2_Slp': zero, 'ltemp2_err': zero
                                 , 'xlfpeak3': zero, 'lfpeak3': zero, 'xlfmin3': zero, 'lfmin3': zero,
                              'lfbaseline3': zero, 'lfbaselineRMS3': zero, 'ltemp3_Slp': zero, 'ltemp3_err': zero}) for
                _ in range(nch)]
    return pd_heat, pd_light


def write_pd(t, h_ped, h_base, h_rms, l_base, l_rms, h_ana, h_band_ana1, h_band_ana2, h_band_ana3, h_tmplt1, h_tmplt2,
             h_tmplt3, l_ana, l_band_ana1, l_band_ana2, l_band_ana3, l_tmplt1, l_tmplt2, l_tmplt3, subrunNum, n_arr, n_arr_x,
             nn, amp_OF, oft_amp):

    time = np.array([t])
    subrunNum = np.array([subrunNum], dtype=np.uint16)
    n_arr = np.array([n_arr], dtype=np.uint16)
    n_arr_x = np.array([n_arr_x], dtype=np.uint32)
    nn = np.array([nn], dtype=np.uint16)

    """Re-definition of heat channel parameter for reduce memory"""
    xhpeak = np.array(h_ana[0], dtype=np.float)
    hpeak = np.array(h_ana[1], dtype=np.float)
    npeaks = np.array(h_ana[2], dtype=np.uint8)
    hrt = np.array(h_ana[3])
    rt90 = np.array(hrt[0], dtype=np.float)
    rt70 = np.array(hrt[1], dtype=np.float)
    rt50 = np.array(hrt[2], dtype=np.float)
    rt30 = np.array(hrt[3], dtype=np.float)
    rt20 = np.array(hrt[4], dtype=np.float)
    rt15 = np.array(hrt[5], dtype=np.float)
    rt10 = np.array(hrt[6], dtype=np.float)
    rt5 = np.array(hrt[7], dtype=np.float)
    hrt_h = np.array(h_ana[4])
    rt90_h = np.array(hrt_h[0], dtype=np.float)
    rt70_h = np.array(hrt_h[1], dtype=np.float)
    rt50_h = np.array(hrt_h[2], dtype=np.float)
    rt30_h = np.array(hrt_h[3], dtype=np.float)
    rt20_h = np.array(hrt_h[4], dtype=np.float)
    rt15_h = np.array(hrt_h[5], dtype=np.float)
    rt10_h = np.array(hrt_h[6], dtype=np.float)
    rt5_h = np.array(hrt_h[7], dtype=np.float)
    hdt = np.array(h_ana[5])
    hdt90 = np.array(hdt[0], dtype=np.float)
    hdt70 = np.array(hdt[1], dtype=np.float)
    hdt50 = np.array(hdt[2], dtype=np.float)
    hdt30 = np.array(hdt[3], dtype=np.float)
    hdt10 = np.array(hdt[3], dtype=np.float)
    hdt_h = np.array(h_ana[6])
    hdt90_h = np.array(hdt_h[0], dtype=np.float)
    hdt70_h = np.array(hdt_h[1], dtype=np.float)
    hdt50_h = np.array(hdt_h[2], dtype=np.float)
    hdt30_h = np.array(hdt_h[3], dtype=np.float)
    hdt10_h = np.array(hdt_h[3], dtype=np.float)
    qsum = np.array(h_ana[7])
    qsum5 = np.array(qsum[0], dtype=np.float32)
    qsum10 = np.array(qsum[1], dtype=np.float32)
    qsum15 = np.array(qsum[2], dtype=np.float32)
    qsum20 = np.array(qsum[3], dtype=np.float32)
    qsum30 = np.array(qsum[4], dtype=np.float32)
    qsum50 = np.array(qsum[5], dtype=np.float32)
    qsum70 = np.array(qsum[6], dtype=np.float32)
    qsum90 = np.array(qsum[7], dtype=np.float32)
    qsum100 = np.array(qsum[8], dtype=np.float32)
    qsum190 = np.array(qsum[9], dtype=np.float32)
    qsum170 = np.array(qsum[10], dtype=np.float32)
    qsum150 = np.array(qsum[11], dtype=np.float32)
    qsum130 = np.array(qsum[12], dtype=np.float32)
    h_base = np.array(h_ana[8], dtype=np.float32)
    h_baseRMS = np.array(h_ana[9], dtype=np.float32)
    h_end = np.array(h_ana[10], dtype=np.float32)
    h_endRMS = np.array(h_ana[11], dtype=np.float32)
    h_ped = np.array(h_ana[12], dtype=np.float32)

    """Re-definition of heat channel filter parameter for reduce memory"""
    xhfmax1 = np.array(h_band_ana1[0], dtype=np.float)
    hfmax1 = np.array(h_band_ana1[1], dtype=np.float)
    xhfmin1 = np.array(h_band_ana1[2], dtype=np.float)
    hfmin1 = np.array(h_band_ana1[3], dtype=np.float)
    nhfpeaks1 = np.array(h_band_ana1[4], dtype=np.uint8)
    hfbase1 = np.array(h_band_ana1[7], dtype=np.float)
    hfbaseRMS1 = np.array(h_band_ana1[8], dtype=np.float)
    htemp1_Slp = np.array(h_tmplt1[0], dtype=np.float)
    htemp1_err = np.array(h_tmplt1[-1], dtype=np.float)

    xhfmax2 = np.array(h_band_ana2[0], dtype=np.float)
    hfmax2 = np.array(h_band_ana2[1], dtype=np.float)
    xhfmin2 = np.array(h_band_ana2[2], dtype=np.float)
    hfmin2 = np.array(h_band_ana2[3], dtype=np.float)
    nhfpeaks2 = np.array(h_band_ana2[4], dtype=np.uint8)
    hfbase2 = np.array(h_band_ana2[7], dtype=np.float)
    hfbaseRMS2 = np.array(h_band_ana2[8], dtype=np.float)
    htemp2_Slp = np.array(h_tmplt2[0], dtype=np.float)
    htemp2_err = np.array(h_tmplt2[-1], dtype=np.float)

    xhfmax3 = np.array(h_band_ana3[0], dtype=np.float)
    hfmax3 = np.array(h_band_ana3[1], dtype=np.float)
    xhfmin3 = np.array(h_band_ana3[2], dtype=np.float)
    hfmin3 = np.array(h_band_ana3[3], dtype=np.float)
    nhfpeaks3 = np.array(h_band_ana3[4], dtype=np.uint8)
    hfbase3 = np.array(h_band_ana3[7], dtype=np.float)
    hfbaseRMS3 = np.array(h_band_ana3[8], dtype=np.float)
    htemp3_Slp = np.array(h_tmplt3[0], dtype=np.float)
    htemp3_err = np.array(h_tmplt3[-1], dtype=np.float)

    """Re-definition of light channel parameter for reduce memory"""
    xlpeak = np.array([l_ana[0]], dtype=np.float)
    lpeak = np.array([l_ana[1]], dtype=np.float)
    lrt = np.array(l_ana[2])
    lrt90 = np.array(lrt[0], dtype=np.float)
    lrt50 = np.array(lrt[1], dtype=np.float)
    lrt10 = np.array(lrt[2], dtype=np.float)
    lrt_h = np.array(l_ana[3])
    lrt90_h = np.array(lrt_h[0], dtype=np.float)
    lrt50_h = np.array(lrt_h[1], dtype=np.float)
    lrt10_h = np.array(lrt_h[2], dtype=np.float)

    """Re-definition of light channel filter parameter for reduce memory"""
    xlfmax1 = np.array(l_band_ana1[0], dtype=np.float)
    lfmax1 = np.array(l_band_ana1[1], dtype=np.float)
    xlfmin1 = np.array(l_band_ana1[2], dtype=np.float)
    lfmin1 = np.array(l_band_ana1[3], dtype=np.float)
    lfbase1 = np.array(l_band_ana1[7], dtype=np.float)
    lfbaseRMS1 = np.array(l_band_ana1[8], dtype=np.float)
    ltemp1_Slp = np.array(l_tmplt1[0], dtype=np.float)
    ltemp1_err = np.array(l_tmplt1[-1], dtype=np.float)

    xlfmax2 = np.array(l_band_ana2[0], dtype=np.float)
    lfmax2 = np.array(l_band_ana2[1], dtype=np.float)
    xlfmin2 = np.array(l_band_ana2[2], dtype=np.float)
    lfmin2 = np.array(l_band_ana2[3], dtype=np.float)
    lfbase2 = np.array(l_band_ana2[7], dtype=np.float)
    lfbaseRMS2 = np.array(l_band_ana2[8], dtype=np.float)
    ltemp2_Slp = np.array(l_tmplt2[0], dtype=np.float)
    ltemp2_err = np.array(l_tmplt2[-1], dtype=np.float)

    xlfmax3 = np.array(l_band_ana3[0], dtype=np.float)
    lfmax3 = np.array(l_band_ana3[1], dtype=np.float)
    xlfmin3 = np.array(l_band_ana3[2], dtype=np.float)
    lfmin3 = np.array(l_band_ana3[3], dtype=np.float)
    lfbase3 = np.array(l_band_ana3[7], dtype=np.float)
    lfbaseRMS3 = np.array(l_band_ana3[8], dtype=np.float)
    ltemp3_Slp = np.array(l_tmplt3[0], dtype=np.float)
    ltemp3_err = np.array(l_tmplt3[-1], dtype=np.float)

    """Re-definition of optimal filter result parameter"""
    amp_OF1 = np.array(amp_OF[0], dtype=np.float)
    amp_OF2 = np.array(amp_OF[1], dtype=np.float)
    amp_OF3 = np.array(amp_OF[2], dtype=np.float)

    oft_amp1 = np.array(oft_amp[0], dtype=np.float)
    oft_amp2 = np.array(oft_amp[1], dtype=np.float)

    pd_h_ana = pd.DataFrame({'time': time, 'xhpeak': xhpeak, 'hpeak': hpeak, 'npeaks': npeaks
                                , 'hrt90': rt90, 'hrt70': rt70, 'hrt50': rt50, 'hrt30': rt30, 'hrt20': rt20,
                             'hrt15': rt15, 'hrt10': rt10, 'hrt5': rt5
                                , 'hrt90_h': rt90_h, 'hrt70_h': rt70_h, 'hrt50_h': rt50_h, 'hrt30_h': rt30_h,
                             'hrt20_h': rt20_h, 'hrt15_h': rt15_h, 'hrt10_h': rt10_h, 'hrt5_h': rt5_h
                                , 'hdt90': hdt90, 'hdt70': hdt70, 'hdt50': hdt50, 'hdt30': hdt30, 'hdt10': hdt10
                                , 'hdt90_h': hdt90_h, 'hdt70_h': hdt70_h, 'hdt50_h': hdt50_h, 'hdt30_h': hdt30_h,
                             'hdt10_h': hdt10_h
                                , 'hpedestal': h_ped, 'hendline': h_end, 'hendlineRMS': h_endRMS
                                , 'hbaseline': h_base, 'hbaselineRMS': h_baseRMS
                                , 'qsum90': qsum90, 'qsum70': qsum70, 'qsum50': qsum50, 'qsum30': qsum30,
                             'qsum20': qsum20, 'qsum15': qsum15, 'qsum10': qsum10, 'qsum5': qsum5
                                , 'qsum100': qsum100, 'qsum190': qsum190, 'qsum170': qsum170, 'qsum150': qsum150,
                             'qsum130': qsum130
                                , 'xhfpeak1': xhfmax1, 'hfpeak1': hfmax1, 'hfmin1': hfmin1, 'xhfmin1': xhfmin1,
                             'nhfpeaks1': nhfpeaks1, 'hfbaseline1': hfbase1, 'hfbaselineRMS1': hfbaseRMS1,
                             'htemp1_Slp': htemp1_Slp, 'htemp1_err': htemp1_err
                                , 'xhfpeak2': xhfmax2, 'hfpeak2': hfmax2, 'hfmin2': hfmin2, 'xhfmin2': xhfmin2,
                             'nhfpeaks2': nhfpeaks2, 'hfbaseline2': hfbase2, 'hfbaselineRMS2': hfbaseRMS2,
                             'htemp2_Slp': htemp2_Slp, 'htemp2_err': htemp2_err
                                , 'xhfpeak3': xhfmax3, 'hfpeak3': hfmax3, 'hfmin3': hfmin3, 'xhfmin3': xhfmin3,
                             'nhfpeaks3': nhfpeaks3, 'hfbaseline3': hfbase3, 'hfbaselineRMS3': hfbaseRMS3,
                             'htemp3_Slp': htemp3_Slp, 'htemp3_err': htemp3_err
                                , 'subrunNum': subrunNum, 'n_arr': n_arr, 'n_arr_x': n_arr_x, 'nn': nn
                                , 'amp_OF1': amp_OF1, 'amp_OF2': amp_OF2, 'amp_OF3': amp_OF3, 'oft_amp': oft_amp1,
                             'ofte_amp': oft_amp2})

    pd_l_ana = pd.DataFrame({'time': time, 'xlpeak': xlpeak, 'lpeak': lpeak
                                , 'lrt_90': lrt90, 'lrt_50': lrt50, 'lrt_10': lrt10, 'lrt90_h': lrt90_h,
                             'lrt50_h': lrt50_h, 'lrt10_h': lrt10_h
                                , 'lbaseline': l_base, 'lbaselineRMS': l_rms
                                , 'xlfpeak1': xlfmax1, 'lfpeak1': lfmax1, 'xlfmin1': xlfmin1, 'lfmin1': lfmin1,
                             'lfbaseline1': lfbase1, 'lfbaselineRMS1': lfbaseRMS1, 'ltemp1_Slp': ltemp1_Slp,
                             'ltemp1_err': ltemp1_err
                                , 'xlfpeak2': xlfmax2, 'lfpeak2': lfmax2, 'xlfmin2': xlfmin2, 'lfmin2': lfmin2,
                             'lfbaseline2': lfbase2, 'lfbaselineRMS2': lfbaseRMS2, 'ltemp2_Slp': ltemp2_Slp,
                             'ltemp2_err': ltemp2_err
                                , 'xlfpeak3': xlfmax3, 'lfpeak3': lfmax3, 'xlfmin3': xlfmin3, 'lfmin3': lfmin3,
                             'lfbaseline3': lfbase3, 'lfbaselineRMS3': lfbaseRMS3, 'ltemp3_Slp': ltemp3_Slp,
                             'ltemp3_err': ltemp3_err})

    return pd_h_ana, pd_l_ana


def bug_pd():
    zero = np.array([0], dtype=np.bool_)

    pd_heat = pd.DataFrame({'time': zero, 'xhpeak': zero, 'hpeak': zero, 'npeaks': zero
                               , 'hrt90': zero, 'hrt70': zero, 'hrt50': zero, 'hrt30': zero, 'hrt20': zero,
                            'hrt15': zero, 'hrt10': zero, 'hrt5': zero
                               , 'hrt90_h': zero, 'hrt70_h': zero, 'hrt50_h': zero, 'hrt30_h': zero, 'hrt20_h': zero,
                            'hrt15_h': zero, 'hrt10_h': zero, 'hrt5_h': zero
                               , 'hdt90': zero, 'hdt70': zero, 'hdt50': zero, 'hdt30': zero, 'hdt10': zero
                               , 'hdt90_h': zero, 'hdt70_h': zero, 'hdt50_h': zero, 'hdt30_h': zero, 'hdt10_h': zero
                               , 'hpedestal': zero, 'hendline': zero, 'hendlineRMS': zero
                               , 'hbaseline': zero, 'hbaselineRMS': zero
                               , 'qsum90': zero, 'qsum70': zero, 'qsum50': zero, 'qsum30': zero, 'qsum20': zero,
                            'qsum15': zero, 'qsum10': zero, 'qsum5': zero
                               , 'qsum100': zero, 'qsum190': zero, 'qsum170': zero, 'qsum150': zero, 'qsum130': zero
                               , 'xhfpeak1': zero, 'hfpeak1': zero, 'hfmin1': zero, 'hfbaseline1': zero,
                            'hfbaselineRMS1': zero, 'htemp1_Slp': zero, 'htemp1_err': zero
                               , 'xhfpeak2': zero, 'hfpeak2': zero, 'hfmin2': zero, 'hfbaseline2': zero,
                            'hfbaselineRMS2': zero, 'htemp2_Slp': zero, 'htemp2_err': zero
                               , 'xhfpeak3': zero, 'hfpeak3': zero, 'hfmin3': zero, 'hfbaseline3': zero,
                            'hfbaselineRMS3': zero, 'htemp3_Slp': zero, 'htemp3_err': zero
                               , 'subrunNum': zero, 'n_arr': zero, 'n_arr_x': zero, 'nn': zero
                               , 'amp_OF1': zero, 'amp_OF2': zero, 'amp_OF3': zero, 'oft_amp': zero, 'ofte_amp': zero})

    pd_light = pd.DataFrame({'time': zero, 'xlpeak': zero, 'lpeak': zero
                                , 'lrt_90': zero, 'lrt_50': zero, 'lrt_10': zero
                                , 'lrt90_x': zero, 'lrt50_x': zero, 'lrt10_x': zero
                                , 'lpedestal': zero, 'lendline': zero, 'lendlineRMS': zero
                                , 'lbaseline': zero, 'lbaselineRMS': zero
                                , 'xlfpeak1': zero, 'lfpeak1': zero, 'xlfmin1': zero, 'lfmin1': zero,
                             'lfbaseline1': zero, 'lfbaselineRMS1': zero, 'ltemp1_Slp': zero, 'ltemp1_err': zero
                                , 'xlfpeak2': zero, 'lfpeak2': zero, 'xlfmin2': zero, 'lfmin2': zero,
                             'lfbaseline2': zero, 'lfbaselineRMS2': zero, 'ltemp2_Slp': zero, 'ltemp2_err': zero
                                , 'xlfpeak3': zero, 'lfpeak3': zero, 'xlfmin3': zero, 'lfmin3': zero,
                             'lfbaseline3': zero, 'lfbaselineRMS3': zero, 'ltemp3_Slp': zero, 'ltemp3_err': zero})
    return pd_heat, pd_light


def PROD_save_pd(OUTPATH, runN, pd_heat, pd_light, cryst_name, ncrystal, subN):
    for i in range(ncrystal):
        pd_heat[i].to_hdf(OUTPATH + '{0}/PROD1_{1}_{2:03d}.h5'.format(runN, cryst_name, subN), key='heat', mode='w')
    for i in range(ncrystal):
        pd_light[i].to_hdf(OUTPATH + '{0}/PROD1_{1}_{2:03d}.h5'.format(runN, cryst_name, subN), key='light')

def TRG_save_pd(OUTPATH, runN, subrunNum, pd_heat, pd_light, ncrystal):
    for i in range(ncrystal):
        pd_heat[i].to_hdf(OUTPATH + '{0}/PROD_ch{1}_{2:05d}.h5'.format(runN, i, int(subrunNum)), key='heat', mode='w')
    for i in range(ncrystal):
        pd_light[i].to_hdf(OUTPATH + '{0}/PROD_ch{1}_{2:05d}.h5'.format(runN, i, int(subrunNum)), key='light')
