import numpy as np
import sys, os, glob, datetime, time, subprocess, threading, h5py

import FuncLib_H5W as func_h5w

var1 = sys.argv[1]
runN = var1

thr = 100
ncrystal = 4
mergeN = int(10)

'''Input Run Number'''
h5_path = '/mnt/lustre/ibs/cupamore/data/RODY/RAW/2022_1st_RODY/{0}/'.format(runN)
h5files = h5_path+'*h5*'
path = '/mnt/lustre/ibs/wootae/h5py_read/h5Read_v0.0.3/h5Trig_v0.0.3'
exepath = path+'/Shell/'
OUTPATH = '/mnt/lustre/ibs/wootae/h5py_read/h5Read_v0.0.3/h5Trig_v0.0.3/data/'

def MakeBashSh_v2(subrunNum) :
    shName = 'rody_{0}'.format(runN)
    shName = exepath+'{0}_{1}.sh'.format(shName, subrunNum)
    with open(shName, 'w') as rsh :
        rsh.write('''\
#!/bin/bash -f
#SBATCH -J rody_3rd_exp
#SBATCH -p jepyc
##SBATCH -N 10
##SBATCH -n 5
#SBATCH --mincpus=1

module load anaconda3/
/mnt/lustre/ibs/wootae/anaconda3/bin/python {0}/h5run.py {1} {2} {3} {4} {5} {6} {7}
'''.format(path, h5_path,subrunNum, runN, mergeN, thr, ncrystal, OUTPATH))
    return shName

'''run bash script of QJOB'''

def JobShell(execute) :
    subprocess.Popen(["sbatch {0}".format(execute)], shell=True)

def Threading(execute):
    my_thread = threading.Thread(target=JobShell(execute))
    my_thread.start()

def run():
    tfileGrup = func_h5w.SORTINGFILE(h5files, mergeN)
    print(h5files)
    shell_array = []
    for i in range(len(tfileGrup)):
        a = MakeBashSh_v2(i)
        shell_array.append(a)

    for i in range((len(shell_array))):
        Threading(shell_array[i])

    print(shell_array)

if __name__ == "__main__":
    run()