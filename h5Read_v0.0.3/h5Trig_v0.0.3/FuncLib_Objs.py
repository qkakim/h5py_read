import h5py
import numpy as np

from scipy.optimize import *
from scipy.signal import *
from scipy import stats
from scipy.integrate import odeint, simps

###ButterWorth bandpass filter###
def butter_bandpass(lowcut, highcut, fs, order=5) :
    nyq = 0.5*fs
    low = lowcut/nyq
    high = highcut/nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5) :
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def myfft2(ayy_tmp_ch3, sampling_rate, sampling_interval):
    n_sample = len(ayy_tmp_ch3)
    t = np.arange(0,n_sample*sampling_interval, sampling_interval)
    k = np.arange(n_sample)
    T = n_sample/sampling_rate
    frq_bin = 1/T
    frq = k/T
    frq = frq[:n_sample//2]
    Yr = np.fft.fft(ayy_tmp_ch3)
    Y = Yr/n_sample
    Y = Y[:n_sample//2]

    return frq, 2*abs(Y)/np.sqrt(frq_bin), Yr

###2nd ploynominal fuction###
def poly2(x, a, b, c):
    return a*x**2 + b*x +c
###2nd ploynominal fuction###
def poly2_1(x, a, b, c):
    return a*(x-b)**2+c
###1st ploynominal fuction###
def poly1(x, a, b) :
    return a*x + b

###Example READ PROD FILEs using H5py
def READ5PY(h5file) :
    with h5py.File(h5file, 'r') as f :
        print (f.keys)
        dset = f['wave']

def peak_height(data, rt_h, rt_x, ped, fit_term=5) :
    try:
        rt_term = np.linspace(rt_x - fit_term, rt_x + fit_term, fit_term * 2)
        popt1, pcov1 = curve_fit(poly2_1, rt_term, data[rt_x - fit_term:rt_x + fit_term],
                                 p0=[-1, rt_x, rt_h])

        x_rt = popt1[1]
        rt = popt1[2] - ped
    except RuntimeError:
        x_rt = 0
        rt = 0
    return x_rt, rt

def risetime (data, rt_X, rt_Y, fit_term=10) :
    try:
        def objective(x, a, b, c):
            return (rt_Y - poly2(x, a, b, c)) ** 2

        nrt_x = np.linspace(rt_X - fit_term, rt_X + fit_term, 2 * fit_term)
        popt1, pcov1 = curve_fit(poly2, nrt_x, data[rt_X - fit_term:rt_X + fit_term])
        res = minimize_scalar(objective, bounds=(rt_X - fit_term, rt_X + fit_term), method='bounded',
                                args=tuple(popt1))
        rt_f = res.x
    except :
        rt_f = 0

    return rt_f

def QSUM (data, base_ini, rt_X, pedestal) :
    try :
        qsum = np.sum(data[base_ini:int(rt_X)]) - (int(rt_X) - base_ini)*pedestal
    except :
        qsum = 0
    return qsum

'''HEAT Channel anaylsis'''
def heat_ana(heat_array, trg, base_ini=17200, dt_x=25000, wid=100):

    try:
        pedestal = np.mean(heat_array[base_ini+200:base_ini+400])
        peaks, _ = find_peaks(heat_array[base_ini:base_ini+3000], height=pedestal+50, width=wid, distance=wid)
        npeaks = len(peaks)
        #xhpeaks = peaks + base_ini
        #hpeak = np.max(heat_array[xhpeaks]) - pedestal
        #arghpeak = np.argmax(heat_array[xhpeaks])
        #xhpeak = xhpeaks[arghpeak]
        hpeak = np.max(heat_array[base_ini+600: base_ini+3000])
        xhpeak = np.argmax(heat_array[base_ini+600:base_ini+3000]) + base_ini+600

        fit_term = 10

        #peak height information
        xhpeak, hpeak = peak_height(heat_array, hpeak, xhpeak, pedestal, fit_term=fit_term)

        #RISE PART height
        rt90_h = hpeak * 0.9 + pedestal
        rt70_h = hpeak * 0.7 + pedestal
        rt50_h = hpeak * 0.5 + pedestal
        rt30_h = hpeak * 0.3 + pedestal
        rt20_h = hpeak * 0.2 + pedestal
        rt15_h = hpeak * 0.15 + pedestal
        rt10_h = hpeak * 0.1 + pedestal
        rt5_h = hpeak * 0.05 + pedestal
        rt_h = [rt90_h-pedestal, rt70_h-pedestal, rt50_h-pedestal, rt30_h-pedestal, rt20_h-pedestal, rt15_h-pedestal, rt10_h-pedestal, rt5_h-pedestal]

        arg_rt90 = np.argmin(abs(heat_array[base_ini+600:int(xhpeak)] - rt90_h)) + base_ini + 600
        arg_rt70 = np.argmin(abs(heat_array[base_ini+600:int(xhpeak)] - rt70_h)) + base_ini + 600
        arg_rt50 = np.argmin(abs(heat_array[base_ini+600:int(xhpeak)] - rt50_h)) + base_ini + 600
        arg_rt30 = np.argmin(abs(heat_array[base_ini+600:int(xhpeak)] - rt30_h)) + base_ini + 600
        arg_rt20 = np.argmin(abs(heat_array[base_ini+600:int(xhpeak)] - rt20_h)) + base_ini + 600
        arg_rt15 = np.argmin(abs(heat_array[base_ini+600:int(xhpeak)] - rt15_h)) + base_ini + 600
        arg_rt10 = np.argmin(abs(heat_array[base_ini+600:int(xhpeak)] - rt10_h)) + base_ini + 600
        arg_rt5 = np.argmin(abs(heat_array[base_ini+600:int(xhpeak)] - rt5_h)) + base_ini + 600

        #DECAY PART height
        dt90_h = hpeak * 0.9 + pedestal
        dt70_h = hpeak * 0.7 + pedestal
        dt50_h = hpeak * 0.5 + pedestal
        dt30_h = hpeak * 0.3 + pedestal
        dt10_h = hpeak * 0.1 + pedestal
        dt_h = [dt90_h-pedestal, dt70_h-pedestal, dt50_h-pedestal, dt30_h-pedestal, dt10_h-pedestal]

        arg_dt90 = np.argmin(abs(heat_array[int(xhpeak):int(xhpeak)+dt_x] - dt90_h)) + int(xhpeak)
        arg_dt70 = np.argmin(abs(heat_array[int(xhpeak):int(xhpeak)+dt_x] - dt70_h)) + int(xhpeak)
        arg_dt50 = np.argmin(abs(heat_array[int(xhpeak):int(xhpeak)+dt_x] - dt50_h)) + int(xhpeak)
        arg_dt30 = np.argmin(abs(heat_array[int(xhpeak):int(xhpeak)+dt_x] - dt30_h)) + int(xhpeak)
        arg_dt10 = np.argmin(abs(heat_array[int(xhpeak):int(xhpeak)+dt_x] - dt10_h)) + int(xhpeak)

        #RISE PART
        fit_term = 10
        rt90 = risetime(heat_array, arg_rt90, rt90_h, fit_term=fit_term)
        rt70 = risetime(heat_array, arg_rt70, rt70_h, fit_term=fit_term)
        rt50 = risetime(heat_array, arg_rt50, rt50_h, fit_term=fit_term)
        rt30 = risetime(heat_array, arg_rt30, rt30_h, fit_term=fit_term)
        rt20 = risetime(heat_array, arg_rt20, rt20_h, fit_term=fit_term)
        rt15 = risetime(heat_array, arg_rt15, rt15_h, fit_term=fit_term)
        rt10 = risetime(heat_array, arg_rt10, rt10_h, fit_term=fit_term)
        fit_term = 5
        rt5 = risetime(heat_array, arg_rt5, rt5_h, fit_term=fit_term)
        rt = [rt90, rt70, rt50, rt30, rt20, rt15, rt10, rt5]

        #DECAY PART
        fit_term = 20
        dt90 = risetime(heat_array, arg_dt90, dt90_h, fit_term=fit_term)
        dt70 = risetime(heat_array, arg_dt70, dt70_h, fit_term=fit_term)
        dt50 = risetime(heat_array, arg_dt50, dt50_h, fit_term=fit_term)
        dt30 = risetime(heat_array, arg_dt30, dt30_h, fit_term=fit_term)
        dt10 = risetime(heat_array, arg_dt10, dt10_h, fit_term=fit_term)
        dt = [dt90, dt70, dt50, dt30, dt10]

        endline = np.mean(heat_array[-2000:])
        endlineRMS = np.std(heat_array[-2000:])

        hbaseline = np.mean(heat_array[base_ini - 11000:base_ini - 1000])
        hbaselineRMS = np.std(heat_array[base_ini - 11000:base_ini - 1000])

        #SUM of PULSE
        qsum5 = QSUM(heat_array, base_ini, arg_rt5, pedestal)
        qsum10 = QSUM(heat_array, base_ini, arg_rt10, pedestal)
        qsum15 = QSUM(heat_array, base_ini, arg_rt15, pedestal)
        qsum20 = QSUM(heat_array, base_ini, arg_rt20, pedestal)
        qsum30 = QSUM(heat_array, base_ini, arg_rt30, pedestal)
        qsum50 = QSUM(heat_array, base_ini, arg_rt50, pedestal)
        qsum70 = QSUM(heat_array, base_ini, arg_rt70, pedestal)
        qsum90 = QSUM(heat_array, base_ini, arg_rt90, pedestal)
        qsum100 = QSUM(heat_array, base_ini, xhpeak, pedestal)
        qsum190 = QSUM(heat_array, base_ini, arg_dt90, pedestal)
        qsum170 = QSUM(heat_array, base_ini, arg_dt70, pedestal)
        qsum150 = QSUM(heat_array, base_ini, arg_dt50, pedestal)
        qsum130 = QSUM(heat_array, base_ini, arg_dt30, pedestal)
        qsum = [qsum5, qsum10, qsum15, qsum20, qsum30, qsum50, qsum70, qsum90, qsum100, qsum190, qsum170, qsum150, qsum130]

        result = [xhpeak, hpeak, npeaks, rt, rt_h, dt, dt_h, qsum, hbaseline, hbaselineRMS, endline, endlineRMS, pedestal]

    except :
        result = [0, 0, 0, [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 0, 0, 0, 0, 0]
    return result

def light_ana(light_array, base_ini=17200):

    try :
        pedestal = np.mean(light_array[base_ini+200:base_ini+400])
        peaks, _ = find_peaks(light_array[base_ini+500:base_ini+2000], height=pedestal + 10)
        #xlpeaks = peaks + base_ini
        #lpeak = np.max(light_array[xlpeaks]) - pedestal
        #arglpeak = np.argmax(light_array[xlpeaks])
        #xlpeak = xlpeaks[arglpeak]
        lpeak = np.max(light_array[base_ini+500:base_ini+2000])
        xlpeak = np.argmax(light_array[base_ini+500:base_ini+2000])+base_ini+500

        fit_term = 20
        xlpeak, lpeak = peak_height(light_array, lpeak, xlpeak, pedestal, fit_term=fit_term)

        # RISE PART height
        rt90_h = lpeak * 0.9 + pedestal
        rt50_h = lpeak * 0.5 + pedestal
        rt10_h = lpeak * 0.1 + pedestal
        rt_h = [rt90_h-pedestal, rt50_h-pedestal, rt10_h-pedestal]

        arg_rt90 = np.argmin(abs(light_array[base_ini+200:int(xlpeak)] - rt90_h)) + base_ini + 500
        arg_rt50 = np.argmin(abs(light_array[base_ini+200:int(xlpeak)] - rt50_h)) + base_ini + 500
        arg_rt10 = np.argmin(abs(light_array[base_ini+200:int(xlpeak)] - rt10_h)) + base_ini + 500

        #RISE PART
        fit_term = 5
        rt90 = risetime(light_array, arg_rt90, rt90_h, fit_term=fit_term)
        rt50 = risetime(light_array, arg_rt50, rt50_h, fit_term=fit_term)
        rt10 = risetime(light_array, arg_rt10, rt10_h, fit_term=fit_term)
        rt = [rt90, rt50, rt10]

        result = [xlpeak, lpeak, rt, rt_h]
    except :
        result = [0, 0, [0,0,0], [0,0,0]]

    return result

'''bandpass filter analysis for heat channel'''

def heat_filter_ana(heat_array, trg, lowcut, highcut, order, sampling_rate, base_ini=17200) :
    try :
        heat_array = np.array(heat_array).astype('int')
        fdata = butter_bandpass_filter(heat_array-heat_array[0], lowcut, highcut, sampling_rate, order=order)
        fbaseline = np.mean(fdata[base_ini - 10500:base_ini - 500])
        fbaselineRMS = np.std(fdata[base_ini - 10500:base_ini - 500])

        peaks, _ = find_peaks(fdata[base_ini:base_ini+2000], height=fbaseline+trg, prominence=1)
        xhfpeaks = peaks + base_ini
        npeaks = len(peaks)
        xhfpeak = np.argmax(fdata[base_ini:base_ini+2000]) + base_ini
        hfpeak = np.max(fdata[base_ini:base_ini+2000])
        #hfpeak = np.max(fdata[xhfpeaks]) - fbaseline
        #arghfpeak = np.argmax(fdata[xhfpeaks])
        #xhfpeak = xhfpeaks[arghfpeak]

        hfmin = np.min(fdata[xhfpeak : xhfpeak + 1200])
        xhfmin = np.argmin(fdata[xhfpeak : xhfpeak + 1200]) + xhfpeak

        fit_term = 10
        xhfmax, hfmax = peak_height(fdata, hfpeak, xhfpeak, fbaseline, fit_term=fit_term)
        xhfmin, hfmin = peak_height(fdata, hfmin, xhfmin, fbaseline, fit_term=fit_term)

        # RISE PART height
        rt90_h = hfmax * 0.9 + fbaseline
        rt50_h = hfmax * 0.5 + fbaseline
        rt10_h = hfmax * 0.1 + fbaseline
        rt_h = [rt90_h-fbaseline, rt50_h-fbaseline, rt10_h-fbaseline]

        arg_rt90 = np.argmin(abs(fdata[base_ini + 200:int(xhfmax)] - rt90_h)) + base_ini + 200
        arg_rt50 = np.argmin(abs(fdata[base_ini + 200:int(xhfmax)] - rt50_h)) + base_ini + 200
        arg_rt10 = np.argmin(abs(fdata[base_ini + 200:int(xhfmax)] - rt10_h)) + base_ini + 200

        # RISE PART
        fit_term = 10
        rt90 = risetime(fdata, arg_rt90, rt90_h, fit_term=fit_term)
        rt50 = risetime(fdata, arg_rt50, rt50_h, fit_term=fit_term)
        rt10 = risetime(fdata, arg_rt10, rt10_h, fit_term=fit_term)
        rt = [rt90, rt50, rt10]

        result = [xhfmax, hfmax, xhfmin, hfmin, npeaks, rt, rt_h, fbaseline, fbaselineRMS]

    except :
        result = [0, 0, 0, 0, 0, [0, 0, 0], [0, 0, 0], 0, 0]

    return result

'''bandpass filter analysis for light channel'''
def light_filter_ana(light_array, trg, lowcut, highcut, order, sampling_rate, base_ini=17200) :
    try :
        light_array = np.array(light_array).astype('int')
        fdata = butter_bandpass_filter(light_array-light_array[0], lowcut, highcut, sampling_rate, order=order)
        fbaseline = np.mean(fdata[base_ini - 5500:base_ini - 500])
        fbaselineRMS = np.std(fdata[base_ini - 5500:base_ini - 500])

        peaks, _ = find_peaks(fdata[base_ini:base_ini+2000], height=fbaseline+trg, prominence=1)
        xlfpeaks = peaks + base_ini
        npeaks = len(peaks)
        xlfpeak = np.argmax(fdata[base_ini:base_ini + 2000]) + base_ini
        lfpeak = np.max(fdata[base_ini:base_ini + 2000])

        #lfpeak = np.max(fdata[xlfpeaks]) - fbaseline
        #arglfpeak = np.argmax(fdata[xlfpeaks])
        #xlfpeak = xlfpeaks[arglfpeak]

        lfmin = np.min(fdata[xlfpeak : xlfpeak + 1000])
        xlfmin = np.argmin(fdata[xlfpeak : xlfpeak + 1000])

        fit_term = 10
        xlfmax, lfmax = peak_height(fdata, lfpeak, xlfpeak, fbaseline, fit_term=fit_term)
        xlfmin, lfmin = peak_height(fdata, lfmin, xlfmin, fbaseline, fit_term=fit_term)

        # RISE PART height
        rt90_h = lfmax * 0.9 + fbaseline
        rt50_h = lfmax * 0.5 + fbaseline
        rt10_h = lfmax * 0.1 + fbaseline
        rt_h = [rt90_h-fbaseline, rt50_h-fbaseline, rt10_h-fbaseline]

        arg_rt90 = np.argmin(abs(fdata[base_ini + 200:int(xlfmax)] - rt90_h)) + base_ini + 200
        arg_rt50 = np.argmin(abs(fdata[base_ini + 200:int(xlfmax)] - rt50_h)) + base_ini + 200
        arg_rt10 = np.argmin(abs(fdata[base_ini + 200:int(xlfmax)] - rt10_h)) + base_ini + 200

        # RISE PART
        fit_term = 10
        rt90 = risetime(fdata, arg_rt90, rt90_h, fit_term=fit_term)
        rt50 = risetime(fdata, arg_rt50, rt50_h, fit_term=fit_term)
        rt10 = risetime(fdata, arg_rt10, rt10_h, fit_term=fit_term)
        rt = [rt90, rt50, rt10]

        result = [xlfmax, lfmax, xlfmin, lfmin, npeaks, rt, rt_h, fbaseline, fbaselineRMS]

    except :
        result = [0, 0, 0, 0, 0, [0, 0, 0], [0, 0, 0], 0, 0]

    return result


'''For Template Fitting'''
def template_fitting(axx, amb_array, tmp_axx, tmp_array, window0, window1, xpeak10_b, xpeak10):
    try:
        OF_window0_b = int(xpeak10_b) - window0
        OF_window1_b = int(xpeak10_b) + window1

        axx_tmp_b = tmp_axx[OF_window0_b:OF_window1_b] - OF_window0_b
        ayy_tmp_b = tmp_array[OF_window0_b:OF_window1_b]

        OF_window0 = int(xpeak10) - window0
        OF_window1 = int(xpeak10) + window1

        axx_tmp = axx[OF_window0:OF_window1] - OF_window0
        ayy_tmp = amb_array[OF_window0:OF_window1]

        S_b = np.sum(ayy_tmp * ayy_tmp_b) / np.sum(ayy_tmp_b ** 2)

        samples = window1
        ayy_chi = ayy_tmp[window0:]
        ayy_chi_b = ayy_tmp_b[window0:] * S_b

        chi_b = np.sum((ayy_chi - ayy_chi_b) ** 2 / ayy_chi_b) / (samples - 1)

        result = [S_b, chi_b]

    except ValueError:
        result = [0, 0]

    return result

'''template fitting method time sink using trigger point'''
def template1(data, ref, init, fini, base_init, base_fini, lowcut, highcut, order, wig=0):
    try:
        sampling_rate = 100e3

        filt_ref = butter_bandpass_filter(ref - ref[0], lowcut, highcut, sampling_rate, order)
        filt_ref = filt_ref / np.max(filt_ref)

        data = data - np.mean(data[base_init:base_fini])
        filt_data = butter_bandpass_filter(data - data[0], lowcut, highcut, sampling_rate, order)

        res = stats.linregress(filt_ref[init:fini], filt_data[init + wig:fini + wig])

    except:
        print("failed lingress fitting")
        res = [0, 0, 0, 0, 0]
    return res


def template1_least(data, ref_wave, init, fini, base_init, base_fini, lowcut, highcut, order, ntry):
    res_arr = np.empty([2 * ntry, 5])

    for i in range(-ntry, ntry):
        res = template1(data, ref_wave, init, fini, base_init, base_fini, lowcut, highcut, order, int(i))
        res_arr[i] = res[0], res[1], res[2], res[3], res[4]

    min_res = res_arr[np.argmin(res_arr[:, 4])]
    return min_res

'''template fitting method3 for light (using refernce waveform applied bandpass filter)'''
def template2(data, ref, init, fini, base_init, base_fini, lowcut, highcut, order, wig=0):
    try:
        sampling_rate = 100e3

        filt_ref = ref
        data = data - np.mean(data[base_init:base_fini])
        filt_data = butter_bandpass_filter(data - data[0], lowcut, highcut, sampling_rate, order)

        res = stats.linregress(filt_ref[init:fini], filt_data[init + wig:fini + wig])
    except:
        print("failed lingress fitting")
        res = [0, 0, 0, 0, 0]
    return res

def template2_least(data, ref_wave, init, fini, base_init, base_fini, lowcut, highcut, order, ntry):
    res_arr = np.empty([2 * ntry, 5])

    for i in range(-ntry, ntry):
        res = template2(data, ref_wave, init, fini, base_init, base_fini, lowcut, highcut, order, int(i))
        res_arr[i] = res[0], res[1], res[2], res[3], res[4]

    min_res = res_arr[np.argmin(res_arr[:, 4])]
    return min_res

'''template fitting method time sink each max'''
def template3(data, ref, ini, fin, lowcut, highcut, order, wig=0):
    try:
        sampling_rate = 100e3

        filt_data = butter_bandpass_filter(data - data[0], lowcut, highcut, sampling_rate, order)

        ref_max = np.argmax(ref[20000:22000]) + 20000
        fdata_max = np.argmax(filt_data[20000:22000]) + 20000

        res = stats.linregress(ref[ref_max-int(ini):ref_max+int(fin)+int(wig)], filt_data[fdata_max-int(ini):fdata_max+int(fin)+int(wig)])
    except:
        #print("failed lingress fitting")
        res = [0, 0, 0, 0, 0]
    return res

def template3_least(data, ref_wave, ini, fin, lowcut, highcut, order, ntry):
    res_arr = np.empty([2 * ntry, 5])

    for i in range(-ntry, ntry):
        res = template3(data, ref_wave, ini, fin, lowcut, highcut, order, int(i))
        res_arr[i] = res[0], res[1], res[2], res[3], res[4]

    min_res = res_arr[np.argmin(res_arr[:, 4])]
    return min_res


### template fitting method for light channel time sink each max
def template4(data, ref, ini, fin, lowcut, highcut, order, wig=0):
    try:
        filt_data = ref
        ref_max = np.argmax(ref[20000:22000]) + 20000
        fdata_max = np.argmax(filt_data[20000:22000]) + 20000

        res = stats.linregress(ref[ref_max-int(ini):ref_max+int(fin)+int(wig)], filt_data[fdata_max-int(ini):fdata_max+int(fin)+int(wig)])
    except:
        #print("failed lingress fitting")
        res = [0, 0, 0, 0, 0]
    return res

def template4_least(data, ref_wave, ini, fin, lowcut, highcut, order, ntry):
    res_arr = np.empty([2 * ntry, 5])

    for i in range(-ntry, ntry):
        res = template4(data, ref_wave, ini, fin, lowcut, highcut, order, int(i))
        res_arr[i] = res[0], res[1], res[2], res[3], res[4]

    min_res = res_arr[np.argmin(res_arr[:, 4])]
    return min_res



'''freqeuncy domain Optimal filtering'''
def optimal_function2(tt, pulse_fft, template_fft, noise_pow, sampling_rate, tmax):
    try :
        f_bin = (1./tt[-1])
        N1 = len(tt)
        freqspec = np.fft.fftfreq(tt.shape[-1],1./sampling_rate)


        iomegat = np.exp(-1j*2*np.pi*freqspec*tmax)
        optimal_fft = iomegat*template_fft.conjugate()*pulse_fft/noise_pow
        optimal_ifft = np.fft.ifft(optimal_fft)

        facDM_N2 = (pulse_fft*template_fft.conjugate() + pulse_fft.conjugate()*template_fft)/(2*noise_pow)
        sample_freq_negative = freqspec[N1//2:]
        facDM_N2_negative = facDM_N2[N1//2:]
        sample_freq_negative = np.append(sample_freq_negative, freqspec[0])
        facDM_N2_negative = np.append(facDM_N2_negative, facDM_N2[0])
        sample_freq_positive = freqspec[:N1//2]
        facDM_N2_positive = facDM_N2[:N1//2]
        I1_facDM_N2 = simps(facDM_N2_positive,sample_freq_positive)
        I2_facDM_N2 = simps(facDM_N2_negative,sample_freq_negative)
        I_facDM_N2 = I1_facDM_N2 + I2_facDM_N2

        facM2_N2 = template_fft*template_fft.conjugate()/(noise_pow)
        facM2_N2_negative = facM2_N2[N1//2:]
        facM2_N2_negative = np.append(facM2_N2_negative, facM2_N2[0])
        facM2_N2_positive = facM2_N2[:N1//2]
        I1_facM2_N2 = simps(facM2_N2_positive,sample_freq_positive)
        I2_facM2_N2 = simps(facM2_N2_negative,sample_freq_negative)
        I_facM2_N2 = I1_facM2_N2 + I2_facM2_N2

        A1 = I_facDM_N2/I_facM2_N2

        A2 = np.max(np.abs(optimal_ifft))

    except ValueError :
        A1 = 0
        A2 = 0

    return A1, A2

def optimal_fit(noise, frq, ref, pulse, xhpeak, ini, fin):
    try :
        xhpeak = int(xhpeak)
        tt = np.linspace(0, (fin+ini)*10e-6, int(fin+ini))
        pulse = pulse - np.mean(pulse[xhpeak-2500:xhpeak-1500])

        # noise_fft
        y2 = noise
        frq2 = frq
        y2_base = y2 * y2.conjugate()
        y2_base_tmp = np.append(y2_base, y2_base[-1])
        y2_base_reverse = np.flip(y2_base_tmp[1:])
        y2_base_ext = np.append(y2_base, y2_base_reverse)
        noise_fft = y2_base_ext

        # ref_fft
        ref = ref[xhpeak-int(ini):xhpeak+int(fin)]

        pulse_fft = np.fft.fft(pulse[xhpeak-int(ini):xhpeak+int(fin)])
        ref_fft = np.fft.fft(ref)

        tmax_ref = np.argmax(pulse[xhpeak-int(ini):xhpeak+int(fin)]) / 100e3

        aa = optimal_function2(tt, pulse_fft, ref_fft, noise_fft, 100e3, tmax_ref)

        r = np.real(aa[0])

    except ValueError :
        r = 0

    return r

#def heat_ana(heat_array, trg, base_ini=17000, dt_x=25000, wid=100)
#def heat_filter_ana(heat_array, trg, wid, lowcut, highcut, order, sampling_rate, base_ini=17000)
'''Time domain optimal filtering'''
def filt (ref, covM, ini, fin) :
    try :
        r = heat_ana(ref, 50, base_ini=20800, dt_x=35000, wid=50)
        rt10 = int(r[3][-2])

        ref = ref[rt10-ini: rt10+fin]
        ref_T = ref.reshape(1,len(ref))
        ref_T = ref_T

        filt1 = np.dot(ref_T, np.linalg.inv(covM))
        filt2 = np.linalg.inv(np.dot(filt1, ref_T.T))
        filt3 = np.dot(filt2, ref_T)
        filt4 = np.dot(filt3, np.linalg.inv(covM))
        filt4 = filt4[0]
    except ValueError :
        filt4 = np.zeros(len(ref))

    return filt4

def of_time(data, xhpeak, ini, fin, filt) :
    try :
        arr = data - np.mean(data[20500:20700])
        arr = arr[xhpeak-ini:xhpeak+fin]
        arr = np.array(arr).reshape(len(arr), 1)

        filt = np.array(filt).reshape(1, np.shape(filt)[0])

        of_amp = np.dot(filt, arr)
        of_amp = np.array(of_amp)[0][0]
    except :
        of_amp = -1

    return of_amp