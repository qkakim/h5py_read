import numpy as np
import sys, os, glob, datetime, time, subprocess, threading, h5py

var1 = sys.argv[1]
var2 = sys.argv[2]
var3 = sys.argv[3]

runN = var1
crystal_name = var2
ch = var3
thr = 50
ncrystal = 4
mergeN = int(10)

'''Input Run Number'''
h5_path = '/mnt/lustre/ibs/cupamore/data/RODY/RAW/2021_2nd_RODY/{0}/'.format(runN)
h5files = h5_path+'*h5*'
path = '/mnt/lustre/ibs/wootae/h5py_read/h5Read_v0.0.3/h5Prod_v0.0.3'
exepath = path+'/Shell/'
OUTPATH = '/mnt/lustre/ibs/wootae/h5py_read/h5Read_v0.0.3/h5Prod_v0.0.3/data/'

h_ref_file = path + '/REF/ref_{0}_{1}_heat.txt'.format(runN, crystal_name)
l_ref_file = path + '/REF/ref_{0}_{1}_light.txt'.format(runN, crystal_name)
noise_file1 = path + '/NOISE/{1}_noise_{0}_normal.txt'.format(runN, crystal_name)
noise_file2 = path + '/NOISE/{1}_noise_{0}_hann.txt'.format(runN, crystal_name)
noise_file3 = path + '/NOISE/{1}_noise_{0}_hamm.txt'.format(runN, crystal_name)
covM_file = path + '/covM/covM_{0}_{1}.txt'.format(runN, crystal_name)
cut_file = path + '/txt/cut_{0}_{1}.txt'.format(runN, crystal_name)

init = 21000
fini = 52000

lst = np.loadtxt(cut_file).T.astype('int')
lst_arr = lst[:,2:4]
subrunNum = lst[:,0]*mergeN + lst[:,1]
file_array = np.array([subrunNum, lst_arr[:,0], lst_arr[:,1]])

def MakeBashSh_v2(subN) :
    shName = '{0}_{1}'.format(runN, crystal_name)
    shName = exepath+'rody_prod_{0}_{1}.sh'.format(shName, subN)
    with open(shName, 'w') as rsh :
        rsh.write('''\
#!/bin/bash -f
#SBATCH -J rody_{3}_{7}_{12}
#SBATCH -p jepyc
##SBATCH -N 10
##SBATCH -n 5
#SBATCH --mincpus=1
/mnt/lustre/ibs/wootae/anaconda3/bin/python {0}/h5run_1st.py {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14} {15} {16}
'''.format(path, h5_path, runN, mergeN, OUTPATH, cut_file, ch, init, fini, crystal_name, h_ref_file, l_ref_file, subN, noise_file1, noise_file2, noise_file3, covM_file))
    return shName

'''run bash script of QJOB'''

def JobShell(execute) :
    subprocess.Popen(["sbatch {0}".format(execute)], shell=True)

def Threading(execute):
    my_thread = threading.Thread(target=JobShell(execute))
    my_thread.start()

def run():
    for i in range(np.max(file_array[0,:]//mergeN)+1) :
        a = MakeBashSh_v2(i)
        print (a)
        #Threading(a)
        JobShell(a) 
        
if __name__ == "__main__":
    run()
