import numpy as np
import h5py, os, glob, datetime, time, subprocess, threading, sys
import ROOT

from array import array
from scipy.signal import *

var1 = sys.argv[1]
var2 = sys.argv[2]
var3 = sys.argv[3]
var4 = sys.argv[4]

h5_path = var1
subrunNum = var2
runN = var3
mergeN = int(var4)

'''Input Run Number'''
#h5files = h5_path+'*{0}*h5*'.format(runN)
h5files = h5_path+'*h5*'
OutPutPath = '/data/AMoRE/users/kimwootae/PROD/2020_2nd_RODY/{0}'.format(runN)

'''Sort files'''
def SORTINGFILE(n_files, mergeN) :
    #files = sorted(glob.glob(n_files), key=os.path.getmtime)
    files = sorted(glob.glob(n_files))
    n = len(files)
    infiles = []
    tfileGrup = []
    i = 0
    for filename in files :
        infiles.append(filename)
        if(len(infiles) == mergeN) :
            i += 1
            tfileGrup.append(infiles)
            infiles = []
        elif((len(files)//mergeN)==i and (len(files)%mergeN == len(infiles))) :
            tfileGrup.append(infiles)
    return tfileGrup

'''bandpass filter'''
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5*fs
    low = lowcut/nyq
    high = highcut/nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

'''TRIGGER'''
'''
def TRG(ch_array, thr) :
    ad = ch_array - thr

    ## 1st trigger
    n_trg1 = np.prod([ad[1:], ad[:-1]], axis=0)
    trg1 = (n_trg1 < 0)
    trg1 = np.append(trg1, False)

    ## 2nd trigger
    n_trg2 = np.diff(ch_array)
    n_trg2 = np.append(n_trg2, 0)
    trg2 = (n_trg2 > thr*0.01)

    ## Finial trigger
    trgf = np.all([trg1, trg2], axis=0)

    return np.where(trgf==True)
'''
'''temporary trigger logic'''
def TRG(ch_array, thr) :
    peaks, _ = find_peaks(ch_array, height=thr, prominence=1, width=50)
    return peaks

'''READ HDF5 FILE AND USING TRIGGER, OUTPUT:event array'''

def READ5PY (file_array) :
    init = 18000
    fini = 32000
    lowcut = 200
    highcut = 500
    fs = 100e+3

    maxn = 100000
    ncrystal = 4
    nch = ncrystal = ncrystal*2

    t = [[] for _ in range(nch)]
    ch = [[] for _ in range(nch)]
    trgon = [[] for _ in range(nch)]
    tmp = [[] for _ in range(nch)]
    tmpRMS = [[] for _ in range(nch)]
    
    aadc = array('i', nch*[0])

    for i in range(nch) :
        t[i] = array('f', [0])
        ch[i] = array('i', [0]*maxn)
        trgon[i] = array('i', [0])
        tmp[i] = array('f', [0])
        tmpRMS[i] = array('f', [0])

    fout = ROOT.TFile("{0}/nopeak_{1}_{2}.root".format(OutPutPath, runN, subrunNum), "recreate")
    #fout = TFile("{0}/data/{1:03d}_{2:05d}.root".format(OutPutPATH, int(RunNum), int(subrunNum)), "recreate")
    tr = ROOT.TTree("event", "1st Production event")

    for ii in range(nch) :
        tr.Branch("ch_{0}".format(ii), ch[ii], "ch_{0}[100000]/I".format(ii))
        tr.Branch("t_{0}".format(ii), t[ii], "t_{0}[1]/F".format(ii))
        tr.Branch("trgon_{0}".format(ii), trgon[ii], "trgon_{0}[1]/I".format(ii))
        tr.Branch("tmp_{0}".format(ii), tmp[ii], "tmp_{0}[1]/F".format(ii))
        tr.Branch("tmpRMS_{0}".format(ii), tmpRMS[ii], "tmpRMS_{0}[1]/F".format(ii))

    for i in file_array :
        print (i)
        with h5py.File(i, 'r') as f :
            print ("Keys : %s" %f.keys())
            print (list(f.keys()))

            dset = f['rawdata']
            print (dset.dtype)
            print (dset.shape)

            mod = 1
            lch = 8
            gch = mod*lch
            print ("total global channel : ", gch)

            ladc = [[] for _ in range(lch)]
            h_gadc = [[[],[],[],[],[],[],[],[]] for _ in range(mod)]
            l_gadc = [[[],[],[],[],[],[],[],[]] for _ in range(mod)]
            
            for ii in range(int(dset.shape[0])) :
                data = np.array(dset[ii][2])
                tt = np.array(dset[ii][1])
                if ((ii == 0) or (ii % 100 == 0)) :
                    print (datetime.datetime.now())
                for j in range(mod) :

                    for k in range(1, lch, 2) :
                        h_y = np.array(data[j][k])
                        l_y = np.array(data[j][k-1])

                        h_gadc[j][k].append(h_y)
                        l_gadc[j][k].append(l_y)

                        if(len(h_gadc[j][k]) <= 6) :
                            continue

                        if(len(h_gadc[j][k]) > 6) :
                            del h_gadc[j][k][0]
                            del l_gadc[j][k][0]
                        
                        h_yy = np.array(np.reshape(h_gadc[j][k], -1)).astype('int')
                        l_yy = np.array(np.reshape(l_gadc[j][k], -1)).astype('int')

                        yyx = np.array(h_yy[30000:130000])
                        yyy = butter_bandpass_filter(yyx-yyx[0], lowcut, highcut, fs, order=1)
                        testx = TRG(yyy, 150)

                        TMP = np.array(data[j][7])
                        tmpmean = np.mean(TMP[-2000:])
                        tmprms = np.std(TMP[-2000:])
                        if(len(testx) > 1) : continue
                        #if(len(testx[0]) >= 2) : continue
                        
                        trgon[k-1][0] = 1
                        trgon[k][0] = 1
                        tmp[k][0] = tmpmean
                        tmpRMS[k][0] = tmprms
                        tmp[k-1][0] = tmpmean
                        tmpRMS[k-1][0] = tmprms
                        t[k-1][0] = tt[j]*1e-9+(ii+30000)*1e-6
                        t[k][0] = tt[j]*1e-9+(ii+30000)*1e-6
                            
                        hh_y = h_yy[int(30000):int(130000)]
                        ll_y = l_yy[int(30000):int(130000)]
                            
                        for iii in range(maxn) :
                            ch[k-1][iii] = ll_y[iii]        
                            ch[k][iii] = hh_y[iii]        
                            
                        tr.Fill()

                        '''initialize parameters'''
                        t[k-1][0] = 0
                        t[k][0] = 0
                        tmp[k][0] = 0
                        tmpRMS[k][0] = 0
                        trgon[k-1][0] = 0
                        trgon[k][0] = 0

                        for iii in range(maxn) :
                            ch[k-1][iii] = 0  
                            ch[k][iii] = 0
    
    tr.Write()
    fout.Close()


'''RUN FUNCTION'''
def run() :
    a = SORTINGFILE(h5files, mergeN)
    inputfiles = a[int(subrunNum)]
    READ5PY(inputfiles)

if __name__ == '__main__' :
    run()
