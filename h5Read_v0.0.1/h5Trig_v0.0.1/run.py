import numpy as np
import sys, os, glob, datetime, time, subprocess, threading, h5py

from scipy.signal import *

var1 = sys.argv[1]
runN = var1

mergeN = int(15)

#'''Input Run Number'''
#h5_path = '/data/AMoRE/users/kimwootae/RAW/2020_2nd_RODY/{0}/'.format(runN)
#h5files = h5_path+'*{0}*h5'.format(runN)
#path = '/data/AMoRE/users/kimwootae/work/version_test/h5_rody/prod_v1.0.0/'
#exepath = path+'/Shell/'

'''Input Run Number'''
#h5_path = '/data/AMoRE/users/kimwootae/RAW/2020_2nd_RODY/{0}/'.format(runN)
h5_path = '/data/AMoRE/users/kimwootae/RAW/2021_1st_RODY/{0}/'.format(runN)
h5files = h5_path+'*h5*'
path = '/data/AMoRE/users/kimwootae/work/h5py_read/h5Read_v0.0.1/h5Trig_v0.0.1'
exepath = path+'/Shell/'

def MakeBashSh(subrunNum) :
    shName = 'rody_{0}'.format(runN)
    shName = exepath+'{0}_{1}.sh'.format(shName, subrunNum)
    with open(shName, 'w') as rsh :
        rsh.write('''\

##!/bin/bash

path={1}
subrunNum={2}
source /share/amore/anaconda3/etc/profile.d/conda.sh
source /share/amore/ROOT/6.14.04-v01.01/bin/thisroot.sh
source /home/kimwootae/env/pyroot.sh
conda activate v01

log_file={0}/LOSG/test_{2}.log
python {0}/h5prod_v2_1.py {1} {2} {3} {4}
'''.format(path, h5_path,subrunNum, runN, mergeN))
    return shName

'''run bash script of QJOB'''
def JobShell(execute) :
    subprocess.Popen(["qsub -q short {0}".format(execute)],shell=True)

def SORTINGFILE(n_files, mergeN) :
    #files = sorted(glob.glob(n_files), key=os.path.getmtime)
    files = sorted(glob.glob(n_files))
    n = len(files)
    infiles = []
    tfileGrup = []
    i = 0
    
    for filename in files : 
        infiles.append(filename)
        if(len(infiles) == mergeN) :
            i += 1
            tfileGrup.append(infiles)
            infiles = []
        elif((len(files)//mergeN) ==i  and (len(files)%mergeN == len(infiles))) :
            tfileGrup.append(infiles)
    return tfileGrup

def Threading(execute) :
    my_thread = threading.Thread(target=JobShell(execute))
    my_thread.start()

def run() :
    tfileGrup = SORTINGFILE(h5files, mergeN)
    print (h5files) 
    shell_array = []
    for i in range(len(tfileGrup)) :
        a = MakeBashSh(i)
        shell_array.append(a)
     
    for i in range((len(shell_array))) :
        Threading(shell_array[i])
    
    print (shell_array)
    
if __name__ == "__main__" : 
    run()
