import pandas as pd
import numpy as np
import os, sys, re, h5py
import matplotlib.pyplot as plt
import time

from scipy.optimize import *
from scipy.integrate import *
from scipy.signal import *

import FuncLib20211st as Func

var1 = sys.argv[1]
var2 = sys.argv[2]
var3 = sys.argv[3]
var4 = sys.argv[4]

ROOT_PATH = var1
OUTPATH = var2
subrunNum = var3
runN = var4

start_execute = time.time()

hlowcut = 30
hhighcut = 220
llowcut = 200
lhighcut = 500
fs = 100e+3
order = 1
conversion_factor = 10./2**18
base_start = 15200
base_end = 17000
nmod = 4

pd_heat = [pd.DataFrame({'time' : [0], 'xhpeak' : [0], 'hpeak' : [0], 'hrt90_h' : [0], 'hrt70_h' : [0], 'hrt50_h' : [0], 'hrt10_h' : [0], 'hrt10_h' : [0], 'hrt90' : [0], 'hrt70' : [0], 'hrt50' : [0], 'hrt30' : [0], 'hrt10' : [0], 'hdt90_h' : [0], 'hdt70_h' : [0], 'hdt50_h' : [0], 'hdt30_h' : [0], 'hdt10_h' : [0], 'hdt90' : [0], 'hdt70' : [0], 'hdt50' : [0], 'hdt30' : [0], 'hdt10' : [0], 'hpedestal' : [0], 'hendline' : [0], 'hendlineRMS' : [0], 'hbaseline' : [0], 'hbaselineRMS' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0], 'pedestal' : [0], 'pedestalRMS' : [0], 'S_a ' : [0], 'chi2' : [0], 'Slope' : [0]}) for _ in range(4)]
pd_light = [pd.DataFrame({'time' : [0], 'xlpeak' : [0], 'lpeak' : [0], 'lrt90_h' : [0], 'lrt50_h' : [0], 'lrt10_h' : [0], 'lrt90' : [0], 'lrt50' : [0], 'lrt10' : [0], 'lpedestal' : [0], 'lendline' : [0], 'lendlineRMS' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0]}) for _ in range(4)]
pd_heat_band1 = [pd.DataFrame({'time' : [0], 'xhfpeak' : [0], 'hfpeak' : [0], 'hfbaseline': [0], 'hfbaselineRMS' : [0], 'xhfmin' : [0], 'hfmin' : [0], 'xhfpeak2nd' : [0], 'hfpeak2nd' : [0], 'fit_xhfpeak' :[0], 'fit_hfpeak' : [0], 'fit_xhfmin' : [0], 'fit_hfmin' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0], 'S_a ' : [0], 'chi2' : [0]}) for _ in range(4)]
pd_heat_band2 = [pd.DataFrame({'time' : [0], 'xhfpeak' : [0], 'hfpeak' : [0], 'hfbaseline': [0], 'hfbaselineRMS' : [0], 'xhfmin' : [0], 'hfmin' : [0], 'xhfpeak2nd' : [0], 'hfpeak2nd' : [0], 'fit_xhfpeak' :[0], 'fit_hfpeak' : [0], 'fit_xhfmin' : [0], 'fit_hfmin' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0], 'S_a ' : [0], 'chi2' : [0]}) for _ in range(4)]
pd_heat_band3 = [pd.DataFrame({'time' : [0], 'xhfpeak' : [0], 'hfpeak' : [0], 'hfbaseline': [0], 'hfbaselineRMS' : [0], 'xhfmin' : [0], 'hfmin' : [0], 'xhfpeak2nd' : [0], 'hfpeak2nd' : [0], 'fit_xhfpeak' :[0], 'fit_hfpeak' : [0], 'fit_xhfmin' : [0], 'fit_hfmin' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0], 'S_a ' : [0], 'chi2' : [0]}) for _ in range(4)]
pd_heat_band4 = [pd.DataFrame({'time' : [0], 'xhfpeak' : [0], 'hfpeak' : [0], 'hfbaseline': [0], 'hfbaselineRMS' : [0], 'xhfmin' : [0], 'hfmin' : [0], 'xhfpeak2nd' : [0], 'hfpeak2nd' : [0], 'fit_xhfpeak' :[0], 'fit_hfpeak' : [0], 'fit_xhfmin' : [0], 'fit_hfmin' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0], 'S_a ' : [0], 'chi2' : [0]}) for _ in range(4)]
pd_heat_band5 = [pd.DataFrame({'time' : [0], 'xhfpeak' : [0], 'hfpeak' : [0], 'hfbaseline': [0], 'hfbaselineRMS' : [0], 'xhfmin' : [0], 'hfmin' : [0], 'xhfpeak2nd' : [0], 'hfpeak2nd' : [0], 'fit_xhfpeak' :[0], 'fit_hfpeak' : [0], 'fit_xhfmin' : [0], 'fit_hfmin' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0], 'S_a ' : [0], 'chi2' : [0]}) for _ in range(4)]
pd_heat_band6 = [pd.DataFrame({'time' : [0], 'xhfpeak' : [0], 'hfpeak' : [0], 'hfbaseline': [0], 'hfbaselineRMS' : [0], 'xhfmin' : [0], 'hfmin' : [0], 'xhfpeak2nd' : [0], 'hfpeak2nd' : [0], 'fit_xhfpeak' :[0], 'fit_hfpeak' : [0], 'fit_xhfmin' : [0], 'fit_hfmin' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0], 'S_a ' : [0], 'chi2' : [0]}) for _ in range(4)]
pd_heat_band7 = [pd.DataFrame({'time' : [0], 'xhfpeak' : [0], 'hfpeak' : [0], 'hfbaseline': [0], 'hfbaselineRMS' : [0], 'xhfmin' : [0], 'hfmin' : [0], 'xhfpeak2nd' : [0], 'hfpeak2nd' : [0], 'fit_xhfpeak' :[0], 'fit_hfpeak' : [0], 'fit_xhfmin' : [0], 'fit_hfmin' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0], 'S_a ' : [0], 'chi2' : [0]}) for _ in range(4)]
pd_light_band = [pd.DataFrame({'time': [0], 'xlfpeak' : [0], 'lfpeak' : [0], 'lfbaseline': [0], 'lfbaselineRMS' : [0], 'xlfmin' : [0], 'lfmin' : [0], 'xlfpeak2nd' : [0], 'lfpeak2nd' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0]}) for _ in range(4)]

#ROOT_PATH = '/data/AMoRE/users/kimwootae/PROD/2020_2nd_RODY/20201030/20201030_0.root'
ROOT_FILE = ROOT_PATH+'{0}_{1}.root'.format(runN, subrunNum)

tr_input = Func.pyroot_file_read(ROOT_FILE)
print(ROOT_FILE)
nentries = tr_input.GetEntries()
print (nentries)

ref_wave = [[] for _ in range(nmod)]
ref_wave[0] = np.ones(50000)
ref_wave[1] = np.loadtxt('/data/AMoRE/users/kimwootae/work/h5py_read/h5Read_v0.0.1/h5Prod_v0.0.1/Ref/20210323_LMOA_ref_2p6_1131.txt')
ref_wave[2] = np.loadtxt('/data/AMoRE/users/kimwootae/work/h5py_read/h5Read_v0.0.1/h5Prod_v0.0.1/Ref/20210323_LMO6_ref_2p6_1080.txt')
ref_wave[3] = np.ones(50000)

for i in range(nentries) :

    if (i%100==0 and i>0) :
        inter_execute = time.time()
        inter_run_time = inter_execute - start_execute
        print ("{0}, run time:{1}".format(i, inter_run_time))

    evtnumber = i
    ch_heat, ch_light, time_heat, time_light, base_heat, base_light, baseRMS_heat, baseRMS_light, trgON, tmp_heat, tmp_light, tmpRMS_heat, tmpRMS_light = Func.pyroot_read(tr_input, evtnumber, base_start, base_end, fs)

    #if (i > 200) : break

    for ii in range(nmod) :
        trg = trgON[ii]
        if(trg == 0) : continue
        
        h = np.array(ch_heat[ii])
        l = np.array(ch_light[ii])
        h_mean = np.mean(h[5000:15000])
        h_std = np.std(h[5000:15000])
        h_ped = np.mean(h[17500:17900])
        h_pedRMS = np.std(h[17500:17900])
        h_x = np.arange(0,len(h))
        #heat channel ana
        h_ana = Func.heat_ana(h, conversion_factor, fs, np.mean(h[14000:17000]), np.std(h[14000:17000]), base_end)
        rt90_x, rt70_x, rt50_x, rt30_x, rt10_x = h_ana[2]
        rt90, rt70, rt50, rt30, rt10 = h_ana[3]
        dt90_x, dt70_x, dt50_x, dt30_x, dt10_x = h_ana[4]
        dt90, dt70, dt50, dt30, dt10 = h_ana[5]

        #print ('rt_x : ', np.array(h_ana[2])*10e-6)
        #print ('rt : ', np.array(h_ana[3])*10./2**18)
        #print ('dt_x : ', np.array(h_ana[4])*10e-6)
        #print ('dt : ', np.array(h_ana[5])*10./2**18)

        ref_w = ref_wave[ii] - ref_wave[ii][0]
        #ref_w = ref_w/np.max(ref_w)
        h_for_tmp =  h-h[int(rt10_x)-100]
        htmp_ana = Func.heat_ana(ref_w, conversion_factor, fs, np.mean(ref_w[14000:17000]), np.std(ref_w[14000:17000]), base_end)
        h_ana_tmp = Func.template_fitting(h_x, h_for_tmp, h_x, ref_wave[ii]-ref_wave[ii][0], 700, 3000, int(rt10_x), int(htmp_ana[2][4]))
        #heat channel bandpass filter ana
        h_band_ana1 = Func.heat_filter_ana(30, 100, 1, h-h[0], conversion_factor, fs, base_end)
        h_band_ana1_tmp = Func.bandpass_template_fitting(h_x, h, h_x, ref_wave[ii], 1000, 3000, int(rt10_x), 30, 100, 1)
        h_band_ana2 = Func.heat_filter_ana(70, 100, 1, h-h[0], conversion_factor, fs, base_end) 
        h_band_ana2_tmp = Func.bandpass_template_fitting(h_x, h, h_x, ref_wave[ii], 1000, 3000, int(rt10_x), 70, 100, 1)
        h_band_ana3 = Func.heat_filter_ana(10, 350, 1, h-h[0], conversion_factor, fs, base_end)
        h_band_ana3_tmp = Func.bandpass_template_fitting(h_x, h, h_x, ref_wave[ii], 1000, 3000, int(rt10_x), 10, 350, 1)
        h_band_ana4 = Func.heat_filter_ana(60, 450, 3, h-h[0], conversion_factor, fs, base_end)
        h_band_ana4_tmp = Func.bandpass_template_fitting(h_x, h, h_x, ref_wave[ii], 1000, 3000, int(rt10_x), 60, 450, 3)
        h_band_ana5 = Func.heat_filter_ana(20, 240, 3, h-h[0], conversion_factor, fs, base_end)
        h_band_ana5_tmp = Func.bandpass_template_fitting(h_x, h, h_x, ref_wave[ii], 1000, 3000, int(rt10_x), 20, 240, 3)
        h_band_ana6 = Func.heat_filter_ana(80, 740, 5, h-h[0], conversion_factor, fs, base_end)
        h_band_ana6_tmp = Func.bandpass_template_fitting(h_x, h, h_x, ref_wave[ii], 1000, 3000, int(rt10_x), 80, 740, 5)
        h_band_ana7 = Func.heat_filter_ana(220, 440, 5, h-h[0], conversion_factor, fs, base_end)
        h_band_ana7_tmp = Func.bandpass_template_fitting(h_x, h, h_x, ref_wave[ii], 1000, 3000, int(rt10_x), 220, 440, 5)
        #light channel ana
        l_ana = Func.light_ana(l, conversion_factor, fs, np.mean(l[14000:17000]), np.std(l[14000:17000]), base_end)
        #light channel bandpass filter ana
        l_band_ana = Func.light_filter_ana(llowcut, lhighcut, order, l, conversion_factor, fs, base_end)
        pd_h_ana = pd.DataFrame({'time' : [time_heat[ii]], 'xhpeak' : [h_ana[0]], 'hpeak' : [h_ana[1]]\
                , 'hrt90_h' : rt90, 'hrt70_h' : rt70, 'hrt50_h' : rt50, 'hrt30_h' : rt30, 'hrt10_h' : rt10\
                , 'hrt90' : rt90_x, 'hrt70' : rt70_x, 'hrt50' : rt50_x, 'hrt30' : rt30_x, 'hrt10' : rt10_x\
                , 'hdt90_h' : dt90, 'hdt70_h' : dt70, 'hdt50_h' : dt50, 'hdt30_h' : dt30, 'hdt10_h' : dt10\
                , 'hrt90' : dt90_x, 'hdt70' : dt70_x, 'hdt50' : dt50_x, 'hdt30' : dt30_x, 'hdt10' : dt10_x\
                , 'hbaseline' : [h_mean], 'hendline' : [h_ana[6]], 'hendlineRMS' : [h_ana[7]], 'hbaselineRMS' : [h_std]\
                , 'tmp' : [tmp_heat[ii]], 'tmpRMS' : [tmpRMS_heat[ii]], 'evtnum' : [evtnumber], 'subrun' : [subrunNum]\
                , 'pedestal' : [h_ped], 'pedestalRMS' : [h_pedRMS], 'S_a' : [h_ana_tmp[0]], 'chi2' : [h_ana_tmp[1]], 'Slope' : [h_ana[-1]]})
        pd_l_ana = pd.DataFrame({'time' : [time_light[ii]], 'xlpeak' : [l_ana[0]], 'lpeak' : [l_ana[1]]\
                , 'lrt90_h' : [l_ana[2]], 'lrt50_h' : [l_ana[3]], 'lrt10_h' : [l_ana[3]]\
                , 'lrt90' : [l_ana[4]], 'lrt50' : [l_ana[5]], 'lrt10' : [l_ana[6]]\
                , 'lpedestal' : [l_ana[14]], 'lendline' : [l_ana[11]], 'lendlineRMS' : [l_ana[12]]\
                , 'tmp' : [tmp_light[ii]], 'tmpRMS' : [tmpRMS_light[ii]], 'evtnum' : [evtnumber], 'subrun' : [subrunNum]})
        pd_h_band_ana1 = pd.DataFrame({'time' : [time_heat[ii]], 'xhfpeak' : [h_band_ana1[0]], 'hfpeak' : [h_band_ana1[1]], 'hfbaseline' : [h_band_ana1[2]], 'hfbaselineRMS' : [h_band_ana1[3]]\
                , 'xhfmin' : [h_band_ana1[4]], 'hfmin' : [h_band_ana1[5]], 'xhfpeak2nd' : [h_band_ana1[6]], 'hfpeak2nd' : [h_band_ana1[7]]\
                , 'fit_xhfpeak' :[h_band_ana1[8]], 'fit_hfpeak' : [h_band_ana1[9]], 'fit_xhfmin' : [h_band_ana1[10]], 'fit_hfmin' : [h_band_ana1[11]]\
                , 'tmp' : [tmp_heat[ii]], 'tmpRMS' : [tmpRMS_heat[ii]], 'evtnum' : [evtnumber], 'subrun' : [subrunNum], 'S_a' : [h_band_ana1_tmp[0]], 'chi2' : [h_band_ana1_tmp[1]]})
        pd_h_band_ana2 = pd.DataFrame({'time' : [time_heat[ii]], 'xhfpeak' : [h_band_ana2[0]], 'hfpeak' : [h_band_ana2[1]], 'hfbaseline' : [h_band_ana2[2]], 'hfbaselineRMS' : [h_band_ana2[3]]\
                , 'xhfmin' : [h_band_ana2[4]], 'hfmin' : [h_band_ana2[5]], 'xhfpeak2nd' : [h_band_ana2[6]], 'hfpeak2nd' : [h_band_ana2[7]]\
                , 'fit_xhfpeak' :[h_band_ana2[8]], 'fit_hfpeak' : [h_band_ana2[9]], 'fit_xhfmin' : [h_band_ana2[10]], 'fit_hfmin' : [h_band_ana2[11]]\
                , 'tmp' : [tmp_heat[ii]], 'tmpRMS' : [tmpRMS_heat[ii]], 'evtnum' : [evtnumber], 'subrun' : [subrunNum], 'S_a' : [h_band_ana2_tmp[0]], 'chi2' : [h_band_ana2_tmp[1]]})
        pd_h_band_ana3 = pd.DataFrame({'time' : [time_heat[ii]], 'xhfpeak' : [h_band_ana3[0]], 'hfpeak' : [h_band_ana3[1]], 'hfbaseline' : [h_band_ana3[2]], 'hfbaselineRMS' : [h_band_ana3[3]]\
                , 'xhfmin' : [h_band_ana3[4]], 'hfmin' : [h_band_ana3[5]], 'xhfpeak2nd' : [h_band_ana3[6]], 'hfpeak2nd' : [h_band_ana3[7]]\
                , 'fit_xhfpeak' :[h_band_ana3[8]], 'fit_hfpeak' : [h_band_ana3[9]], 'fit_xhfmin' : [h_band_ana3[10]], 'fit_hfmin' : [h_band_ana3[11]]\
                , 'tmp' : [tmp_heat[ii]], 'tmpRMS' : [tmpRMS_heat[ii]], 'evtnum' : [evtnumber], 'subrun' : [subrunNum], 'S_a' : [h_band_ana3_tmp[0]], 'chi2' : [h_band_ana3_tmp[1]]})
        pd_h_band_ana4 = pd.DataFrame({'time' : [time_heat[ii]], 'xhfpeak' : [h_band_ana4[0]], 'hfpeak' : [h_band_ana4[1]], 'hfbaseline' : [h_band_ana4[2]], 'hfbaselineRMS' : [h_band_ana4[3]]\
                , 'xhfmin' : [h_band_ana4[4]], 'hfmin' : [h_band_ana4[5]], 'xhfpeak2nd' : [h_band_ana4[6]], 'hfpeak2nd' : [h_band_ana4[7]]\
                , 'fit_xhfpeak' :[h_band_ana4[8]], 'fit_hfpeak' : [h_band_ana4[9]], 'fit_xhfmin' : [h_band_ana4[10]], 'fit_hfmin' : [h_band_ana4[11]]\
                , 'tmp' : [tmp_heat[ii]], 'tmpRMS' : [tmpRMS_heat[ii]], 'evtnum' : [evtnumber], 'subrun' : [subrunNum], 'S_a' : [h_band_ana4_tmp[0]], 'chi2' : [h_band_ana4_tmp[1]]})
        pd_h_band_ana5 = pd.DataFrame({'time' : [time_heat[ii]], 'xhfpeak' : [h_band_ana5[0]], 'hfpeak' : [h_band_ana5[1]], 'hfbaseline' : [h_band_ana5[2]], 'hfbaselineRMS' : [h_band_ana5[3]]\
                , 'xhfmin' : [h_band_ana5[4]], 'hfmin' : [h_band_ana5[5]], 'xhfpeak2nd' : [h_band_ana5[6]], 'hfpeak2nd' : [h_band_ana5[7]]\
                , 'fit_xhfpeak' :[h_band_ana5[8]], 'fit_hfpeak' : [h_band_ana5[9]], 'fit_xhfmin' : [h_band_ana5[10]], 'fit_hfmin' : [h_band_ana5[11]]\
                , 'tmp' : [tmp_heat[ii]], 'tmpRMS' : [tmpRMS_heat[ii]], 'evtnum' : [evtnumber], 'subrun' : [subrunNum], 'S_a' : [h_band_ana5_tmp[0]], 'chi2' : [h_band_ana5_tmp[1]]})
        pd_h_band_ana6 = pd.DataFrame({'time' : [time_heat[ii]], 'xhfpeak' : [h_band_ana6[0]], 'hfpeak' : [h_band_ana6[1]], 'hfbaseline' : [h_band_ana6[2]], 'hfbaselineRMS' : [h_band_ana6[3]]\
                , 'xhfmin' : [h_band_ana6[4]], 'hfmin' : [h_band_ana6[5]], 'xhfpeak2nd' : [h_band_ana6[6]], 'hfpeak2nd' : [h_band_ana6[7]]\
                , 'fit_xhfpeak' :[h_band_ana6[8]], 'fit_hfpeak' : [h_band_ana6[9]], 'fit_xhfmin' : [h_band_ana6[10]], 'fit_hfmin' : [h_band_ana6[11]]\
                , 'tmp' : [tmp_heat[ii]], 'tmpRMS' : [tmpRMS_heat[ii]], 'evtnum' : [evtnumber], 'subrun' : [subrunNum], 'S_a' : [h_band_ana6_tmp[0]], 'chi2' : [h_band_ana6_tmp[1]]})
        pd_h_band_ana7 = pd.DataFrame({'time' : [time_heat[ii]], 'xhfpeak' : [h_band_ana7[0]], 'hfpeak' : [h_band_ana7[1]], 'hfbaseline' : [h_band_ana7[2]], 'hfbaselineRMS' : [h_band_ana7[3]]\
                , 'xhfmin' : [h_band_ana7[4]], 'hfmin' : [h_band_ana7[5]], 'xhfpeak2nd' : [h_band_ana7[6]], 'hfpeak2nd' : [h_band_ana7[7]]\
                , 'fit_xhfpeak' :[h_band_ana7[8]], 'fit_hfpeak' : [h_band_ana7[9]], 'fit_xhfmin' : [h_band_ana7[10]], 'fit_hfmin' : [h_band_ana7[11]]\
                , 'tmp' : [tmp_heat[ii]], 'tmpRMS' : [tmpRMS_heat[ii]], 'evtnum' : [evtnumber], 'subrun' : [subrunNum], 'S_a' : [h_band_ana7_tmp[0]], 'chi2' : [h_band_ana7_tmp[1]]})
        pd_l_band_ana = pd.DataFrame({'time' : [time_light[ii]], 'xlfpeak' : [l_band_ana[0]], 'lfpeak' : [l_band_ana[1]], 'lfbaseline' : [l_band_ana[2]], 'lfbaselineRMS' : [l_band_ana[3]]\
                , 'xlfmin' : [l_band_ana[4]], 'lfmin' : [l_band_ana[5]], 'xlfpeak2nd' : [l_band_ana[6]], 'lfpeak2nd' : [l_band_ana[7]]\
                , 'tmp' : [tmp_light[ii]], 'tmpRMS' : [tmpRMS_light[ii]], 'evtnum' : [evtnumber], 'subrun' : [subrunNum]})
        
        pd_heat[ii] = pd_heat[ii].append(pd_h_ana, ignore_index=True)
        pd_light[ii] = pd_light[ii].append(pd_l_ana, ignore_index=True)
        pd_heat_band1[ii] = pd_heat_band1[ii].append(pd_h_band_ana1, ignore_index=True)
        pd_heat_band2[ii] = pd_heat_band2[ii].append(pd_h_band_ana2, ignore_index=True)
        pd_heat_band3[ii] = pd_heat_band3[ii].append(pd_h_band_ana3, ignore_index=True)
        pd_heat_band4[ii] = pd_heat_band4[ii].append(pd_h_band_ana4, ignore_index=True)
        pd_heat_band5[ii] = pd_heat_band5[ii].append(pd_h_band_ana5, ignore_index=True)
        pd_heat_band6[ii] = pd_heat_band6[ii].append(pd_h_band_ana6, ignore_index=True)
        pd_heat_band7[ii] = pd_heat_band7[ii].append(pd_h_band_ana7, ignore_index=True)
        pd_light_band[ii] = pd_light_band[ii].append(pd_l_band_ana, ignore_index=True)
        
        #PATH = '/data/AMoRE/users/kimwootae/work/h5py_read/H5PROD_TEST/'
        #print (OUTPATH+"{0}/".format(runN))
for i in range(4) :
    pd_heat[i].to_hdf(OUTPATH+"{0}/heat_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat', mode='w')
    pd_light[i].to_hdf(OUTPATH+"{0}/light_ch{1}_{2}.h5".format(runN, i, subrunNum), key='light', mode='w')
    pd_heat_band1[i].to_hdf(OUTPATH+"{0}/heat_filter1_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat_band', mode='w')
    pd_heat_band2[i].to_hdf(OUTPATH+"{0}/heat_filter2_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat_band', mode='w')
    pd_heat_band3[i].to_hdf(OUTPATH+"{0}/heat_filter3_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat_band', mode='w')
    pd_heat_band4[i].to_hdf(OUTPATH+"{0}/heat_filter4_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat_band', mode='w')
    pd_heat_band5[i].to_hdf(OUTPATH+"{0}/heat_filter5_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat_band', mode='w')
    pd_heat_band6[i].to_hdf(OUTPATH+"{0}/heat_filter6_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat_band', mode='w')
    pd_heat_band7[i].to_hdf(OUTPATH+"{0}/heat_filter7_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat_band', mode='w')
    pd_light_band[i].to_hdf(OUTPATH+"{0}/light_filter_ch{1}_{2}.h5".format(runN, i, subrunNum), key='light_band', mode='w')
    print (i)
    #df.to_hdf('test_pd_h5.h5', key='df', mode='w')                                                                                                                                                                                                                                                
