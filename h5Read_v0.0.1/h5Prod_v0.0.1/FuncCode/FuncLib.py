import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import *
from ROOT import TFile, TTree, TChain
import pandas as pd
from scipy.signal import *
from array import array
from scipy.signal import find_peaks
import h5py

###Reading pyroot File###
def pyroot_file_read(file_input):
    tr = TChain("event")
    tr.Add(file_input)

    return tr

def pyroot_read(tr, n_evt, base_start, base_end, sampling_rate) :

    tr.GetEntry(n_evt)
    nmod = 4
    ch_heat = [[] for _ in range(nmod)]
    ch_light = [[] for _ in range(nmod)]
    time_heat = [[] for _ in range(nmod)]
    time_light = [[] for _ in range(nmod)]
    trgon = [[] for _ in range(nmod*2)]
    tmp_heat = [[] for _ in range(nmod)]
    tmp_light = [[] for _ in range(nmod)]
    tmpRMS_heat = [[] for _ in range(nmod)]
    tmpRMS_light = [[] for _ in range(nmod)]

    ch_heat[0] = array('i', tr.ch_1)
    ch_heat[1] = array('i', tr.ch_3)
    ch_heat[2] = array('i', tr.ch_5)
    ch_heat[3] = array('i', tr.ch_7)
    #ch_heat[4] = array('i', tr.ch_9)
    #ch_heat[5] = array('i', tr.ch_11)
    #ch_heat[6] = array('i', tr.ch_13)
    #ch_heat[7] = array('i', tr.ch_15)
    ch_light[0] = array('i', tr.ch_0)
    ch_light[1] = array('i', tr.ch_2)
    ch_light[2] = array('i', tr.ch_4)
    ch_light[3] = array('i', tr.ch_6)
    #ch_light[4] = array('i', tr.ch_8)
    #ch_light[5] = array('i', tr.ch_10)
    #ch_light[6] = array('i', tr.ch_12)
    #ch_light[7] = array('i', tr.ch_14)
    time_heat[0] = tr.t_1
    time_heat[1] = tr.t_3
    time_heat[2] = tr.t_5
    time_heat[3] = tr.t_7
    #time_heat[4] = tr.t_9
    #time_heat[5] = tr.t_11
    #time_heat[6] = tr.t_13
    #time_heat[7] = tr.t_15
    time_light[0] = tr.t_0
    time_light[1] = tr.t_2
    time_light[2] = tr.t_4
    time_light[3] = tr.t_6
    #time_light[4] = tr.t_8
    #time_light[5] = tr.t_10
    #time_light[6] = tr.t_12
    #time_light[7] = tr.t_14
    trgon[0] = tr.trgon_1
    trgon[1] = tr.trgon_3
    trgon[2] = tr.trgon_5
    trgon[3] = tr.trgon_7
    #trgon[4] = tr.trgon_9
    #trgon[5] = tr.trgon_11
    #trgon[6] = tr.trgon_13
    tmp_heat[0] = tr.tmp_1
    tmp_heat[1] = tr.tmp_3
    tmp_heat[2] = tr.tmp_5
    tmp_heat[3] = tr.tmp_7
    tmp_light[0] = tr.tmp_0
    tmp_light[1] = tr.tmp_2
    tmp_light[2] = tr.tmp_4
    tmp_light[3] = tr.tmp_6
    tmpRMS_heat[0] = tr.tmpRMS_1
    tmpRMS_heat[1] = tr.tmpRMS_3
    tmpRMS_heat[2] = tr.tmpRMS_5
    tmpRMS_heat[3] = tr.tmpRMS_7
    tmpRMS_light[0] = tr.tmpRMS_0
    tmpRMS_light[1] = tr.tmpRMS_2
    tmpRMS_light[2] = tr.tmpRMS_4
    tmpRMS_light[3] = tr.tmpRMS_6

    for i in range(nmod) :
        ch_heat[i] = np.array(ch_heat[i]).astype(int)
        ch_light[i] = np.array(ch_light[i]).astype(int)
        time_heat[i] = np.array(time_heat[i]).astype(float)
        time_light[i] = np.array(time_light[i]).astype(float)
        trgon[i] = np.array(trgon[i]).astype(int)
        tmp_heat[i] = np.array(tmp_heat[i]).astype(float)
        tmp_light[i] = np.array(tmp_light[i]).astype(float)
        tmpRMS_heat[i] = np.array(tmpRMS_heat[i]).astype(float)
        tmpRMS_light[i] = np.array(tmpRMS_light[i]).astype(float)

    base_heat = [[] for _ in range(nmod)]
    base_light = [[] for _ in range(nmod)]
    baseRMS_heat = [[] for _ in range(nmod)]
    baseRMS_light = [[] for _ in range(nmod)]

    for i in range(nmod) :
        base_heat[i] = np.mean(ch_heat[i][base_start:base_end])
        base_light[i] = np.mean(ch_light[i][base_start:base_end])

        baseRMS_heat[i] = np.std(ch_heat[i][base_start:base_end])
        baseRMS_light[i] = np.std(ch_light[i][base_start:base_end])

    result = [ch_heat, ch_light, time_heat, time_light, base_heat, base_light, baseRMS_heat, baseRMS_light, trgon, tmp_heat, tmp_light, tmpRMS_heat, tmpRMS_light]

    return result

###ButterWorth bandpass filter###
def butter_bandpass_sos(lowcut, highcut, fs, order =5):
    nyq = 0.5*fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = butter(order, [low, high], btype='band', output='sos')
    return sos

def butter_bandpass_filter_sos(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass_sos(lowcut, highcut, fs, order=order)
    y = sosfilt(sos, data)
    return y

###ploynominal fuction###
def poly2(x, a, b, c):
    return a*x**2 + b*x +c
###ploynominal fuction###
def poly2_1(x, a, b, c):
    return a*(x-b)**2+c

###Example READ PROD FILEs using H5py
def READ5PY(h5file) :
    with h5py.File(h5file, 'r') as f :
        print (f.keys)
        dset = f['wave']

###HEAT Channel anaylsis###
def heat_ana(heat_array, conversion_factor, sampling_rate, baseline, baselineRMS, base_end) :
   
    try :
        #pulse height and height point
        grad_heat_array = np.gradient(heat_array)
        n_heat_array= movingaverage(grad_heat_array, 1)

        argtrg = np.argmax(n_heat_array[base_end:base_end+700]) + base_end
        #argtrg = int(argtrg - 30) 

        pedestal = np.mean(heat_array[argtrg-100:argtrg])
        hmin = np.min(heat_array[argtrg:argtrg+1000])
        peaks, _ = find_peaks(heat_array[argtrg:argtrg+700], height=pedestal+500)
        xhpeaks = peaks+argtrg
        pedestal = np.mean(heat_array[17600:17700])
        hpeak = np.max(heat_array[xhpeaks]) - pedestal
        #hpeak = np.max(heat_array[xhpeaks]) - np.mean(heat_array[17600:17800])
        arghpeak = np.argmax(heat_array[xhpeaks])
        xhpeak = xhpeaks[arghpeak]
        
        #pulse peak point finder more detail using fitting
        rt100_x = np.linspace(xhpeak-100, xhpeak+100, 100*2)
        popt1, pcov1 = curve_fit(poly2_1, rt100_x, heat_array[xhpeak-100:xhpeak+100], p0=[-1, xhpeak, hpeak])
        
        xhpeak = int(popt1[1])
        hpeak = popt1[2] - pedestal
        
        rt90 = hpeak*0.9+pedestal
        arg_rt90 = np.argmin(abs(heat_array[17700:xhpeak] - rt90))+17700

        rt50 = hpeak*0.5+pedestal
        arg_rt50 = np.argmin(abs(heat_array[17700:xhpeak] - rt50))+17700

        rt10 = hpeak*0.1+pedestal
        arg_rt10 = np.argmin(abs(heat_array[17700:xhpeak] - rt10))+17700
        
        #print (rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10)
        ##for risetime point fitting
        fit_term = 5
        
        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2
        
        rt90_x = np.linspace(arg_rt90-fit_term, arg_rt90+fit_term, 2*fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, heat_array[arg_rt90-fit_term:arg_rt90+fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90-fit_term, arg_rt90+fit_term), method='bounded', args=tuple(popt1))
        rt90_f = res90.x
        
        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2
        
        rt50_x = np.linspace(arg_rt50-fit_term, arg_rt50+fit_term, 2*fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, heat_array[arg_rt50-fit_term:arg_rt50+fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50-fit_term, arg_rt50+fit_term), method='bounded', args=tuple(popt2))
        rt50_f = res50.x
        
        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2
        
        rt10_x = np.linspace(arg_rt10-fit_term, arg_rt10+fit_term, 2*fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, heat_array[arg_rt10-fit_term:arg_rt10+fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10-fit_term, arg_rt10+fit_term), method='bounded', args=tuple(popt3))
        rt10_f = res10.x

        ###End baseline
        endline = np.mean(heat_array[-2000:])
        endlineRMS = np.std(heat_array[-2000:])
        
        result = [xhpeak, hpeak, rt90_f, rt50_f, rt10_f, rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10, endline, endlineRMS, hmin, pedestal]
    
    except ValueError :
        result = [-1]*15
        
    except RuntimeError :
        
        #pulse height and height point
        grad_heat_array = np.gradient(heat_array)
        n_heat_array= movingaverage(grad_heat_array, 1)

        argtrg = np.argmax(n_heat_array[base_end:base_end+700]) + base_end
        #argtrg = int(argtrg - 30) 

        pedestal = np.mean(heat_array[argtrg-100:argtrg])
        hmin = np.min(heat_array[argtrg:argtrg+1000])
        peaks, _ = find_peaks(heat_array[argtrg:argtrg+700], height=pedestal+500)
        xhpeaks = peaks+argtrg
        pedestal = np.mean(heat_array[17600:17700])
        hpeak = np.max(heat_array[xhpeaks]) - pedestal
        #hpeak = np.max(heat_array[xhpeaks]) - np.mean(heat_array[17600:17800])
        arghpeak = np.argmax(heat_array[xhpeaks])
        xhpeak = xhpeaks[arghpeak]
        
        rt90 = hpeak*0.9+pedestal
        arg_rt90 = np.argmin(abs(heat_array[17700:xhpeak] - rt90))+17700

        rt50 = hpeak*0.5+pedestal
        arg_rt50 = np.argmin(abs(heat_array[17700:xhpeak] - rt50))+17700

        rt10 = hpeak*0.1+pedestal
        arg_rt10 = np.argmin(abs(heat_array[17700:xhpeak] - rt10))+17700
        
        #print (rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10)
        ##for risetime point fitting
        fit_term = 5
        
        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2
        
        rt90_x = np.linspace(arg_rt90-fit_term, arg_rt90+fit_term, 2*fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, heat_array[arg_rt90-fit_term:arg_rt90+fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90-fit_term, arg_rt90+fit_term), method='bounded', args=tuple(popt1))
        rt90_f = res90.x
        
        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2
        
        rt50_x = np.linspace(arg_rt50-fit_term, arg_rt50+fit_term, 2*fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, heat_array[arg_rt50-fit_term:arg_rt50+fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50-fit_term, arg_rt50+fit_term), method='bounded', args=tuple(popt2))
        rt50_f = res50.x
        
        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2
        
        rt10_x = np.linspace(arg_rt10-fit_term, arg_rt10+fit_term, 2*fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, heat_array[arg_rt10-fit_term:arg_rt10+fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10-fit_term, arg_rt10+fit_term), method='bounded', args=tuple(popt3))
        rt10_f = res10.x

        ###End baseline
        endline = np.mean(heat_array[-2000:])
        endlineRMS = np.std(heat_array[-2000:])
        
        result = [xhpeak, hpeak, rt90_f, rt50_f, rt10_f, rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10, endline, endlineRMS, hmin, pedestal]
    return result

###LIGHT Channel analysis###
   
def light_ana(light_array, conversion_factor, sampling_rate, baseline, baselineRMS, base_end) :

    try :
        #pulse height and height point
        grad_light_array = np.gradient(light_array)
        n_light_array= movingaverage(grad_light_array, 40)

        argtrg = np.argmax(n_light_array[base_end:base_end+1500]) + base_end
        argtrg = int(argtrg - 70) 

        pedestal = np.mean(light_array[argtrg-500:argtrg])
        lmin = np.min(light_array[argtrg:argtrg+1000])
        peaks, _ = find_peaks(light_array[argtrg:argtrg+1500], height=pedestal+10)
        xlpeaks = peaks+argtrg
        lpeak = np.max(light_array[xlpeaks]) - pedestal
        arglpeak = np.argmax(light_array[xlpeaks])
        xlpeak = xlpeaks[arglpeak]
        #result = [xlpeak, lpeak]
        
        #For Risetime
        rt90 = lpeak*0.9+pedestal
        arg_rt90 = np.argmin(abs(light_array[base_end:xlpeak] - rt90))+base_end

        rt50 = lpeak*0.5+pedestal
        arg_rt50 = np.argmin(abs(light_array[base_end:xlpeak] - rt50))+base_end

        rt10 = lpeak*0.1+pedestal
        arg_rt10 = np.argmin(abs(light_array[base_end:xlpeak] - rt10))+base_end
        
        ##for risetime point fitting
        fit_term = 5

        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2
        
        rt90_x = np.linspace(arg_rt90-fit_term, arg_rt90+fit_term, 2*fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, light_array[arg_rt90-fit_term:arg_rt90+fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90-fit_term, arg_rt90+fit_term), method='bounded', args=tuple(popt1))
        rt90_f = res90.x
        
        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2
        
        rt50_x = np.linspace(arg_rt50-fit_term, arg_rt50+fit_term, 2*fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, light_array[arg_rt50-fit_term:arg_rt50+fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50-fit_term, arg_rt50+fit_term), method='bounded', args=tuple(popt2))
        rt50_f = res50.x
        
        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2
        
        rt10_x = np.linspace(arg_rt10-fit_term, arg_rt10+fit_term, 2*fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, light_array[arg_rt10-fit_term:arg_rt10+fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10-fit_term, arg_rt10+fit_term), method='bounded', args=tuple(popt3))
        rt10_f = res10.x
    
        ###End baseline
        endline = np.mean(light_array[-2000:])
        endlineRMS = np.std(light_array[-2000:])
        result = [xlpeak, lpeak, rt90_f, rt50_f, rt10_f, rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10, endline, endlineRMS, lmin, pedestal]
    
    except ValueError :
        result = [-1]*15
        
    return result

def light_ana_amore(light_array, conversion_factor, sampling_rate, x1, x2):
    try : 
        move_avg = 15

        diff_array = np.diff(movingaverage(light_array[x1:x2], move_avg))
        diff_array = diff_array[move_avg:-move_avg]
        diffmaxx = np.argmax(diff_array) + x1 + move_avg 
    
        grad_array = np.gradient(movingaverage(light_array[x1:x2], move_avg))
        grad_array = grad_array[move_avg:-move_avg]
        gradmaxx = np.argmax(grad_array) + x1 + move_avg

        diffmax = light_array[diffmaxx]
        gradmax = light_array[gradmaxx]

        #pulse height and height point

        base_end = gradmaxx - 400
        pedestal = np.mean(light_array[base_end-100:base_end])
        lmin = np.min(light_array[gradmaxx:gradmaxx+1000])
        peaks, _ = find_peaks(light_array[gradmaxx:gradmaxx+1000], height=pedestal+10)
        xlpeaks = peaks+x1
        lpeak = np.max(light_array[xlpeaks]) - pedestal
        arglpeak = np.argmax(light_array[xlpeaks])
        xlpeak = xlpeaks[arglpeak]

        #For Risetime
        rt90 = lpeak*0.9+pedestal
        arg_rt90 = np.argmin(abs(light_array[base_end:xlpeak] - rt90))+base_end

        rt50 = lpeak*0.5+pedestal
        arg_rt50 = np.argmin(abs(light_array[base_end:xlpeak] - rt50))+base_end

        rt10 = lpeak*0.1+pedestal
        arg_rt10 = np.argmin(abs(light_array[base_end:xlpeak] - rt10))+base_end
        
        ##for risetime point fitting
        fit_term = 5

        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2
        
        rt90_x = np.linspace(arg_rt90-fit_term, arg_rt90+fit_term, 2*fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, light_array[arg_rt90-fit_term:arg_rt90+fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90-fit_term, arg_rt90+fit_term), method='bounded', args=tuple(popt1))
        rt90_f = res90.x
        
        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2
        
        rt50_x = np.linspace(arg_rt50-fit_term, arg_rt50+fit_term, 2*fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, light_array[arg_rt50-fit_term:arg_rt50+fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50-fit_term, arg_rt50+fit_term), method='bounded', args=tuple(popt2))
        rt50_f = res50.x
        
        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2
        
        rt10_x = np.linspace(arg_rt10-fit_term, arg_rt10+fit_term, 2*fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, light_array[arg_rt10-fit_term:arg_rt10+fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10-fit_term, arg_rt10+fit_term), method='bounded', args=tuple(popt3))
        rt10_f = res10.x
    
        ###End baseline
        endline = np.mean(light_array[-2000:])
        endlineRMS = np.std(light_array[-2000:])
        result = [xlpeak, lpeak, rt90_f, rt50_f, rt10_f, rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10, endline, endlineRMS, lmin, pedestal, diffmaxx, diffmax, gradmaxx, gradmax]
    
    except ValueError :
        result = [-1]*19
        
    return result

#def light_ana(light_array, conversion_factor, sampling_rate, baseline, baselineRMS, base_end) :
#    try :
#        lpeak = np.max(light_array[base_end: base_end+1000]) - baseline
#        xlpeak = np.argmax(light_array[base_end : base_end+1000]) +base_end
#        result = [xlpeak, lpeak]
#    
#    except ValueError :
#        result = [-1. -1]
#      
#    return result

###HEAT Channel filter analysis###
def heat_filter_ana(lowcut, highcut, order, heat_array, conversion_factor, sampling_rate, base_end) :
    try :
        #print ("in func ----- nofileter : ", heat_array[0:10])
        y = butter_bandpass_filter_sos(heat_array, lowcut, highcut, sampling_rate, order=order)
        #print ("in func ----- fileter : ", y[0:10]) 
        fbaseline = np.mean(y[0:15000])
        fbaselineRMS = np.std(y[0:15000])
        
        hpeak = np.max(y[17000:20000])
        xhpeak = np.argmax(y[17000:20000])
        xhpeak = xhpeak + 17000
        
        yy = 9999
        xx = 0
        
        for ii in range(xhpeak, xhpeak+1000, 1) :
            #if (y[ii] > yy) : break
            if (y[ii] < yy) :
                yy = y[ii]
                xx = ii
        
        lfmin = yy
        xlfmin = xx
        
        hpeak2nd = np.max(y[xlfmin:xlfmin+1000])
        xhpeak2nd = np.argmax(y[xlfmin:xlfmin+1000]) + xlfmin
                
        result = [xhpeak, hpeak, fbaseline, fbaselineRMS, xlfmin, lfmin, xhpeak2nd, hpeak2nd]
        
    except ValueError :
        result = [-1]*8
        
    return result

def heat_filter_ana_1(lowcut, highcut, order, heat_array, conversion_factor, sampling_rate, base_end) :
    try :
        #print ("in func ----- nofileter : ", heat_array[0:10])
        y = butter_bandpass_filter_sos(heat_array, lowcut, highcut, sampling_rate, order=order)
        #print ("in func ----- fileter : ", y[0:10]) 
        grad_array = np.gradient(y)
        n_array= movingaverage(grad_array, 1)

        argtrg = np.argmax(n_array[500:1000])
        
        fbaseline = np.mean(y[argtrg-100:argtrg-50])
        fbaselineRMS = np.std(y[argtrg-100:argtrg-50])

        peaks, _ = find_peaks(y[argtrg:argtrg+700], height=fbaseline+100)
        xhpeaks = peaks+argtrg
        hpeak = np.max(y[xhpeaks]) - fbaseline
        arghpeak = np.argmax(y[xhpeaks]) 
        xhpeak = xhpeaks[arghpeak]
        
        yy = 9999
        xx = 0
        
        for ii in range(xhpeak, xhpeak+500, 1) :
            #if (y[ii] > yy) : break
            if (y[ii] < yy) :
                yy = y[ii]
                xx = ii
        
        lfmin = yy
        xlfmin = xx
        
        hpeak2nd = np.max(y[xlfmin:xlfmin+500])
        xhpeak2nd = np.argmax(y[xlfmin:xlfmin+500]) + xlfmin
        
        result = [xhpeak, hpeak, fbaseline, fbaselineRMS, xlfmin, lfmin, xhpeak2nd, hpeak2nd]
        
    except ValueError :
        result = [-1]*8
        
    return result

###LIGHT Channel filter analysis###
def light_filter_ana(lowcut, highcut, order, light_array, conversion_factor, sampling_rate, base_end) :
    try :
        y = butter_bandpass_filter_sos(light_array-light_array[0], lowcut, highcut, sampling_rate, order=order)
        
        fbaseline = np.mean(y[int(base_end)-1000:int(base_end)])
        fbaselineRMS = np.std(y[int(base_end)-1000:int(base_end)])

        peaks, _ = find_peaks(y[base_end:base_end+1000], height=fbaseline+10)
        xhpeaks = peaks+base_end
        hpeak = np.max(y[xhpeaks])
        hpeak1 = np.max(y[xhpeaks]) - fbaseline
        arghpeak = np.argmax(y[xhpeaks])
        xhpeak = xhpeaks[arghpeak]
        
        yy = 9999
        xx = 0
        
        for ii in range(xhpeak, xhpeak+1000, 1) :
            if (y[ii] < yy) :
                yy = y[ii]
                xx = ii
        
        lfmin = yy
        xlfmin = xx
        
        hpeak2nd = np.max(y[xlfmin:xlfmin+1000])
        xhpeak2nd = np.argmax(y[xlfmin:xlfmin+1000]) + xlfmin
        
        result = [xhpeak, hpeak, fbaseline, fbaselineRMS, xlfmin, lfmin, xhpeak2nd, hpeak2nd, hpeak1]

        
    except ValueError :
        result = [-1]*9
        
    return result

def ch_filter_risetime(light_array, conversion_factor, sampling_rate, lowcut, highcut, order, base_end):
    try : 
        #pulse height and height point
        y = butter_bandpass_filter_sos(light_array-light_array[0], lowcut, highcut, sampling_rate, order=order)
        
        fbaseline = np.mean(y[7000:7700])
        fbaselineRMS = np.std(y[7000:7700])

        pedestal = fbaseline
        peaks, _ = find_peaks(y[base_end:base_end+1000], height=fbaseline+10)
        xlpeaks = peaks+base_end
        lpeak = np.max(y[xlpeaks])
        arglpeak = np.argmax(y[xlpeaks])
        xlpeak = xlpeaks[arglpeak]
        #lpeak = np.max(y[base_end:base_end+1000]) - pedestal
        #xlpeak = np.argmax(y[base_end:base_end+1000]) + base_end

        #For Risetime
        rt90 = lpeak*0.9
        arg_rt90 = np.argmin(abs(y[base_end:xlpeak] - rt90))+base_end

        rt50 = lpeak*0.5
        arg_rt50 = np.argmin(abs(y[base_end:xlpeak] - rt50))+base_end

        rt10 = lpeak*0.1
        arg_rt10 = np.argmin(abs(y[base_end:xlpeak] - rt10))+base_end
        
        ##for risetime point fitting
        fit_term = 5

        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2
        
        rt90_x = np.linspace(arg_rt90-fit_term, arg_rt90+fit_term, 2*fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, y[arg_rt90-fit_term:arg_rt90+fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90-fit_term, arg_rt90+fit_term), method='bounded', args=tuple(popt1))
        rt90_f = res90.x
        
        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2
        
        rt50_x = np.linspace(arg_rt50-fit_term, arg_rt50+fit_term, 2*fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, y[arg_rt50-fit_term:arg_rt50+fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50-fit_term, arg_rt50+fit_term), method='bounded', args=tuple(popt2))
        rt50_f = res50.x
        
        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2
        
        rt10_x = np.linspace(arg_rt10-fit_term, arg_rt10+fit_term, 2*fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, y[arg_rt10-fit_term:arg_rt10+fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10-fit_term, arg_rt10+fit_term), method='bounded', args=tuple(popt3))
        rt10_f = res10.x
    
        result = [xlpeak, lpeak, rt90_f, rt50_f, rt10_f, rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10]
    
    except ValueError :
        result = [-1]*11
        
    return result

    
###For Template Fitting def###
def template_fitting(axx, amb_array, tmp_axx, tmp_array, window0, window1, xpeak10_b, xpeak10) :
    try :
        OF_window0_b = int(xpeak10_b)-window0
        OF_window1_b = int(xpeak10_b)+window1
        
        axx_tmp_b = tmp_axx[OF_window0_b:OF_window1_b]-OF_window0_b
        ayy_tmp_b = tmp_array[OF_window0_b:OF_window1_b]
    
        OF_window0 = int(xpeak10)-window0
        OF_window1 = int(xpeak10)+window1

        axx_tmp = axx[OF_window0:OF_window1]-OF_window0
        ayy_tmp = amb_array[OF_window0:OF_window1]


        S_b = np.sum(ayy_tmp*ayy_tmp_b)/np.sum(ayy_tmp_b**2)
    
        samples = window1
        ayy_chi = ayy_tmp[window0:]
        ayy_chi_b = ayy_tmp_b[window0:]*S_b
    
        chi_b = np.sum((ayy_chi-ayy_chi_b)**2/ayy_chi_b)/(samples-1)
        
        result = [S_b, chi_b]
        
    except ValueError : 
        result = [-1]*2
        
    return result

###GET template fitting parameter###
def heat_ana_temp(heat_array, conversion_factor, sampling_rate, baseline, baselineRMS, base_end) :
   
        #pulse height and height point
        peaks, _ = find_peaks(heat_array[base_end:base_end+500], height=baseline+0.5)
        xhpeaks = peaks+base_end
        hpeak = np.max(heat_array[xhpeaks]) - baseline
        arghpeak = np.argmax(heat_array[xhpeaks])
        xhpeak = xhpeaks[arghpeak]
        result = [xhpeak, hpeak]
        
        #For Risetime
        rt90 = hpeak*0.9+baseline
        arg_rt90 = np.argmin(abs(heat_array[base_end:xhpeak] - rt90))+base_end

        rt50 = hpeak*0.5+baseline
        arg_rt50 = np.argmin(abs(heat_array[base_end:xhpeak] - rt50))+base_end

        rt10 = hpeak*0.1+baseline
        arg_rt10 = np.argmin(abs(heat_array[base_end:xhpeak] - rt10))+base_end
        
        ##for risetime point fitting
        fit_term = 5

        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2
        
        rt90_x = np.linspace(arg_rt90-fit_term, arg_rt90+fit_term, 2*fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, heat_array[arg_rt90-fit_term:arg_rt90+fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90-fit_term, arg_rt90+fit_term), method='bounded', args=tuple(popt1))
        rt90_f = res90.x
        
        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2
        
        rt50_x = np.linspace(arg_rt50-fit_term, arg_rt50+fit_term, 2*fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, heat_array[arg_rt50-fit_term:arg_rt50+fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50-fit_term, arg_rt50+fit_term), method='bounded', args=tuple(popt2))
        rt50_f = res50.x
        
        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2
        
        rt10_x = np.linspace(arg_rt10-fit_term, arg_rt10+fit_term, 2*fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, heat_array[arg_rt10-fit_term:arg_rt10+fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10-fit_term, arg_rt10+fit_term), method='bounded', args=tuple(popt3))
        rt10_f = res10.x
        
        result = [xhpeak, hpeak, rt90_f, rt50_f, rt10_f]
         
        return result

def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')
