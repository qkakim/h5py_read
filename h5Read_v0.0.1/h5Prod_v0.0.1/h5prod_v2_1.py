import pandas as pd
import numpy as np
import os, sys, re, h5py
import matplotlib.pyplot as plt
import time

from scipy.optimize import *
from scipy.integrate import *
from scipy.signal import *

import FuncLib20211st as Func

var1 = sys.argv[1]
var2 = sys.argv[2]
var3 = sys.argv[3]
var4 = sys.argv[4]

ROOT_PATH = var1
OUTPATH = var2
subrunNum = var3
runN = var4

start_execute = time.time()

hlowcut = 30
hhighcut = 220
llowcut = 200
lhighcut = 500
fs = 100e+3
order = 1
conversion_factor = 10./2**18
base_start = 15200
base_end = 17000
nmod = 4

pd_heat = [pd.DataFrame({'time' : [0], 'xhpeak' : [0], 'hpeak' : [0], 'hrt90_h' : [0], 'hrt50_h' : [0], 'hrt10_h' : [0], 'hrt90' : [0], 'hrt50' : [0], 'hrt10' : [0], 'hpedestal' : [0], 'hendline' : [0], 'hendlineRMS' : [0], 'hbaseline' : [0], 'hbaselineRMS' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0], 'pedestal' : [0], 'pedestalRMS' : [0]}) for _ in range(4)]
pd_light = [pd.DataFrame({'time' : [0], 'xlpeak' : [0], 'lpeak' : [0], 'lrt90_h' : [0], 'lrt50_h' : [0], 'lrt10_h' : [0], 'lrt90' : [0], 'lrt50' : [0], 'lrt10' : [0], 'lpedestal' : [0], 'lendline' : [0], 'lendlineRMS' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0]}) for _ in range(4)]
pd_heat_band1 = [pd.DataFrame({'time' : [0], 'xhfpeak' : [0], 'hfpeak' : [0], 'hfbaseline': [0], 'hfbaselineRMS' : [0], 'xhfmin' : [0], 'hfmin' : [0], 'xhfpeak2nd' : [0], 'hfpeak2nd' : [0], 'fit_xhfpeak' :[0], 'fit_hfpeak' : [0], 'fit_xhfmin' : [0], 'fit_hfmin' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0]}) for _ in range(4)]
pd_heat_band2 = [pd.DataFrame({'time' : [0], 'xhfpeak' : [0], 'hfpeak' : [0], 'hfbaseline': [0], 'hfbaselineRMS' : [0], 'xhfmin' : [0], 'hfmin' : [0], 'xhfpeak2nd' : [0], 'hfpeak2nd' : [0], 'fit_xhfpeak' :[0], 'fit_hfpeak' : [0], 'fit_xhfmin' : [0], 'fit_hfmin' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0]}) for _ in range(4)]
pd_heat_band3 = [pd.DataFrame({'time' : [0], 'xhfpeak' : [0], 'hfpeak' : [0], 'hfbaseline': [0], 'hfbaselineRMS' : [0], 'xhfmin' : [0], 'hfmin' : [0], 'xhfpeak2nd' : [0], 'hfpeak2nd' : [0], 'fit_xhfpeak' :[0], 'fit_hfpeak' : [0], 'fit_xhfmin' : [0], 'fit_hfmin' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0]}) for _ in range(4)]
pd_heat_band4 = [pd.DataFrame({'time' : [0], 'xhfpeak' : [0], 'hfpeak' : [0], 'hfbaseline': [0], 'hfbaselineRMS' : [0], 'xhfmin' : [0], 'hfmin' : [0], 'xhfpeak2nd' : [0], 'hfpeak2nd' : [0], 'fit_xhfpeak' :[0], 'fit_hfpeak' : [0], 'fit_xhfmin' : [0], 'fit_hfmin' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0]}) for _ in range(4)]
pd_light_band = [pd.DataFrame({'time': [0], 'xlfpeak' : [0], 'lfpeak' : [0], 'lfbaseline': [0], 'lfbaselineRMS' : [0], 'xlfmin' : [0], 'lfmin' : [0], 'xlfpeak2nd' : [0], 'lfpeak2nd' : [0], 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [0], 'subrun' : [0]}) for _ in range(4)]

#ROOT_PATH = '/data/AMoRE/users/kimwootae/PROD/2020_2nd_RODY/20201030/20201030_0.root'
ROOT_FILE = ROOT_PATH+'{0}_{1}_THR100.root'.format(runN, subrunNum)

tr_input = Func.pyroot_file_read(ROOT_FILE)
print(ROOT_FILE)
nentries = tr_input.GetEntries()
print (nentries)
for i in range(nentries) :

    if (i%100==0 and i>0) :
        inter_execute = time.time()
        inter_run_time = inter_execute - start_execute
        print ("{0}, run time:{1}".format(i, inter_run_time))

    evtnumber = i
    ch_heat, ch_light, time_heat, time_light, base_heat, base_light, baseRMS_heat, baseRMS_light, trgON, tmp_heat, tmp_light, tmpRMS_heat, tmpRMS_light = Func.pyroot_read(tr_input, evtnumber, base_start, base_end, fs)

    #if (i > 200) : break
    
    for ii in range(nmod) :
        trg = trgON[ii]
        if(trg == 0) : continue
        
        h = np.array(ch_heat[ii])
        l = np.array(ch_light[ii])
        h_mean = np.mean(h[5000:15000])
        h_std = np.std(h[5000:15000])
        h_ped = np.mean(h[17500:17900])
        h_pedRMS = np.std(h[17500:17900])
        #heat channel ana
        h_ana = Func.heat_ana(h, conversion_factor, fs, np.mean(h[14000:17000]), np.std(h[14000:17000]), base_end)
        #heat channel bandpass filter ana
        h_band_ana1 = Func.heat_filter_ana(200, 500, order, h-h[0], conversion_factor, fs, base_end)
        h_band_ana2 = Func.heat_filter_ana(500, 800, order, h-h[0], conversion_factor, fs, base_end) 
        h_band_ana3 = Func.heat_filter_ana(1000, 1500, order, h-h[0], conversion_factor, fs, base_end)
        h_band_ana4 = Func.heat_filter_ana(40, 220, order, h-h[0], conversion_factor, fs, base_end)
        
        #temporary cut
        if (h_ana[2] == -1) : continue
        if ((h_ana[2] - h_ana[4]) < 0) : continue
        if ((h_ana[5] - h_ana[7]) < 0) : continue
        if (h_band_ana1[1] < 0) : continue

        #light channel ana
        l_ana = Func.light_ana(l, conversion_factor, fs, np.mean(l[14000:17000]), np.std(l[14000:17000]), base_end)
        #light channel bandpass filter ana
        l_band_ana = Func.light_filter_ana(llowcut, lhighcut, order, l, conversion_factor, fs, base_end)
        pd_h_ana = pd.DataFrame({'time' : [time_heat[ii]], 'xhpeak' : [h_ana[0]], 'hpeak' : [h_ana[1]]\
                                 , 'hrt90_h' : [h_ana[2]], 'hrt50_h' : [h_ana[3]], 'hrt10_h' : [h_ana[4]]\
                                 , 'hrt90' : [h_ana[5]], 'hrt50' : [h_ana[6]], 'hrt10' : [h_ana[7]]\
                                 , 'hbaseline' : [h_mean], 'hendline' : [h_ana[11]], 'hendlineRMS' : [h_ana[12]], 'hbaselineRMS' : [h_std]\
                                 , 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [evtnumber], 'subrun' : [subrunNum]\
                                 , 'pedestal' : [h_ped], 'pedestalRMS' : [h_pedRMS]})
        pd_l_ana = pd.DataFrame({'time' : [time_light[ii]], 'xlpeak' : [l_ana[0]], 'lpeak' : [l_ana[1]]\
                                 , 'lrt90_h' : [l_ana[2]], 'lrt50_h' : [l_ana[3]], 'lrt10_h' : [l_ana[3]]\
                                 , 'lrt90' : [l_ana[4]], 'lrt50' : [l_ana[5]], 'lrt10' : [l_ana[6]]\
                                 , 'lpedestal' : [l_ana[14]], 'lendline' : [l_ana[11]], 'lendlineRMS' : [l_ana[12]]\
                                 , 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [evtnumber], 'subrun' : [subrunNum]})
        pd_h_band_ana1 = pd.DataFrame({'time' : [time_heat[ii]], 'xhfpeak' : [h_band_ana1[0]], 'hfpeak' : [h_band_ana1[1]], 'hfbaseline' : [h_band_ana1[2]], 'hfbaselineRMS' : [h_band_ana1[3]]\
                                      , 'xhfmin' : [h_band_ana1[4]], 'hfmin' : [h_band_ana1[5]], 'xhfpeak2nd' : [h_band_ana1[6]], 'hfpeak2nd' : [h_band_ana1[7]]\
                                      , 'fit_xhfpeak' :[h_band_ana1[8]], 'fit_hfpeak' : [h_band_ana1[9]], 'fit_xhfmin' : [h_band_ana1[10]], 'fit_hfmin' : [h_band_ana1[11]]\
                                      , 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [evtnumber], 'subrun' : [subrunNum]})
        pd_h_band_ana2 = pd.DataFrame({'time' : [time_heat[ii]], 'xhfpeak' : [h_band_ana2[0]], 'hfpeak' : [h_band_ana2[1]], 'hfbaseline' : [h_band_ana2[2]], 'hfbaselineRMS' : [h_band_ana2[3]]\
                                      , 'xhfmin' : [h_band_ana2[4]], 'hfmin' : [h_band_ana2[5]], 'xhfpeak2nd' : [h_band_ana2[6]], 'hfpeak2nd' : [h_band_ana2[7]]\
                                      , 'fit_xhfpeak' :[h_band_ana2[8]], 'fit_hfpeak' : [h_band_ana2[9]], 'fit_xhfmin' : [h_band_ana2[10]], 'fit_hfmin' : [h_band_ana2[11]]\
                                      , 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [evtnumber], 'subrun' : [subrunNum]})
        pd_h_band_ana3 = pd.DataFrame({'time' : [time_heat[ii]], 'xhfpeak' : [h_band_ana3[0]], 'hfpeak' : [h_band_ana3[1]], 'hfbaseline' : [h_band_ana3[2]], 'hfbaselineRMS' : [h_band_ana3[3]]\
                                      , 'xhfmin' : [h_band_ana3[4]], 'hfmin' : [h_band_ana3[5]], 'xhfpeak2nd' : [h_band_ana3[6]], 'hfpeak2nd' : [h_band_ana3[7]]\
                                      , 'fit_xhfpeak' :[h_band_ana3[8]], 'fit_hfpeak' : [h_band_ana3[9]], 'fit_xhfmin' : [h_band_ana3[10]], 'fit_hfmin' : [h_band_ana3[11]]\
                                      , 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [evtnumber], 'subrun' : [subrunNum]})
        pd_h_band_ana4 = pd.DataFrame({'time' : [time_heat[ii]], 'xhfpeak' : [h_band_ana4[0]], 'hfpeak' : [h_band_ana4[1]], 'hfbaseline' : [h_band_ana4[2]], 'hfbaselineRMS' : [h_band_ana4[3]]\
                                      , 'xhfmin' : [h_band_ana4[4]], 'hfmin' : [h_band_ana4[5]], 'xhfpeak2nd' : [h_band_ana4[6]], 'hfpeak2nd' : [h_band_ana4[7]]\
                                      , 'fit_xhfpeak' :[h_band_ana4[8]], 'fit_hfpeak' : [h_band_ana4[9]], 'fit_xhfmin' : [h_band_ana4[10]], 'fit_hfmin' : [h_band_ana4[11]]\
                                      , 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [evtnumber], 'subrun' : [subrunNum]})
        pd_l_band_ana = pd.DataFrame({'time' : [time_light[ii]], 'xlfpeak' : [l_band_ana[0]], 'lfpeak' : [l_band_ana[1]], 'lfbaseline' : [l_band_ana[2]], 'lfbaselineRMS' : [l_band_ana[3]]\
                                      , 'xlfmin' : [l_band_ana[4]], 'lfmin' : [l_band_ana[5]], 'xlfpeak2nd' : [l_band_ana[6]], 'lfpeak2nd' : [l_band_ana[7]]\
                                      , 'tmp' : [0], 'tmpRMS' : [0], 'evtnum' : [evtnumber], 'subrun' : [subrunNum]})
        
        pd_heat[ii] = pd_heat[ii].append(pd_h_ana, ignore_index=True)
        pd_light[ii] = pd_light[ii].append(pd_l_ana, ignore_index=True)
        pd_heat_band1[ii] = pd_heat_band1[ii].append(pd_h_band_ana1, ignore_index=True)
        pd_heat_band2[ii] = pd_heat_band2[ii].append(pd_h_band_ana2, ignore_index=True)
        pd_heat_band3[ii] = pd_heat_band3[ii].append(pd_h_band_ana3, ignore_index=True)
        pd_heat_band4[ii] = pd_heat_band4[ii].append(pd_h_band_ana4, ignore_index=True)
        pd_light_band[ii] = pd_light_band[ii].append(pd_l_band_ana, ignore_index=True)
        
        #PATH = '/data/AMoRE/users/kimwootae/work/h5py_read/H5PROD_TEST/'
        #print (OUTPATH+"{0}/".format(runN))
for i in range(4) :
    pd_heat[i].to_hdf(OUTPATH+"{0}/heat_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat', mode='w')
    pd_light[i].to_hdf(OUTPATH+"{0}/light_ch{1}_{2}.h5".format(runN, i, subrunNum), key='light', mode='w')
    pd_heat_band1[i].to_hdf(OUTPATH+"{0}/heat_filter1_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat_band', mode='w')
    pd_heat_band2[i].to_hdf(OUTPATH+"{0}/heat_filter2_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat_band', mode='w')
    pd_heat_band3[i].to_hdf(OUTPATH+"{0}/heat_filter3_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat_band', mode='w')
    pd_heat_band4[i].to_hdf(OUTPATH+"{0}/heat_filter4_ch{1}_{2}.h5".format(runN, i, subrunNum), key='heat_band', mode='w')
    pd_light_band[i].to_hdf(OUTPATH+"{0}/light_filter_ch{1}_{2}.h5".format(runN, i, subrunNum), key='light_band', mode='w')
    print (i)
    #df.to_hdf('test_pd_h5.h5', key='df', mode='w')                                                                                                                                                                                                                                                
