import os
import sys
import glob

import subprocess
import threading


var1 = sys.argv[1]
runN = var1

'''Input run Number'''
#path = '/data/AMoRE/users/sckim/RODY/AMoRE_ADC/RAW/2019{0}/'.format(runNum)
path = '/data/AMoRE/users/kimwootae/work/h5py_read/h5Read_v0.0.1/h5Prod_v0.0.1'
prod_path = '/data/AMoRE/users/kimwootae/PROD/2021_1st_RODY/{0}/'.format(runN)
#prodfiles = path+'*light*{0}*root*'.format(var1)
prodfiles = prod_path+'*.root*'

exepath = "/data/AMoRE/users/kimwootae/work/h5py_read/h5Read_v0.0.1/h5Prod_v0.0.1/Shell/"
outpath = '/data/AMoRE/users/kimwootae/REPROD/2021_1st_RODY/'

def MakebashSh(prod_path, outpath, subrunNum, runN) :
    ROOT_PATH = prod_path
    OUTPUTPATH = outpath
    shName = exepath+'{0}_{1}.sh'.format(runN, subrunNum)
    with open(shName, 'w') as rsh :
        rsh.write('''\
##!/bin/bash
        
cd {0}

path={0}
subrunNum={1}
source /share/amore/anaconda3/etc/profile.d/conda.sh
source /share/amore/ROOT/6.14.04-v01.01/bin/thisroot.sh
source /home/kimwootae/env/pyroot.sh

conda activate v01
    
python {0}/h5prod_v2.py {1} {2} {3} {4}

'''.format(path, ROOT_PATH, OUTPUTPATH, subrunNum, runN))
    return shName

'''run bash script of QJOB'''
def JobShell(execute) :
    #subprocess.Popen(["python", "{0}".format(execute), "{0}".format(path), "{0}".format(subrunNum)])
    subprocess.Popen(["qsub -q medium {0}".format(execute)],shell=True)
    
'''threading function for parallel running script'''
def Threading(execute) :
    my_thread = threading.Thread(target=JobShell(execute))
    my_thread.start()
        
'''running function'''
def run() :
    files = sorted(glob.glob(prodfiles))
    n = len(files)
    print (files)
    shell_array = []
    for i in range(len(files)) :
        a = MakebashSh(prod_path, outpath, i, runN)
        shell_array.append(a)

    for i in range(len(files)) :
        Threading(shell_array[i])
        
    print (shell_array)
        
if __name__ == "__main__" :
    run()
                                                                                                                            
