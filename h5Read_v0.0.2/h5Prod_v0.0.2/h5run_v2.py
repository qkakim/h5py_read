import numpy as np
import h5py, os, glob, datetime, time, subprocess, threading, sys, psutil
import matplotlib.pyplot as plt
import pandas as pd

from array import array
from scipy.signal import *

import FuncLib_Objs as func_objs
import FuncLib_H5W as func_h5w

conversion_factor = 10. / 2 ** 18
sampling_rate = 100e3
dt_x = 35000  # region of calculation about decay component depend on crystal decay component
var1 = sys.argv[1]
var2 = sys.argv[2]
var3 = sys.argv[3]
var4 = sys.argv[4]
var5 = sys.argv[5]
var6 = sys.argv[6]
var7 = sys.argv[7]
var8 = sys.argv[8]
var9 = sys.argv[9]
var10 = sys.argv[10]
var11 = sys.argv[11]
var12 = sys.argv[12]
var13 = sys.argv[13]

h5_path = var1
runN = var2
mergeN = int(var3)
OUTPATH = var4
cut_file = var5
ch = var6
init = var7
fini = var8
cryst_name = var9
h_ref_file = var10
l_ref_file = var11
subN = var12
noise_file = var13

h_ref_wave = np.loadtxt(h_ref_file)
l_ref_wave = np.loadtxt(l_ref_file)
frq, noise = np.loadtxt(noise_file)

'''loading txt file with after cut event '''
lst = np.loadtxt(cut_file)
lst = lst.T.astype('int')
lst_arr = lst[:, 2:4]
subrunNum = lst[:, 0] * int(mergeN) + lst[:, 1]

'''TRIGGER'''


def TRG(ch_array, thr):
    ad = ch_array - thr

    ## 1st trigger
    n_trg1 = np.prod([ad[1:], ad[:-1]], axis=0)
    trg1 = (n_trg1 < 0)
    trg1 = np.append(trg1, False)

    ## 2nd trigger
    n_trg2 = np.diff(ch_array)
    n_trg2 = np.append(n_trg2, 0)
    trg2 = (n_trg2 > thr * 0.01)

    ## Finial trigger
    trgf = np.all([trg1, trg2], axis=0)

    return np.where(trgf == True)


'''READ HDF5 FILE AND USING TRIGGER AND MAKE A PRODUCTIOM FILE'''
def TRACK_EVENT(file_name, ch, n_arr, n_arr_x, init, fini, subrunnum):
    try:
        init = int(init)
        fini = int(fini)
        fs = 100e+3

        with h5py.File(file_name, 'r') as f:

            dset = f['rawdata']

            tt = np.array(dset[int(n_arr) - 2][1])

            h_arr1 = np.array(dset[int(n_arr) - 2][2][0][int(ch)])
            h_arr2 = np.array(dset[int(n_arr) - 1][2][0][int(ch)])
            h_arr3 = np.array(dset[int(n_arr) - 0][2][0][int(ch)])
            h_arr4 = np.array(dset[int(n_arr) + 1][2][0][int(ch)])
            h_data = np.append(h_arr1, h_arr2)
            h_data = np.append(h_data, h_arr3)
            h_data = np.append(h_data, h_arr4)

            l_arr1 = np.array(dset[int(n_arr) - 2][2][0][int(ch) + 1])
            l_arr2 = np.array(dset[int(n_arr) - 1][2][0][int(ch) + 1])
            l_arr3 = np.array(dset[int(n_arr) - 0][2][0][int(ch) + 1])
            l_arr4 = np.array(dset[int(n_arr) + 1][2][0][int(ch) + 1])
            l_data = np.append(l_arr1, l_arr2)
            l_data = np.append(l_data, l_arr3)
            l_data = np.append(l_data, l_arr4)

            ini = 32768 + int(n_arr_x)

            t = (tt * 1e-9) + (ini - init) * 1e-6

            h_data = h_data[ini - init:ini + fini]
            l_data = l_data[ini - init:ini + fini]

            '''calculation simple parameter'''
            h_base = np.mean(h_data[init - 6500:init - 1500])
            h_rms = np.std(h_data[init - 6500:init - 1500])
            l_base = np.mean(l_data[init - 5500:init - 1000])
            l_rms = np.std(l_data[init - 5500:init - 1000])
            h_ped = np.mean(h_data[init - 2000:init - 1500])
            h_pedRMS = np.std(h_data[init - 2000:init - 1500])

            '''Production'''
            h_ana, h_band_ana1, h_band_ana2, h_band_ana3, h_tmplt1, h_tmplt2, h_tmplt3, amp_OF = heat_prod(np.array(h_data),
                                                                                                   h_ref_wave)
            l_ana, l_band_ana1, l_band_ana2, l_band_ana3, l_tmplt1, l_tmplt2, l_tmplt3 = light_prod(np.array(l_data),
                                                                                                    l_ref_wave)

            '''write pandas'''
            pd_heat, pd_light = func_h5w.write_pd(t, h_ped, h_base, h_rms, l_base, l_rms, h_ana, h_band_ana1,
                                                  h_band_ana2, h_band_ana3, h_tmplt1, h_tmplt2, h_tmplt3, l_ana,
                                                  l_band_ana1, l_band_ana2, l_band_ana3, l_tmplt1, l_tmplt2, l_tmplt3,
                                                  subrunnum, n_arr, n_arr_x, amp_OF)

    except ValueError:
        pd_heat = pd_light = func_h5w.bug_pd()
    return pd_heat, pd_light


def heat_prod(h, h_ref_wave):
    h = np.array(h).astype('int')
    base_ini = 20800

    heat_ana = func_objs.heat_ana(h, base_ini, dt_x)
    order = 1
    h_band_ana1 = func_objs.heat_filter_ana(200, 500, 2, h - h[0], sampling_rate, base_ini)
    h_band_ana2 = func_objs.heat_filter_ana(30, 220, 2, h - h[0], sampling_rate, base_ini)
    h_band_ana3 = func_objs.heat_filter_ana(50, 250, 2, h - h[0], sampling_rate, base_ini)

    ##LMOCUP
    h_tmplt1 = func_objs.template1_least(h, h_ref_wave, 20500, 24500, 19000, 20000, 200, 500, 2, 5)
    h_tmplt2 = func_objs.template1_least(h, h_ref_wave, 20500, 25500, 19000, 20000, 30, 220, 2, 5)
    h_tmplt3 = func_objs.template1_least(h, h_ref_wave, 20500, 22500, 19000, 20000, 50, 250, 2, 5)

    ##NMO
    # h_tmplt1 = func_objs.template1_least(h, h_ref_wave, 20500, 22500, 19000, 20000, 50, 300, 2, 5)
    # h_tmplt2 = func_objs.template1_least(h, h_ref_wave, 20500, 25500, 19000, 20000, 30, 220, 2, 5)
    # h_tmplt3 = func_objs.template1_least(h, h_ref_wave, 20500, 22500, 19000, 20000, 100, 250, 2, 5)

    ##Optimal Filtering
    amp_OF = func_objs.optimal_fit(nosie, h_ref_wave, h)

    return heat_ana, h_band_ana1, h_band_ana2, h_band_ana3, h_tmplt1, h_tmplt2, h_tmplt3, amp_OF


def light_prod(l, l_ref_wave):
    base_ini = 20900
    l = np.array(l).astype('int')
    light_ana = func_objs.light_ana(l - l[0], base_ini)
    order = 2
    l_band_ana1 = func_objs.light_filter_ana(200, 440, order, l - l[0], conversion_factor, 100e3, base_ini)
    l_band_ana2 = func_objs.light_filter_ana(500, 1200, order, l - l[0], conversion_factor, 100e3, base_ini)
    l_band_ana3 = func_objs.light_filter_ana(1000, 2000, order, l - l[0], conversion_factor, 100e3, base_ini)
    # LMO
    # l_tmplt1 = func_objs.template1_least(l, l_ref_wave, 20900, 21900, 20750, 20900, 200, 440, 2, 5)
    # l_tmplt2 = func_objs.template1_least(l, l_ref_wave, 20900, 21200, 20750, 20900, 500, 1200, 2, 5)
    # l_tmplt3 = func_objs.template1_least(l, l_ref_wave, 20950, 21100, 20750, 20900, 1000, 2000, 2, 5)
    ##NMO
    l_tmplt1 = func_objs.template1_least(l, l_ref_wave, 20900, 21900, 20750, 20900, 200, 440, 2, 5)
    l_tmplt2 = func_objs.template1_least(l, l_ref_wave, 20900, 21200, 20750, 20900, 500, 1200, 2, 5)
    l_tmplt3 = func_objs.template1_least(l, l_ref_wave, 20900, 21200, 20750, 20900, 1000, 2000, 2, 5)
    return light_ana, l_band_ana1, l_band_ana2, l_band_ana3, l_tmplt1, l_tmplt2, l_tmplt3


def run(mergeN, subN):
    h5files = h5_path + '*h5*'
    file_Name = func_h5w.FILE_NAME(h5files)
    file_array = np.array([subrunNum, lst_arr[:, 0], lst_arr[:, 1]]).astype('int')
    subN = int(subN)
    cc = ((file_array[0] >= mergeN * subN) & (file_array[0] < mergeN * (subN + 1)))
    file_array = np.array([file_array[0][cc], file_array[1][cc], file_array[2][cc]])
    heat_pd, light_pd = func_h5w.init_pd(1)

    for nn, ii in enumerate(file_array.T):
        subrunnum, n_arr, n_arr_x = ii
        file_name = file_Name + '{0:05d}'.format(subrunnum)

        if ((nn == 0) or (nn % 100 == 0)):
            print("subrunNum : ", subrunnum, ", process time : ", datetime.datetime.now())
            pid = os.getpid()
            current_process = psutil.Process(pid)
            current_process_memory_usage_as_KB = current_process.memory_info()[0] / 2. ** 20
            print(f"Current memory KB : {current_process_memory_usage_as_KB : 9.3f} KB")
            print("Check Point1 : ", file_name)

        if (n_arr > 1829): continue
        PD_heat, PD_light = TRACK_EVENT(file_name, ch, n_arr, n_arr_x, init, fini, subrunnum)

        heat_pd[0] = heat_pd[0].append(PD_heat, ignore_index=True)
        light_pd[0] = light_pd[0].append(PD_light, ignore_index=True)
    func_h5w.save_pd(OUTPATH=OUTPATH, runN=runN, pd_heat=heat_pd, pd_light=light_pd, cryst_name=cryst_name, ncrystal=1,
                     subN=subN)


if __name__ == '__main__':
    run(mergeN, subN)