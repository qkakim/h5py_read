import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import *
import pandas as pd
from scipy.signal import *
from array import array
from scipy.signal import find_peaks
import h5py
from scipy import stats
from scipy.integrate import odeint, simps


def pyroot_read(tr, n_evt, base_start, base_end, sampling_rate):
    tr.GetEntry(n_evt)
    nmod = 4
    ch_heat = [[] for _ in range(nmod)]
    ch_light = [[] for _ in range(nmod)]
    time_heat = [[] for _ in range(nmod)]
    time_light = [[] for _ in range(nmod)]
    trgon = [[] for _ in range(nmod * 2)]

    ch_heat[0] = array('i', tr.ch_1)
    ch_heat[1] = array('i', tr.ch_3)
    ch_heat[2] = array('i', tr.ch_5)
    ch_heat[3] = array('i', tr.ch_7)
    # ch_heat[4] = array('i', tr.ch_9)
    # ch_heat[5] = array('i', tr.ch_11)
    # ch_heat[6] = array('i', tr.ch_13)
    # ch_heat[7] = array('i', tr.ch_15)
    ch_light[0] = array('i', tr.ch_0)
    ch_light[1] = array('i', tr.ch_2)
    ch_light[2] = array('i', tr.ch_4)
    ch_light[3] = array('i', tr.ch_6)
    # ch_light[4] = array('i', tr.ch_8)
    # ch_light[5] = array('i', tr.ch_10)
    # ch_light[6] = array('i', tr.ch_12)
    # ch_light[7] = array('i', tr.ch_14)
    time_heat[0] = tr.t_1
    time_heat[1] = tr.t_3
    time_heat[2] = tr.t_5
    time_heat[3] = tr.t_7
    # time_heat[4] = tr.t_9
    # time_heat[5] = tr.t_11
    # time_heat[6] = tr.t_13
    # time_heat[7] = tr.t_15
    time_light[0] = tr.t_0
    time_light[1] = tr.t_2
    time_light[2] = tr.t_4
    time_light[3] = tr.t_6
    # time_light[4] = tr.t_8
    # time_light[5] = tr.t_10
    # time_light[6] = tr.t_12
    # time_light[7] = tr.t_14
    trgon[0] = tr.trgon_1
    trgon[1] = tr.trgon_3
    trgon[2] = tr.trgon_5
    trgon[3] = tr.trgon_7
    # trgon[4] = tr.trgon_9
    # trgon[5] = tr.trgon_11
    # trgon[6] = tr.trgon_13

    for i in range(nmod):
        ch_heat[i] = np.array(ch_heat[i]).astype(int)
        ch_light[i] = np.array(ch_light[i]).astype(int)
        time_heat[i] = np.array(time_heat[i]).astype(float)
        time_light[i] = np.array(time_light[i]).astype(float)
        trgon[i] = np.array(trgon[i]).astype(int)

    base_heat = [[] for _ in range(nmod)]
    base_light = [[] for _ in range(nmod)]
    baseRMS_heat = [[] for _ in range(nmod)]
    baseRMS_light = [[] for _ in range(nmod)]

    for i in range(nmod):
        base_heat[i] = np.mean(ch_heat[i][base_start:base_end])
        base_light[i] = np.mean(ch_light[i][base_start:base_end])

        baseRMS_heat[i] = np.std(ch_heat[i][base_start:base_end])
        baseRMS_light[i] = np.std(ch_light[i][base_start:base_end])

    result = [ch_heat, ch_light, time_heat, time_light, base_heat, base_light, baseRMS_heat, baseRMS_light, trgon]

    return result


###ButterWorth bandpass filter###
def butter_bandpass_sos(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = butter(order, [low, high], btype='band', output='sos')
    return sos


def butter_bandpass_filter_sos(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass_sos(lowcut, highcut, fs, order=order)
    y = sosfilt(sos, data)
    return y


def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y


###ploynominal fuction###
def poly2(x, a, b, c):
    return a * x ** 2 + b * x + c


###ploynominal fuction###
def poly2_1(x, a, b, c):
    return a * (x - b) ** 2 + c


def poly1(x, a, b):
    return a * x + b


###Example READ PROD FILEs using H5py
def READ5PY(h5file):
    with h5py.File(h5file, 'r') as f:
        print(f.keys)
        dset = f['wave']


###HEAT Channel anaylsis###
def heat_ana(heat_array, base_ini, dt_x):
    try:
        base_ini = int(base_ini)
        pedestal = np.mean(heat_array[base_ini - 300:base_ini])
        peaks, _ = find_peaks(heat_array[base_ini:base_ini + 5000], height=pedestal + 50)
        xhpeaks = peaks + base_ini
        hpeak = np.max(heat_array[xhpeaks]) - pedestal
        arghpeak = np.argmax(heat_array[xhpeaks])
        xhpeak = xhpeaks[arghpeak]

        fit_term = 20
        # pulse peak point finder more detail using fitting
        try:
            rt100_x = np.linspace(xhpeak - fit_term, xhpeak + fit_term, fit_term * 2)
            popt1, pcov1 = curve_fit(poly2_1, rt100_x, heat_array[xhpeak - fit_term:xhpeak + fit_term],
                                     p0=[-1, xhpeak, hpeak])

            xhpeak = int(popt1[1])
            hpeak = popt1[2] - pedestal
        except RuntimeError:
            xhpeak = xhpeak
            hpeak = hpeak

        rt90 = hpeak * 0.9 + pedestal
        arg_rt90 = np.argmin(abs(heat_array[base_ini:xhpeak] - rt90)) + base_ini
        rt70 = hpeak * 0.7 + pedestal
        arg_rt70 = np.argmin(abs(heat_array[base_ini:xhpeak] - rt70)) + base_ini
        rt50 = hpeak * 0.5 + pedestal
        arg_rt50 = np.argmin(abs(heat_array[base_ini:xhpeak] - rt50)) + base_ini
        rt30 = hpeak * 0.3 + pedestal
        arg_rt30 = np.argmin(abs(heat_array[base_ini:xhpeak] - rt30)) + base_ini
        rt20 = hpeak * 0.2 + pedestal
        arg_rt20 = np.argmin(abs(heat_array[base_ini:xhpeak] - rt20)) + base_ini
        rt15 = hpeak * 0.15 + pedestal
        arg_rt15 = np.argmin(abs(heat_array[base_ini:xhpeak] - rt15)) + base_ini
        rt10 = hpeak * 0.1 + pedestal
        arg_rt10 = np.argmin(abs(heat_array[base_ini:xhpeak] - rt10)) + base_ini
        rt5 = hpeak * 0.05 + pedestal
        arg_rt5 = np.argmin(abs(heat_array[base_ini:xhpeak] - rt5)) + base_ini

        dt90 = hpeak * 0.9 + pedestal
        dt70 = hpeak * 0.7 + pedestal
        dt50 = hpeak * 0.5 + pedestal
        dt30 = hpeak * 0.3 + pedestal
        dt10 = hpeak * 0.1 + pedestal

        arg_dt90 = np.argmin(abs(heat_array[int(xhpeak):int(xhpeak) + dt_x] - dt90)) + int(xhpeak)
        arg_dt70 = np.argmin(abs(heat_array[int(xhpeak):int(xhpeak) + dt_x] - dt70)) + int(xhpeak)
        arg_dt50 = np.argmin(abs(heat_array[int(xhpeak):int(xhpeak) + dt_x] - dt50)) + int(xhpeak)
        arg_dt30 = np.argmin(abs(heat_array[int(xhpeak):int(xhpeak) + dt_x] - dt30)) + int(xhpeak)
        arg_dt10 = np.argmin(abs(heat_array[int(xhpeak):int(xhpeak) + dt_x] - dt10)) + int(xhpeak)

        # print (rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10)
        ##for risetime point fitting
        fit_term = 5
        try:
            ###90
            def objective90(x, a, b, c):
                return (rt90 - poly2(x, a, b, c)) ** 2

            rt90_x = np.linspace(arg_rt90 - fit_term, arg_rt90 + fit_term, 2 * fit_term)
            popt1, pcov1 = curve_fit(poly2, rt90_x, heat_array[arg_rt90 - fit_term:arg_rt90 + fit_term])
            res90 = minimize_scalar(objective90, bounds=(arg_rt90 - fit_term, arg_rt90 + fit_term), method='bounded',
                                    args=tuple(popt1))
            rt90_f = res90.x

            ###70
            def objective70(x, a, b, c):
                return (rt70 - poly2(x, a, b, c)) ** 2

            rt70_x = np.linspace(arg_rt70 - fit_term, arg_rt70 + fit_term, 2 * fit_term)
            popt2, pcov2 = curve_fit(poly2, rt70_x, heat_array[arg_rt70 - fit_term:arg_rt70 + fit_term])
            res70 = minimize_scalar(objective70, bounds=(arg_rt70 - fit_term, arg_rt70 + fit_term), method='bounded',
                                    args=tuple(popt2))
            rt70_f = res70.x

            ###50
            def objective50(x, a, b, c):
                return (rt50 - poly2(x, a, b, c)) ** 2

            rt50_x = np.linspace(arg_rt50 - fit_term, arg_rt50 + fit_term, 2 * fit_term)
            popt3, pcov3 = curve_fit(poly2, rt50_x, heat_array[arg_rt50 - fit_term:arg_rt50 + fit_term])
            res50 = minimize_scalar(objective50, bounds=(arg_rt50 - fit_term, arg_rt50 + fit_term), method='bounded',
                                    args=tuple(popt3))
            rt50_f = res50.x

            ###30
            def objective30(x, a, b, c):
                return (rt30 - poly2(x, a, b, c)) ** 2

            rt30_x = np.linspace(arg_rt30 - fit_term, arg_rt30 + fit_term, 2 * fit_term)
            popt4, pcov4 = curve_fit(poly2, rt30_x, heat_array[arg_rt30 - fit_term:arg_rt30 + fit_term])
            res30 = minimize_scalar(objective30, bounds=(arg_rt30 - fit_term, arg_rt30 + fit_term), method='bounded',
                                    args=tuple(popt4))
            rt30_f = res30.x

            ###20
            def objective20(x, a, b, c):
                return (rt20 - poly2(x, a, b, c)) ** 2

            rt20_x = np.linspace(arg_rt20 - fit_term, arg_rt20 + fit_term, 2 * fit_term)
            popt5, pcov5 = curve_fit(poly2, rt20_x, heat_array[arg_rt20 - fit_term:arg_rt20 + fit_term])
            res20 = minimize_scalar(objective20, bounds=(arg_rt20 - fit_term, arg_rt20 + fit_term), method='bounded',
                                    args=tuple(popt5))
            rt20_f = res20.x

            ###15
            def objective15(x, a, b, c):
                return (rt15 - poly2(x, a, b, c)) ** 2

            rt15_x = np.linspace(arg_rt15 - fit_term, arg_rt15 + fit_term, 2 * fit_term)
            popt6, pcov6 = curve_fit(poly2, rt15_x, heat_array[arg_rt15 - fit_term:arg_rt15 + fit_term])
            res15 = minimize_scalar(objective15, bounds=(arg_rt15 - fit_term, arg_rt15 + fit_term), method='bounded',
                                    args=tuple(popt6))
            rt15_f = res15.x

            ###10
            def objective10(x, a, b, c):
                return (rt10 - poly2(x, a, b, c)) ** 2

            rt10_x = np.linspace(arg_rt10 - fit_term, arg_rt10 + fit_term, 2 * fit_term)
            popt7, pcov7 = curve_fit(poly2, rt10_x, heat_array[arg_rt10 - fit_term:arg_rt10 + fit_term])
            res10 = minimize_scalar(objective10, bounds=(arg_rt10 - fit_term, arg_rt10 + fit_term), method='bounded',
                                    args=tuple(popt7))
            rt10_f = res10.x

            ###5
            def objective5(x, a, b, c):
                return (rt5 - poly2(x, a, b, c)) ** 2

            rt5_x = np.linspace(arg_rt5 - fit_term, arg_rt5 + fit_term, 2 * fit_term)
            popt8, pcov8 = curve_fit(poly2, rt5_x, heat_array[arg_rt5 - fit_term:arg_rt5 + fit_term])
            res5 = minimize_scalar(objective5, bounds=(arg_rt5 - fit_term, arg_rt5 + fit_term), method='bounded',
                                   args=tuple(popt8))
            rt5_f = res5.x

        except RuntimeError:
            # print ('Failed fitting of risetime part')
            rt90_f = arg_rt90
            rt70_f = arg_rt70
            rt50_f = arg_rt50
            rt30_f = arg_rt30
            rt20_f = arg_rt20
            rt15_f = arg_rt15
            rt10_f = arg_rt10
            rt5_f = arg_rt5

        fit_term = 20
        try:
            ###90
            def objective90(x, a, b, c):
                return (dt90 - poly2(x, a, b, c)) ** 2

            dt90_x = np.linspace(arg_dt90 - fit_term, arg_dt90 + fit_term, 2 * fit_term)
            popt1, pcov1 = curve_fit(poly2, dt90_x, heat_array[arg_dt90 - fit_term:arg_dt90 + fit_term])
            res90 = minimize_scalar(objective90, bounds=(arg_dt90 - fit_term, arg_dt90 + fit_term), method='bounded',
                                    args=tuple(popt1))
            dt90_f = res90.x

            ###70
            def objective70(x, a, b, c):
                return (dt70 - poly2(x, a, b, c)) ** 2

            dt70_x = np.linspace(arg_dt70 - fit_term, arg_dt70 + fit_term, 2 * fit_term)
            popt2, pcov2 = curve_fit(poly2, dt70_x, heat_array[arg_dt70 - fit_term:arg_dt70 + fit_term])
            res70 = minimize_scalar(objective70, bounds=(arg_dt70 - fit_term, arg_dt70 + fit_term), method='bounded',
                                    args=tuple(popt2))
            dt70_f = res70.x

            ###50
            def objective50(x, a, b, c):
                return (dt50 - poly2(x, a, b, c)) ** 2

            dt50_x = np.linspace(arg_dt50 - fit_term, arg_dt50 + fit_term, 2 * fit_term)
            popt3, pcov3 = curve_fit(poly2, dt50_x, heat_array[arg_dt50 - fit_term:arg_dt50 + fit_term])
            res50 = minimize_scalar(objective50, bounds=(arg_dt50 - fit_term, arg_dt50 + fit_term), method='bounded',
                                    args=tuple(popt3))
            dt50_f = res50.x

            ###30
            def objective30(x, a, b, c):
                return (dt30 - poly2(x, a, b, c)) ** 2

            dt30_x = np.linspace(arg_dt30 - fit_term, arg_dt30 + fit_term, 2 * fit_term)
            popt4, pcov4 = curve_fit(poly2, dt30_x, heat_array[arg_dt30 - fit_term:arg_dt30 + fit_term])
            res30 = minimize_scalar(objective30, bounds=(arg_dt30 - fit_term, arg_dt30 + fit_term), method='bounded',
                                    args=tuple(popt4))
            dt30_f = res30.x

            ###10
            def objective10(x, a, b, c):
                return (dt10 - poly2(x, a, b, c)) ** 2

            dt10_x = np.linspace(arg_dt10 - fit_term, arg_dt10 + fit_term, 2 * fit_term)
            popt5, pcov5 = curve_fit(poly2, dt10_x, heat_array[arg_dt10 - fit_term:arg_dt10 + fit_term])
            res10 = minimize_scalar(objective10, bounds=(arg_dt10 - fit_term, arg_dt10 + fit_term), method='bounded',
                                    args=tuple(popt5))
            dt10_f = res10.x

        except RuntimeError:
            # print ('Failed fitting of decaytime part')
            dt90_f = arg_dt90
            dt70_f = arg_dt70
            dt50_f = arg_dt50
            dt30_f = arg_dt30
            dt10_f = arg_dt10

        try:
            xslp = np.linspace(arg_rt10 - 2000, arg_rt10 - 500, 1500)
            popt, pcov = curve_fit(poly1, xslp, heat_array[int(arg_rt10 - 2000): int(arg_rt10 - 500)])
            Slope = popt[2]
        except:
            Slope = (np.mean(heat_array[int(arg_rt10 - 600): int(arg_rt10 - 500)]) - np.mean(
                heat_array[int(arg_rt10 - 2000): int(arg_rt10 - 1900)])) / 1500

        ###End baseline
        endline = np.mean(heat_array[-2000:])
        endlineRMS = np.std(heat_array[-2000:])

        hbaseline = np.mean(heat_array[base_ini - 5000:base_ini])
        hbaselineRMS = np.std(heat_array[base_ini - 5000:base_ini])

        qsum5 = np.sum(heat_array[base_ini:int(rt5_f)]) - (int(rt5_f) - base_ini) * pedestal
        qsum10 = np.sum(heat_array[base_ini:int(rt10_f)]) - (int(rt10_f) - base_ini) * pedestal
        qsum15 = np.sum(heat_array[base_ini:int(rt15_f)]) - (int(rt15_f) - base_ini) * pedestal
        qsum20 = np.sum(heat_array[base_ini:int(rt20_f)]) - (int(rt20_f) - base_ini) * pedestal
        qsum30 = np.sum(heat_array[base_ini:int(rt30_f)]) - (int(rt30_f) - base_ini) * pedestal
        qsum50 = np.sum(heat_array[base_ini:int(rt50_f)]) - (int(rt50_f) - base_ini) * pedestal
        qsum70 = np.sum(heat_array[base_ini:int(rt70_f)]) - (int(rt70_f) - base_ini) * pedestal
        qsum90 = np.sum(heat_array[base_ini:int(rt90_f)]) - (int(rt90_f) - base_ini) * pedestal

        result = [xhpeak, hpeak, [rt90_f, rt70_f, rt50_f, rt30_f, rt20_f, rt15_f, rt10_f, rt5_f],
                  [rt90, rt70, rt50, rt30, rt20, rt15, rt10, rt5] \
            , [dt90_f, dt70_f, dt50_f, dt30_f, dt10_f], [dt90, dt70, dt50, dt30, dt10],
                  [qsum90, qsum70, qsum50, qsum30, qsum20, qsum15, qsum10, qsum5] \
            , endline, endlineRMS, pedestal, hbaseline, hbaselineRMS, Slope]

    except ValueError:
        result = [-1, -1, [-1, -1, -1, -1, -1, -1, -1, -1], [-1, -1, -1, -1, -1, -1, -1, -1], [-1, -1, -1, -1, -1],
                  [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1, -1, -1, -1], -1, -1, -1, -1, -1, -1]

    return result


###LIGHT Channel analysis###

def light_ana(light_array, base_ini):
    try:
        # pulse height and height point
        pedestal = np.mean(light_array[base_ini - 50:base_ini + 10])
        peaks, _ = find_peaks(light_array[base_ini:base_ini + 350], height=pedestal + 10)
        xlpeaks = peaks + base_ini
        lpeak = np.max(light_array[xlpeaks]) - pedestal
        # hpeak = np.max(heat_array[xhpeaks]) - np.mean(heat_array[17600:17800])
        arglpeak = np.argmax(light_array[xlpeaks])
        xlpeak = xlpeaks[arglpeak]

        fit_term = 20
        # pulse peak point finder more detail using fitting
        try:
            rt100_x = np.linspace(xlpeak - fit_term, xlpeak + fit_term, fit_term * 2)
            popt1, pcov1 = curve_fit(poly2_1, rt100_x, light_array[xlpeak - fit_term:xlpeak + fit_term],
                                     p0=[-1, xlpeak, lpeak])

            xlpeak = int(popt1[1])
            lpeak = popt1[2] - pedestal
        except RuntimeError:
            xlpeak = xlpeak
            lpeak = lpeak

        # For Risetime
        rt90 = lpeak * 0.9 + pedestal
        arg_rt90 = np.argmin(abs(light_array[base_ini:xlpeak] - rt90)) + base_ini

        rt50 = lpeak * 0.5 + pedestal
        arg_rt50 = np.argmin(abs(light_array[base_ini:xlpeak] - rt50)) + base_ini

        rt10 = lpeak * 0.1 + pedestal
        arg_rt10 = np.argmin(abs(light_array[base_ini:xlpeak] - rt10)) + base_ini

        ##for risetime point fitting
        fit_term = 5

        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2

        rt90_x = np.linspace(arg_rt90 - fit_term, arg_rt90 + fit_term, 2 * fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, light_array[arg_rt90 - fit_term:arg_rt90 + fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90 - fit_term, arg_rt90 + fit_term), method='bounded',
                                args=tuple(popt1))
        rt90_f = res90.x

        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2

        rt50_x = np.linspace(arg_rt50 - fit_term, arg_rt50 + fit_term, 2 * fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, light_array[arg_rt50 - fit_term:arg_rt50 + fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50 - fit_term, arg_rt50 + fit_term), method='bounded',
                                args=tuple(popt2))
        rt50_f = res50.x

        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2

        rt10_x = np.linspace(arg_rt10 - fit_term, arg_rt10 + fit_term, 2 * fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, light_array[arg_rt10 - fit_term:arg_rt10 + fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10 - fit_term, arg_rt10 + fit_term), method='bounded',
                                args=tuple(popt3))
        rt10_f = res10.x

        ###End baseline
        endline = np.mean(light_array[-2000:])
        endlineRMS = np.std(light_array[-2000:])
        result = [xlpeak, lpeak, rt90_f, rt50_f, rt10_f, rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10, endline,
                  endlineRMS, -1, pedestal]

    except ValueError:
        result = [-1] * 15

    return result


def light_ana_amore(light_array, conversion_factor, sampling_rate, x1, x2):
    try:
        move_avg = 15

        diff_array = np.diff(movingaverage(light_array[x1:x2], move_avg))
        diff_array = diff_array[move_avg:-move_avg]
        diffmaxx = np.argmax(diff_array) + x1 + move_avg

        grad_array = np.gradient(movingaverage(light_array[x1:x2], move_avg))
        grad_array = grad_array[move_avg:-move_avg]
        gradmaxx = np.argmax(grad_array) + x1 + move_avg

        diffmax = light_array[diffmaxx]
        gradmax = light_array[gradmaxx]

        # pulse height and height point

        base_end = gradmaxx - 400
        pedestal = np.mean(light_array[base_end - 100:base_end])
        lmin = np.min(light_array[gradmaxx:gradmaxx + 1000])
        peaks, _ = find_peaks(light_array[gradmaxx:gradmaxx + 1000], height=pedestal + 10)
        xlpeaks = peaks + x1
        lpeak = np.max(light_array[xlpeaks]) - pedestal
        arglpeak = np.argmax(light_array[xlpeaks])
        xlpeak = xlpeaks[arglpeak]

        # For Risetime
        rt90 = lpeak * 0.9 + pedestal
        arg_rt90 = np.argmin(abs(light_array[base_end:xlpeak] - rt90)) + base_end

        rt50 = lpeak * 0.5 + pedestal
        arg_rt50 = np.argmin(abs(light_array[base_end:xlpeak] - rt50)) + base_end

        rt10 = lpeak * 0.1 + pedestal
        arg_rt10 = np.argmin(abs(light_array[base_end:xlpeak] - rt10)) + base_end

        ##for risetime point fitting
        fit_term = 5

        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2

        rt90_x = np.linspace(arg_rt90 - fit_term, arg_rt90 + fit_term, 2 * fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, light_array[arg_rt90 - fit_term:arg_rt90 + fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90 - fit_term, arg_rt90 + fit_term), method='bounded',
                                args=tuple(popt1))
        rt90_f = res90.x

        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2

        rt50_x = np.linspace(arg_rt50 - fit_term, arg_rt50 + fit_term, 2 * fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, light_array[arg_rt50 - fit_term:arg_rt50 + fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50 - fit_term, arg_rt50 + fit_term), method='bounded',
                                args=tuple(popt2))
        rt50_f = res50.x

        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2

        rt10_x = np.linspace(arg_rt10 - fit_term, arg_rt10 + fit_term, 2 * fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, light_array[arg_rt10 - fit_term:arg_rt10 + fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10 - fit_term, arg_rt10 + fit_term), method='bounded',
                                args=tuple(popt3))
        rt10_f = res10.x

        ###End baseline
        endline = np.mean(light_array[-2000:])
        endlineRMS = np.std(light_array[-2000:])
        result = [xlpeak, lpeak, rt90_f, rt50_f, rt10_f, rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10, endline,
                  endlineRMS, lmin, pedestal, diffmaxx, diffmax, gradmaxx, gradmax]

    except ValueError:
        result = [-1] * 19

    return result


# def light_ana(light_array, conversion_factor, sampling_rate, baseline, baselineRMS, base_end) :
#    try :
#        lpeak = np.max(light_array[base_end: base_end+1000]) - baseline
#        xlpeak = np.argmax(light_array[base_end : base_end+1000]) +base_end
#        result = [xlpeak, lpeak]
#
#    except ValueError :
#        result = [-1. -1]
#
#    return result

###HEAT Channel filter analysis###

def heat_filter_ana(lowcut, highcut, order, heat_array, sampling_rate, base_ini):
    try:
        base_ini = int(base_ini)
        # print ("in func ----- nofileter : ", heat_array[0:10])
        y = butter_bandpass_filter_sos(heat_array, lowcut, highcut, sampling_rate, order=order)
        # print ("in func ----- fileter : ", y[0:10])
        fbaseline = np.mean(y[base_ini - 1000:base_ini - 500])
        fbaselineRMS = np.std(y[base_ini - 1000:base_ini - 500])

        hpeak = np.max(y[base_ini:base_ini + 1200])
        xhpeak = np.argmax(y[base_ini:base_ini + 1200])
        xhpeak = xhpeak + base_ini

        hfmin = np.min(y[xhpeak:xhpeak + 1200])
        xhfmin = np.argmin(y[xhpeak:xhpeak + 1200]) + xhpeak

        hpeak2nd = np.max(y[xhfmin:xhfmin + 1200])
        xhpeak2nd = np.argmax(y[xhfmin:xhfmin + 1200]) + xhfmin

        try:
            rt100_x = np.linspace(xhpeak - 15, xhpeak + 15, 15 * 2)
            popt1, pcov1 = curve_fit(poly2_1, rt100_x, y[xhpeak - 15:xhpeak + 15], p0=[-1, xhpeak, hpeak])
            fxhpeak = popt1[1]
            fhpeak = popt1[2]
        except RuntimeError:
            fxhpeak = -1
            fhpeak = -1

        try:
            rt100_x = np.linspace(xhfmin - 15, xhfmin + 15, 15 * 2)
            popt1, pcov1 = curve_fit(poly2_1, rt100_x, y[xhfmin - 15:xhfmin + 15], p0=[1, xhfmin, hfmin])
            fxhfmin = popt1[1]
            fhfmin = popt1[2]
        except RuntimeError:
            fxhfmin = -1
            fhfmin = -1

        result = [xhpeak, hpeak, fbaseline, fbaselineRMS, xhfmin, hfmin, xhpeak2nd, hpeak2nd, fxhpeak, fhpeak, fxhfmin,
                  fhfmin]

    except ValueError:
        result = [-1] * 12

    return result


def heat_filter_ana_1(lowcut, highcut, order, heat_array, conversion_factor, sampling_rate, base_end):
    try:
        # print ("in func ----- nofileter : ", heat_array[0:10])
        y = butter_bandpass_filter_sos(heat_array, lowcut, highcut, sampling_rate, order=order)
        # print ("in func ----- fileter : ", y[0:10])
        grad_array = np.gradient(y)
        n_array = movingaverage(grad_array, 1)

        argtrg = np.argmax(n_array[500:1000])

        fbaseline = np.mean(y[argtrg - 100:argtrg - 50])
        fbaselineRMS = np.std(y[argtrg - 100:argtrg - 50])

        peaks, _ = find_peaks(y[argtrg:argtrg + 700], height=fbaseline + 100)
        xhpeaks = peaks + argtrg
        hpeak = np.max(y[xhpeaks]) - fbaseline
        arghpeak = np.argmax(y[xhpeaks])
        xhpeak = xhpeaks[arghpeak]

        yy = 9999
        xx = 0

        for ii in range(xhpeak, xhpeak + 500, 1):
            # if (y[ii] > yy) : break
            if (y[ii] < yy):
                yy = y[ii]
                xx = ii

        lfmin = yy
        xlfmin = xx

        hpeak2nd = np.max(y[xlfmin:xlfmin + 500])
        xhpeak2nd = np.argmax(y[xlfmin:xlfmin + 500]) + xlfmin

        result = [xhpeak, hpeak, fbaseline, fbaselineRMS, xlfmin, lfmin, xhpeak2nd, hpeak2nd]

    except ValueError:
        result = [-1] * 8

    return result


###LIGHT Channel filter analysis###
def light_filter_ana(lowcut, highcut, order, light_array, conversion_factor, sampling_rate, base_end):
    try:
        y = butter_bandpass_filter_sos(light_array - light_array[0], lowcut, highcut, sampling_rate, order=order)

        fbaseline = np.mean(y[int(base_end) - 700:int(base_end) + 300])
        fbaselineRMS = np.std(y[int(base_end) - 700:int(base_end) + 300])

        peaks, _ = find_peaks(y[base_end:base_end + 2000], height=fbaseline + 10)
        xhpeaks = peaks + base_end
        hpeak = np.max(y[xhpeaks])
        hpeak1 = np.max(y[xhpeaks]) - fbaseline
        arghpeak = np.argmax(y[xhpeaks])
        xhpeak = xhpeaks[arghpeak]

        yy = 9999
        xx = 0

        for ii in range(xhpeak, xhpeak + 1000, 1):
            if (y[ii] < yy):
                yy = y[ii]
                xx = ii

        lfmin = yy
        xlfmin = xx

        hpeak2nd = np.max(y[xlfmin:xlfmin + 1000])
        xhpeak2nd = np.argmax(y[xlfmin:xlfmin + 1000]) + xlfmin

        result = [xhpeak, hpeak, fbaseline, fbaselineRMS, xlfmin, lfmin, xhpeak2nd, hpeak2nd, hpeak1]


    except ValueError:
        result = [-1] * 9

    return result


def ch_filter_risetime(light_array, conversion_factor, sampling_rate, lowcut, highcut, order, base_end):
    try:
        # pulse height and height point
        y = butter_bandpass_filter_sos(light_array - light_array[0], lowcut, highcut, sampling_rate, order=order)

        fbaseline = np.mean(y[7000:7700])
        fbaselineRMS = np.std(y[7000:7700])

        pedestal = fbaseline
        peaks, _ = find_peaks(y[base_end:base_end + 1000], height=fbaseline + 10)
        xlpeaks = peaks + base_end
        lpeak = np.max(y[xlpeaks])
        arglpeak = np.argmax(y[xlpeaks])
        xlpeak = xlpeaks[arglpeak]
        # lpeak = np.max(y[base_end:base_end+1000]) - pedestal
        # xlpeak = np.argmax(y[base_end:base_end+1000]) + base_end

        # For Risetime
        rt90 = lpeak * 0.9
        arg_rt90 = np.argmin(abs(y[base_end:xlpeak] - rt90)) + base_end

        rt50 = lpeak * 0.5
        arg_rt50 = np.argmin(abs(y[base_end:xlpeak] - rt50)) + base_end

        rt10 = lpeak * 0.1
        arg_rt10 = np.argmin(abs(y[base_end:xlpeak] - rt10)) + base_end

        ##for risetime point fitting
        fit_term = 5

        ###90
        def objective90(x, a, b, c):
            return (rt90 - poly2(x, a, b, c)) ** 2

        rt90_x = np.linspace(arg_rt90 - fit_term, arg_rt90 + fit_term, 2 * fit_term)
        popt1, pcov1 = curve_fit(poly2, rt90_x, y[arg_rt90 - fit_term:arg_rt90 + fit_term])
        res90 = minimize_scalar(objective90, bounds=(arg_rt90 - fit_term, arg_rt90 + fit_term), method='bounded',
                                args=tuple(popt1))
        rt90_f = res90.x

        ###50
        def objective50(x, a, b, c):
            return (rt50 - poly2(x, a, b, c)) ** 2

        rt50_x = np.linspace(arg_rt50 - fit_term, arg_rt50 + fit_term, 2 * fit_term)
        popt2, pcov2 = curve_fit(poly2, rt50_x, y[arg_rt50 - fit_term:arg_rt50 + fit_term])
        res50 = minimize_scalar(objective50, bounds=(arg_rt50 - fit_term, arg_rt50 + fit_term), method='bounded',
                                args=tuple(popt2))
        rt50_f = res50.x

        ###10
        def objective10(x, a, b, c):
            return (rt10 - poly2(x, a, b, c)) ** 2

        rt10_x = np.linspace(arg_rt10 - fit_term, arg_rt10 + fit_term, 2 * fit_term)
        popt3, pcov3 = curve_fit(poly2, rt10_x, y[arg_rt10 - fit_term:arg_rt10 + fit_term])
        res10 = minimize_scalar(objective10, bounds=(arg_rt10 - fit_term, arg_rt10 + fit_term), method='bounded',
                                args=tuple(popt3))
        rt10_f = res10.x

        result = [xlpeak, lpeak, rt90_f, rt50_f, rt10_f, rt90, rt50, rt10, arg_rt90, arg_rt50, arg_rt10]

    except ValueError:
        result = [-1] * 11

    return result


###For Template Fitting def###
def template_fitting(axx, amb_array, tmp_axx, tmp_array, window0, window1, xpeak10_b, xpeak10):
    try:
        OF_window0_b = int(xpeak10_b) - window0
        OF_window1_b = int(xpeak10_b) + window1

        axx_tmp_b = tmp_axx[OF_window0_b:OF_window1_b] - OF_window0_b
        ayy_tmp_b = tmp_array[OF_window0_b:OF_window1_b]

        OF_window0 = int(xpeak10) - window0
        OF_window1 = int(xpeak10) + window1

        axx_tmp = axx[OF_window0:OF_window1] - OF_window0
        ayy_tmp = amb_array[OF_window0:OF_window1]

        S_b = np.sum(ayy_tmp * ayy_tmp_b) / np.sum(ayy_tmp_b ** 2)

        samples = window1
        ayy_chi = ayy_tmp[window0:]
        ayy_chi_b = ayy_tmp_b[window0:] * S_b

        chi_b = np.sum((ayy_chi - ayy_chi_b) ** 2 / ayy_chi_b) / (samples - 1)

        result = [S_b, chi_b]

    except ValueError:
        result = [-1] * 2

    return result


###GET template fitting parameter###
def heat_ana_temp(heat_array, conversion_factor, sampling_rate, baseline, baselineRMS, base_end):
    # pulse height and height point
    peaks, _ = find_peaks(heat_array[base_end:base_end + 500], height=baseline + 0.5)
    xhpeaks = peaks + base_end
    hpeak = np.max(heat_array[xhpeaks]) - baseline
    arghpeak = np.argmax(heat_array[xhpeaks])
    xhpeak = xhpeaks[arghpeak]
    result = [xhpeak, hpeak]

    # For Risetime
    rt90 = hpeak * 0.9 + baseline
    arg_rt90 = np.argmin(abs(heat_array[base_end:xhpeak] - rt90)) + base_end

    rt50 = hpeak * 0.5 + baseline
    arg_rt50 = np.argmin(abs(heat_array[base_end:xhpeak] - rt50)) + base_end

    rt10 = hpeak * 0.1 + baseline
    arg_rt10 = np.argmin(abs(heat_array[base_end:xhpeak] - rt10)) + base_end

    ##for risetime point fitting
    fit_term = 5

    ###90
    def objective90(x, a, b, c):
        return (rt90 - poly2(x, a, b, c)) ** 2

    rt90_x = np.linspace(arg_rt90 - fit_term, arg_rt90 + fit_term, 2 * fit_term)
    popt1, pcov1 = curve_fit(poly2, rt90_x, heat_array[arg_rt90 - fit_term:arg_rt90 + fit_term])
    res90 = minimize_scalar(objective90, bounds=(arg_rt90 - fit_term, arg_rt90 + fit_term), method='bounded',
                            args=tuple(popt1))
    rt90_f = res90.x

    ###50
    def objective50(x, a, b, c):
        return (rt50 - poly2(x, a, b, c)) ** 2

    rt50_x = np.linspace(arg_rt50 - fit_term, arg_rt50 + fit_term, 2 * fit_term)
    popt2, pcov2 = curve_fit(poly2, rt50_x, heat_array[arg_rt50 - fit_term:arg_rt50 + fit_term])
    res50 = minimize_scalar(objective50, bounds=(arg_rt50 - fit_term, arg_rt50 + fit_term), method='bounded',
                            args=tuple(popt2))
    rt50_f = res50.x

    ###10
    def objective10(x, a, b, c):
        return (rt10 - poly2(x, a, b, c)) ** 2

    rt10_x = np.linspace(arg_rt10 - fit_term, arg_rt10 + fit_term, 2 * fit_term)
    popt3, pcov3 = curve_fit(poly2, rt10_x, heat_array[arg_rt10 - fit_term:arg_rt10 + fit_term])
    res10 = minimize_scalar(objective10, bounds=(arg_rt10 - fit_term, arg_rt10 + fit_term), method='bounded',
                            args=tuple(popt3))
    rt10_f = res10.x

    result = [xhpeak, hpeak, rt90_f, rt50_f, rt10_f]

    return result


def bandpass_template_fitting(x_adc, y_adc, ref_x_adc, ref_y_adc, x_i, x_f, rt10_x, lowcut, highcut, order):
    try:

        rt10ref_x = int(np.argmax(ref_y_adc[17000:20000])) + 17000
        rt10_x = int(np.argmax(y_adc[17000:20000])) + 17000

        y_adc = np.array(y_adc) - y_adc[0]
        ref_y_adc = np.array(ref_y_adc) - ref_y_adc[0]

        y_adc = butter_bandpass_filter_sos(y_adc, lowcut, highcut, 100e3, order=order)
        ref_y_adc = butter_bandpass_filter_sos(ref_y_adc, lowcut, highcut, 100e3, order=order)

        # reference waveform
        OF_ref_window0 = rt10ref_x - x_i
        OF_ref_window1 = rt10ref_x + x_f
        ref_x_tmp = ref_x_adc[OF_ref_window0:OF_ref_window1] - ref_x_adc[OF_ref_window0]
        ref_y_tmp = ref_y_adc[OF_ref_window0:OF_ref_window1]

        # data
        OF_window0 = rt10_x - x_i
        OF_window1 = rt10_x + x_f
        x_tmp = x_adc[OF_window0:OF_window1] - x_adc[OF_window0]
        y_tmp = y_adc[OF_window0:OF_window1]

        S_a = np.sum(y_tmp * ref_y_tmp) / np.sum(ref_y_tmp ** 2)

        samples = x_f
        y_chi = y_tmp[x_i:]
        y_a_chi = ref_y_tmp[x_i:] * S_a

        chi_a = np.sum((y_chi - y_a_chi) ** 2 / y_a_chi) / (samples - 1)

        mean_y = np.mean(y_tmp)
        SSE_a = np.sum((ref_y_tmp * S_a - y_tmp) ** 2)
        SST = np.sum((y_tmp - mean_y) ** 2)

        R2_TF_a = 1 - SSE_a / SST

        return [S_a, chi_a, R2_TF_a]

    except:
        return [-2 ** 17] * 3


def bandpass_template_fitting_test(x_adc, y_adc, ref_x_adc, ref_y_adc, x_i, x_f, rt10_x, lowcut, highcut, order):
    rt10_x = int(rt10_x)
    y_adc = np.array(y_adc) - y_adc[0]
    ref_y_adc = np.array(ref_y_adc) - ref_y_adc[0]

    y_adc = butter_bandpass_filter_sos(y_adc, lowcut, highcut, 100e3, order=order)
    ref_y_adc = butter_bandpass_filter_sos(ref_y_adc, lowcut, highcut, 100e3, order=order)

    # reference waveform
    OF_ref_window0 = rt10_x - x_i
    OF_ref_window1 = rt10_x + x_f
    ref_x_tmp = ref_x_adc[OF_ref_window0:OF_ref_window1] - ref_x_adc[OF_ref_window0]
    ref_y_tmp = ref_y_adc[OF_ref_window0:OF_ref_window1]

    # data
    OF_window0 = rt10_x - x_i
    OF_window1 = rt10_x + x_f
    x_tmp = x_adc[OF_window0:OF_window1] - x_adc[OF_window0]
    y_tmp = y_adc[OF_window0:OF_window1]

    S_a = np.sum(y_tmp * ref_y_tmp) / np.sum(ref_y_tmp ** 2)

    samples = x_f
    y_chi = y_tmp[x_i:]
    y_a_chi = ref_y_tmp[x_i:] * S_a

    chi_a = np.sum((y_chi - y_a_chi) ** 2 / y_a_chi) / (samples - 1)

    mean_y = np.mean(y_tmp)
    SSE_a = np.sum((ref_y_tmp * S_a - y_tmp) ** 2)
    SST = np.sum((y_tmp - mean_y) ** 2)

    R2_TF_a = 1 - SSE_a / SST

    return [S_a, chi_a, R2_TF_a]


def movingaverage(interval, window_size):
    window = np.ones(int(window_size)) / float(window_size)
    return np.convolve(interval, window, 'same')


### template fitting method1
def template1(data, ref, init, fini, base_init, base_fini, lowcut, highcut, order, wig=0):
    try:
        sampling_rate = 100e3

        filt_ref = butter_bandpass_filter_sos(ref - ref[0], lowcut, highcut, sampling_rate, order)
        filt_ref = filt_ref / np.max(filt_ref)

        data = data - np.mean(data[base_init:base_fini])
        filt_data = butter_bandpass_filter_sos(data - data[0], lowcut, highcut, sampling_rate, order)

        res = stats.linregress(filt_ref[init:fini], filt_data[init + wig:fini + wig])

    except:
        print("failed lingress fitting")
        res = np.array([-1, -1, -1, -1, -1])
    return res


def template1_least(data, ref_wave, init, fini, base_init, base_fini, lowcut, highcut, order, ntry):
    res_arr = np.empty([2 * ntry, 5])

    for i in range(-ntry, ntry):
        res = template1(data, ref_wave, init, fini, base_init, base_fini, lowcut, highcut, order, int(i))
        res_arr[i] = res[0], res[1], res[2], res[3], res[4]

    min_res = res_arr[np.argmin(res_arr[:, 4])]
    return min_res
'''template fitting method3 for light (using refernce waveform applied bandpass filter)'''
def template2(data, ref, init, fini, base_init, base_fini, lowcut, highcut, order, wig=0):
    try:
        sampling_rate = 100e3

        filt_ref = ref
        data = data - np.mean(data[base_init:base_fini])
        filt_data = butter_bandpass_filter(data - data[0], lowcut, highcut, sampling_rate, order)

        res = stats.linregress(filt_ref[init:fini], filt_data[init + wig:fini + wig])
    except:
        print("failed lingress fitting")
        res = np.array([0, 0, 0, 0, 0])
    return res

def template2_least(data, ref_wave, init, fini, base_init, base_fini, lowcut, highcut, order, ntry):
    res_arr = np.empty([2 * ntry, 5])

    for i in range(-ntry, ntry):
        res = template2(data, ref_wave, init, fini, base_init, base_fini, lowcut, highcut, order, int(i))
        res_arr[i] = res[0], res[1], res[2], res[3], res[4]

    min_res = res_arr[np.argmin(res_arr[:, 4])]
    return min_res

'''template fitting method time sink each max'''
def template3(data, ref, ini, fin, lowcut, highcut, order, wig=0):
    try:
        sampling_rate = 100e3

        filt_data = butter_bandpass_filter(data - data[0], lowcut, highcut, sampling_rate, order)

        ref_max = np.argmax(ref[20000:22000]) + 20000
        fdata_max = np.argmax(filt_data[20000:22000]) + 20000

        res = stats.linregress(ref[ref_max-int(ini):ref_max+int(fin)+int(wig)], filt_data[fdata_max-int(ini):fdata_max+int(fin)+int(wig)])
    except:
        #print("failed lingress fitting")
        res = np.array([0, 0, 0, 0, 0])
    return res

def template3_least(data, ref_wave, ini, fin, lowcut, highcut, order, ntry):
    res_arr = np.empty([2 * ntry, 5])

    for i in range(-ntry, ntry):
        res = template3(data, ref_wave, ini, fin, lowcut, highcut, order, int(i))
        res_arr[i] = res[0], res[1], res[2], res[3], res[4]

    min_res = res_arr[np.argmin(res_arr[:, 4])]
    return min_res


### template fitting method for light channel time sink each max
def template4(data, ref, ini, fin, lowcut, highcut, order, wig=0):
    try:
        filt_data = ref
        ref_max = np.argmax(ref[20000:22000]) + 20000
        fdata_max = np.argmax(filt_data[20000:22000]) + 20000

        res = stats.linregress(ref[ref_max-int(ini):ref_max+int(fin)+int(wig)], filt_data[fdata_max-int(ini):fdata_max+int(fin)+int(wig)])
    except:
        #print("failed lingress fitting")
        res = np.array([0, 0, 0, 0, 0])
    return res

def template4_least(data, ref_wave, ini, fin, lowcut, highcut, order, ntry):
    res_arr = np.empty([2 * ntry, 5])

    for i in range(-ntry, ntry):
        res = template4(data, ref_wave, ini, fin, lowcut, highcut, order, int(i))
        res_arr[i] = res[0], res[1], res[2], res[3], res[4]

    min_res = res_arr[np.argmin(res_arr[:, 4])]
    return min_res


def myfft2(ayy_tmp_ch3, sampling_rate, sampling_interval):
    n_sample = len(ayy_tmp_ch3)
    t = np.arange(0,n_sample*sampling_interval, sampling_interval)
    k = np.arange(n_sample)
    T = n_sample/sampling_rate
    frq_bin = 1/T
    frq = k/T
    frq = frq[:n_sample//2]
    Yr = np.fft.fft(ayy_tmp_ch3)
    Y = Yr/n_sample
    Y = Y[:n_sample//2]

    return frq, 2*abs(Y)/np.sqrt(frq_bin), Yr

def optimal_function2(tt, pulse_fft, template_fft, noise_pow, sampling_rate, tmax):
    f_bin = (1./tt[-1])
    N1 = len(tt)
    freqspec = np.fft.fftfreq(tt.shape[-1],1./sampling_rate)


    iomegat = np.exp(-1j*2*np.pi*freqspec*tmax)
    optimal_fft = iomegat*template_fft.conjugate()*pulse_fft/noise_pow
    optimal_ifft = np.fft.ifft(optimal_fft)

    facDM_N2 =\
    (pulse_fft*template_fft.conjugate() + pulse_fft.conjugate()*template_fft)/(2*noise_pow)
    sample_freq_negative = freqspec[N1//2:]
    facDM_N2_negative = facDM_N2[N1//2:]
    sample_freq_negative = np.append(sample_freq_negative, freqspec[0])
    facDM_N2_negative = np.append(facDM_N2_negative, facDM_N2[0])
    sample_freq_positive = freqspec[:N1//2]
    facDM_N2_positive = facDM_N2[:N1//2]
    I1_facDM_N2 = simps(facDM_N2_positive,sample_freq_positive)
    I2_facDM_N2 = simps(facDM_N2_negative,sample_freq_negative)
    I_facDM_N2 = I1_facDM_N2 + I2_facDM_N2


    #print ("I1,2_facDM_N2:",I1_facDM_N2,I2_facDM_N2)

    facM2_N2 =\
    template_fft*template_fft.conjugate()/(noise_pow)
    facM2_N2_negative = facM2_N2[N1//2:]
    facM2_N2_negative = np.append(facM2_N2_negative, facM2_N2[0])
    facM2_N2_positive = facM2_N2[:N1//2]
    I1_facM2_N2 = simps(facM2_N2_positive,sample_freq_positive)
    I2_facM2_N2 = simps(facM2_N2_negative,sample_freq_negative)
    I_facM2_N2 = I1_facM2_N2 + I2_facM2_N2

    #print ("I1,2_facM2_N2:",I1_facM2_N2,I2_facM2_N2)

    A1 = I_facDM_N2/I_facM2_N2

    A2 = np.max(np.abs(optimal_ifft))

    #print ("A1,2:",A1, A2)

    return A1, A2


def optimal_fit(noise, frq, ref, pulse, xhpeak, ini, fin):
    try :
        xhpeak = int(xhpeak)
        tt = np.linspace(0, (fin+ini)*10e-6, int(fin+ini))
        pulse = pulse - np.mean(pulse[xhpeak-2500:xhpeak-1500])

        # noise_fft
        y2 = noise
        frq2 = frq
        y2_base = y2 * y2.conjugate()
        y2_base_tmp = np.append(y2_base, y2_base[-1])
        y2_base_reverse = np.flip(y2_base_tmp[1:])
        y2_base_ext = np.append(y2_base, y2_base_reverse)
        noise_fft = y2_base_ext

        # ref_fft
        ref = ref[xhpeak-int(ini):xhpeak+int(fin)]

        pulse_fft = np.fft.fft(pulse[xhpeak-int(ini):xhpeak+int(fin)])
        ref_fft = np.fft.fft(ref)

        tmax_ref = np.argmax(pulse[xhpeak-int(ini):xhpeak+int(fin)]) / 100e3

        aa = optimal_function2(tt, pulse_fft, ref_fft, noise_fft, 100e3, tmax_ref)

        r = np.real(aa[0])

    except ValueError :
        r = 0

    return r



#def heat_ana(heat_array, trg, base_ini=17000, dt_x=25000, wid=100)
#def heat_filter_ana(heat_array, trg, wid, lowcut, highcut, order, sampling_rate, base_ini=17000)
'''Time domain optimal filtering'''
def filt (ref, covM, ini, fin) :
    try :
        r = heat_ana(ref, 20800, 35000)
        rt10 = int(r[2][-2])

        ref = ref[rt10-ini: rt10+fin]
        ref_T = ref.reshape(1,len(ref))
        ref_T = ref_T

        filt1 = np.dot(ref_T, np.linalg.inv(covM))
        filt2 = np.linalg.inv(np.dot(filt1, ref_T.T))
        filt3 = np.dot(filt2, ref_T)
        filt4 = np.dot(filt3, np.linalg.inv(covM))
        filt4 = filt4[0]
    except ValueError :
        filt4 = np.zeros(len(ref))

    return filt4

def of_time(data, xhpeak, ini, fin, filt) :
    try :
        arr = data - np.mean(data[20500:20700])
        arr = arr[xhpeak-ini:xhpeak+fin]
        arr = np.array(arr).reshape(len(arr), 1)

        filt = np.array(filt).reshape(1, np.shape(filt)[0])

        of_amp = np.dot(filt, arr)
        of_amp = np.array(of_amp)[0][0]
    except :
        of_amp = -1

    return of_amp