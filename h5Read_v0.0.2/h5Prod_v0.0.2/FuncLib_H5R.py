import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import h5py, os, glob, datetime, time, subprocess, threading, sys, psutil

def Read_WAVE(file_name, ch, n_arr, n_arr_x) :
    init= 18000

    try :
        with h5py.File(file_name, 'r') as f:
            dset = f['rawdata']

            data_1 = np.array(dset[n_arr-1][2][j][ch])
            data_2 = np.array(dset[n_arr][2][j][ch])
            data_3 = np.array(dset[n_arr+1][2][j][ch])

            data = np.array([])
            data = np.append(data, data_1)
            data = np.append(data, data_2)
            data = np.append(data, data_3)

            wave_init = int(32768+int(n_arr_x)-init)
            wave = data[wave_init:wave_init+50000]

    except :
        wave = np.zeros(50000)
        print ("Error!")

    return wave