import numpy as np
import h5py, os , sys, psutil, datatime, glob
import pandas as pd

def save_Wave_Info_to_txt (ifname, runNum, subrunNum, n_arr, n_arr_x) :
    a = np.array([subrunNum, runNum, n_arr, n_arr_x])
    try :
        np.savetxt('./REF/{0}.txt'.format(ifname), a, fmt='%s')
    except :
        print ("fail....")

def read_Wave_Info_from_txt(path, runNum, ch, subrunNum, n_arr, n_arr_x) :
    #ifname = "{0}/*{1}_{2}*".format(path, runNum, subrunNum)
    fpath = path+"./{0}/".format(runNum)
    flist = sorted(glob.glob(fpath))
    ifname = flist[int(subrunNum)]

    try :
        with h5py.File(ifname, 'r') as f :
            dset = f['rawdata']

            n_arr = int(n_arr)
            heat_0 = np.array(dset[n_arr-1][2][int(ch)])
            heat_1 = np.array(dset[n_arr][2][int(ch)])
            heat_2 = np.array(dset[n_arr+1][2][int(ch)])
            heat = np.concatenate((heat_0, heat_1, heat_2))

            n_arr_x = int(n_arr_x + len(heat_0))
            heat = heat[n_arr_x-18000: n_arr_x+32000]

            light_0 = np.array(dset[n_arr - 1][2][int(ch)-1])
            light_1 = np.array(dset[n_arr][2][int(ch)-1])
            light_2 = np.array(dset[n_arr + 1][2][int(ch)-1])
            light = np.concatenate((light_0, light_1, light_2))

            light = light[n_arr_x-18000: n_arr_x+32000]

        return heat, light
    except :
        print ("fail.......")

def save_Wave_to_h5_init(RL) :
    df1 = pd.DataFrame({'heat' : [np.zeros(RL)]})
    df2 = pd.DataFrame({'light' : [np.zeros(RL)]})
    return df1, df2

def save_Wave_to_h5(df_heat, df_light, heat, light) :
    df1 = pd.DataFrame({'heat' : heat})
    df2 = pd.DataFrame({'light' : light})
    df_heat = df_heat.append(df1, ignore_index='True')
    df_light = df_light.append(df2, ignore_index='True')
    return df_heat, df_light

def save_Wave_to_h5_fini(ofname, df_heat, df_light) :
    df_heat.to_hdf('./{0}.h5'.format(ofname), 'heat')
    df_light.to_hdf('./{0}.h5'.format(ofname), 'light')
    