import numpy as np
import h5py, os, glob, datetime, time, subprocess, threading, sys, psutil
import matplotlib.pyplot as plt
import pandas as pd

from array import array
from scipy.signal import *

import FuncLib_Objs as func_objs
import FuncLib_H5W as func_h5w

conversion_factor = 10./2**18
sampling_rate = 100e3

file_array = ['/data/AMoRE/users/kimwootae/RAW/2021_1st_RODY/20210326/amorecont_20210326_1414_10mK.h5.00000' ,\
              '/data/AMoRE/users/kimwootae/RAW/2021_1st_RODY/20210326/amorecont_20210326_1414_10mK.h5.00001',\
              '/data/AMoRE/users/kimwootae/RAW/2021_1st_RODY/20210326/amorecont_20210326_1414_10mK.h5.00002',\
              '/data/AMoRE/users/kimwootae/RAW/2021_1st_RODY/20210326/amorecont_20210326_1414_10mK.h5.00003',\
              '/data/AMoRE/users/kimwootae/RAW/2021_1st_RODY/20210326/amorecont_20210326_1414_10mK.h5.00004',\
              '/data/AMoRE/users/kimwootae/RAW/2021_1st_RODY/20210326/amorecont_20210326_1414_10mK.h5.00005',\
              '/data/AMoRE/users/kimwootae/RAW/2021_1st_RODY/20210326/amorecont_20210326_1414_10mK.h5.00006',\
              '/data/AMoRE/users/kimwootae/RAW/2021_1st_RODY/20210326/amorecont_20210326_1414_10mK.h5.00007',\
              '/data/AMoRE/users/kimwootae/RAW/2021_1st_RODY/20210326/amorecont_20210326_1414_10mK.h5.00008',\
              '/data/AMoRE/users/kimwootae/RAW/2021_1st_RODY/20210326/amorecont_20210326_1414_10mK.h5.00009']

'''TRIGGER'''
def TRG(ch_array, thr) :
    ad = ch_array - thr

    ## 1st trigger
    n_trg1 = np.prod([ad[1:], ad[:-1]], axis=0)
    trg1 = (n_trg1 < 0)
    trg1 = np.append(trg1, False)

    ## 2nd trigger
    n_trg2 = np.diff(ch_array)
    n_trg2 = np.append(n_trg2, 0)
    trg2 = (n_trg2 > thr*0.01)

    ## Finial trigger
    trgf = np.all([trg1, trg2], axis=0)

    return np.where(trgf==True)


def READ5PY(file_array, thr, ncrystal):
    init = 1000
    fini = 4000
    lowcut = 200
    highcut = 500
    fs = 100e+3

    nch = ncrystal * 2

    pd_heat = init_pd()

    for nn, file_name in enumerate(file_array):
        print('file number : ', nn)

        with h5py.File(file_name, 'r') as f:
            print("Keys : %s" % f.keys())
            print(list(f.keys()))

            dset = f['rawdata']
            print(dset.dtype)
            print(dset.shape)

            mod = 1
            lch = ncrystal  # of local channel
            gch = mod * lch  # of global channel
            print("total global channel : ", gch)

            h_gadc = [[[], [], [], [], [], [], [], []] for _ in range(mod)]

            for ii in range(int(dset.shape[0])) :
            #for ii in range(10):
                data = np.array(dset[ii][2])
                tt = np.array(dset[ii][1])

                if ((ii == 0) or (ii % 100 == 0)):
                    print("file : ", file_name, ", process time : ", datetime.datetime.now())
                    pid = os.getpid()
                    current_process = psutil.Process(pid)
                    current_process_memory_usage_as_KB = current_process.memory_info()[0] / 2. ** 20
                    print(f"Current memory KB : {current_process_memory_usage_as_KB : 9.3f} KB")

                for j in range(mod):
                    # for k in range(3,8,2) :
                    h_y = np.array(data[j][5])

                    h_gadc[j][5].append(h_y)

                    if (len(h_gadc[j][5]) <= 3):
                        continue

                    if (len(h_gadc[j][5]) > 3):
                        del h_gadc[j][5][0]

                    h_yy = np.array(np.reshape(h_gadc[j][5], -1)).astype('int')

                    yyx = np.array(h_yy[32768:65536])
                    yyy = func_objs.butter_bandpass_filter(yyx - yyx[0], lowcut, highcut, fs, order=1)
                    testx = TRG(yyy, thr)

                    if ((len(testx) < 1) or (len(testx[0]) < 1)): continue
                    for jj in (testx[0]):
                        jj = int(jj)
                        trgon = 1
                        t = tt[j] * 1e-9 + (jj + 32768 - init) * 1e-6
                        hh_y = h_yy[jj + int(32768) - init:jj + int(32768) + fini]

                        heat = pd.DataFrame({'ch': [np.array(hh_y)]})
                        pd_heat = pd_heat.append(heat, ignore_index=True)
    return pd_heat

def init_pd():
    heat = pd.DataFrame({'ch' : [np.zeros(5000)]})
    return heat

def run(file_array) :
    a = READ5PY(file_array, 2000, 4)
    a.to_hdf('20210326_lom6.h5', key='heat')
    print (a)

if __name__ == '__main__' :
    run()