import numpy as np
import h5py, os, glob, datetime, time, subprocess, threading, sys, psutil
import matplotlib.pyplot as plt
import pandas as pd

from array import array
from scipy.signal import *

import FuncLib_Objs as func_objs
import FuncLib_H5W as func_h5w

conversion_factor = 10./2**18
sampling_rate = 100e3
base_end = 17000

var1 = sys.argv[1]
var2 = sys.argv[2]
var3 = sys.argv[3]
var4 = sys.argv[4]
var5 = sys.argv[5]
var6 = sys.argv[6]
var7 = sys.argv[7]

h5_path = var1
subrunNum = var2
runN = var3
mergeN = int(var4)
thr = var5
ncrystal = int(var6)
OUTPATH = var7

'''TRIGGER'''
def TRG(ch_array, thr) :
    ad = ch_array - thr

    ## 1st trigger
    n_trg1 = np.prod([ad[1:], ad[:-1]], axis=0)
    trg1 = (n_trg1 < 0)
    trg1 = np.append(trg1, False)

    ## 2nd trigger
    n_trg2 = np.diff(ch_array)
    n_trg2 = np.append(n_trg2, 0)
    trg2 = (n_trg2 > thr*0.01)

    ## Finial trigger
    trgf = np.all([trg1, trg2], axis=0)

    return np.where(trgf==True)

'''READ HDF5 FILE AND USING TRIGGER AND MAKE A PRODUCTIOM FILE (phonon channel : odd, photon channel : even)'''
def READ5PY_1 (file_array, thr, ncrystal, trg_ch) :
    init = 18000
    fini = 32000
    lowcut = 200
    highcut = 500
    fs = 100e+3

    nch = ncrystal*2

    heat_pd, light_pd = func_h5w.init_pd(ncrystal)
    
    for nn, file_name in enumerate(file_array) :
        print ('file number : ', nn)

        with h5py.File(file_name, 'r') as f :
            print ("Keys : %s" %f.keys())
            print (list(f.keys()))

            dset = f['rawdata']
            print (dset.dtype)
            print (dset.shape)

            mod = 1
            lch = ncrystal # of local channel
            gch = mod*lch # of global channel
            print ("total global channel : ", gch)

            h_gadc = [[[],[],[],[],[],[],[],[]] for _ in range(mod)]
            l_gadc = [[[],[],[],[],[],[],[],[]] for _ in range(mod)]

            for ii in range(int(dset.shape[0])) :
                data = np.array(dset[ii][2])
                tt = np.array(dset[ii][1])
                if ((ii == 0) or (ii % 100 == 0)) :
                    print ("file : ", file_name, ", process time : ", datetime.datetime.now())
                    pid = os.getpid()
                    current_process = psutil.Process(pid)
                    current_process_memory_usage_as_KB = current_process.memory_info()[0]/2.**20
                    print (f"Current memory KB : {current_process_memory_usage_as_KB : 9.3f} KB")

                for j in range(mod) :

                    for k in trg_ch :
                        h_y = np.array(data[j][k])
                        l_y = np.array(data[j][k-1])

                        h_gadc[j][k].append(h_y)
                        l_gadc[j][k].append(l_y)

                        if(len(h_gadc[j][k]) <= 3) :
                            continue

                        if(len(h_gadc[j][k]) > 3) :
                            del h_gadc[j][k][0]
                            del l_gadc[j][k][0]

                        h_yy = np.array(np.reshape(h_gadc[j][k], -1)).astype('int')
                        l_yy = np.array(np.reshape(l_gadc[j][k], -1)).astype('int')

                        yyx = np.array(h_yy[32768:65536])
                        yyy = func_objs.butter_bandpass_filter(yyx-yyx[0], lowcut, highcut, fs, order=1)
                        testx = TRG(yyy, thr)

                        if((len(testx) < 1) or (len(testx[0]) < 1)) : continue

                        for jj in (testx[0]) :
                            jj = int(jj)
                            t = tt[j]*1e-9+(jj+32768-init)*1e-6
                            hh_y = h_yy[jj+int(32768)-init:jj+int(32768)+fini]
                            ll_y = l_yy[jj+int(32768)-init:jj+int(32768)+fini]
                            
                            '''calculation simple parameter'''
                            h_base = np.mean(hh_y[5000:15000])
                            h_rms = np.std(hh_y[5000:15000])
                            l_base = np.mean(ll_y[5000:15000])
                            l_rms = np.std(ll_y[5000:15000])
                            h_ped = np.mean(hh_y[17500:17900])
                            h_pedRMS = np.std(hh_y[17500:17900])

                            '''Production'''
                            h_ana, h_band_ana1, h_band_ana2, h_band_ana3 = heat_prod(np.array(hh_y))
                            l_ana, l_band_ana = light_prod(np.array(ll_y))

                            '''write pandas'''
                            pd_heat, pd_light = func_h5w.write_pd(t, h_ped, h_base, h_rms, l_base, l_rms, h_ana, h_band_ana1, h_band_ana2, h_band_ana3, l_ana, l_band_ana, subrunNum, ii, jj, int(nn))

                            heat_pd[int(k/2)] = heat_pd[int(k/2)].append(pd_heat, ignore_index=True)
                            light_pd[int(k/2)] = light_pd[int(k/2)].append(pd_light, ignore_index=True)

    return heat_pd, light_pd


'''READ HDF5 FILE AND USING TRIGGER AND MAKE A PRODUCTIOM FILE (phonon channel : even, photon channel : odd)'''
def READ5PY_2(file_array, thr, ncrystal, trg_ch):
    init = 18000
    fini = 32000
    lowcut = 200
    highcut = 500
    fs = 100e+3

    nch = ncrystal * 2

    heat_pd, light_pd = func_h5w.init_pd(ncrystal)

    for nn, file_name in enumerate(file_array):
        print('file number : ', nn)

        with h5py.File(file_name, 'r') as f:
            print("Keys : %s" % f.keys())
            print(list(f.keys()))

            dset = f['rawdata']
            print(dset.dtype)
            print(dset.shape)

            mod = 1
            lch = ncrystal  # of local channel
            gch = mod * lch  # of global channel
            print("total global channel : ", gch)

            h_gadc = [[[], [], [], [], [], [], [], []] for _ in range(mod)]
            l_gadc = [[[], [], [], [], [], [], [], []] for _ in range(mod)]

            for ii in range(int(dset.shape[0])):
                data = np.array(dset[ii][2])
                tt = np.array(dset[ii][1])
                if ((ii == 0) or (ii % 100 == 0)):
                    print("file : ", file_name, ", process time : ", datetime.datetime.now())
                    pid = os.getpid()
                    current_process = psutil.Process(pid)
                    current_process_memory_usage_as_KB = current_process.memory_info()[0] / 2. ** 20
                    print(f"Current memory KB : {current_process_memory_usage_as_KB : 9.3f} KB")

                for j in range(mod):

                    for k in trg_ch:
                        h_y = np.array(data[j][k])
                        l_y = np.array(data[j][k + 1])

                        h_gadc[j][k].append(h_y)
                        l_gadc[j][k].append(l_y)

                        if (len(h_gadc[j][k]) <= 3):
                            continue

                        if (len(h_gadc[j][k]) > 3):
                            del h_gadc[j][k][0]
                            del l_gadc[j][k][0]

                        h_yy = np.array(np.reshape(h_gadc[j][k], -1)).astype('int')
                        l_yy = np.array(np.reshape(l_gadc[j][k], -1)).astype('int')

                        yyx = np.array(h_yy[32768:65536])
                        yyy = func_objs.butter_bandpass_filter(yyx - yyx[0], lowcut, highcut, fs, order=1)
                        testx = TRG(yyy, thr)

                        if ((len(testx) < 1) or (len(testx[0]) < 1)): continue

                        for jj in (testx[0]):
                            jj = int(jj)
                            t = tt[j] * 1e-9 + (jj + 32768 - init) * 1e-6
                            hh_y = h_yy[jj + int(32768) - init:jj + int(32768) + fini]
                            ll_y = l_yy[jj + int(32768) - init:jj + int(32768) + fini]

                            '''calculation simple parameter'''
                            h_base = np.mean(hh_y[5000:15000])
                            h_rms = np.std(hh_y[5000:15000])
                            l_base = np.mean(ll_y[5000:15000])
                            l_rms = np.std(ll_y[5000:15000])
                            h_ped = np.mean(hh_y[17500:17900])
                            h_pedRMS = np.std(hh_y[17500:17900])

                            '''Production'''
                            h_ana, h_band_ana1, h_band_ana2, h_band_ana3 = heat_prod(np.array(hh_y))
                            l_ana, l_band_ana = light_prod(np.array(ll_y))

                            '''write pandas'''
                            pd_heat, pd_light = func_h5w.write_pd(t, h_ped, h_base, h_rms, l_base, l_rms, h_ana,
                                                                  h_band_ana1, h_band_ana2, h_band_ana3, l_ana,
                                                                  l_band_ana, subrunNum, ii, jj, int(nn))

                            heat_pd[int(k / 2)] = heat_pd[int(k / 2)].append(pd_heat, ignore_index=True)
                            light_pd[int(k / 2)] = light_pd[int(k / 2)].append(pd_light, ignore_index=True)

    return heat_pd, light_pd

def heat_prod(h) :
    base_end = 17000
    baseline = np.mean(h[14000:17000])
    baselineRMS = np.std(h[14000:17000])
    heat_ana = func_objs.heat_ana(h, conversion_factor, sampling_rate, baseline, baselineRMS, base_end)
    order = 1
    h_band_ana1 = func_objs.heat_filter_ana(200, 500, order, h-h[0], conversion_factor, sampling_rate, base_end)
    h_band_ana2 = func_objs.heat_filter_ana(30, 220, order, h-h[0], conversion_factor, sampling_rate, base_end)
    h_band_ana3 = func_objs.heat_filter_ana(1000, 1200, order, h-h[0], conversion_factor, sampling_rate, base_end)
    
    return heat_ana, h_band_ana1, h_band_ana2, h_band_ana3

def light_prod(l) :
    base_end = 17000
    baseline = np.mean(l[14000:17000])
    baselineRMS = np.std(l[14000:17000])
    light_ana = func_objs.light_ana(l-l[0], conversion_factor, sampling_rate, baseline, baselineRMS, base_end)
    order = 1
    l_band_ana = func_objs.light_filter_ana(200, 500, order, l-l[0], conversion_factor, 100e3, base_end)

    return light_ana, l_band_ana

def run() :
    h5files = h5_path + '*h5*'
    a = func_h5w.SORTINGFILE(h5files, mergeN)
    inputfiles = a[int(subrunNum)]
    thr = 100 #Threshold
    ncrystal = 4 #number of crystal
    trg_ch = [0,2,4,6] #trigger channel == phonon channel
    heat_pd, light_pd = READ5PY_2(inputfiles, thr=int(thr), ncrystal=ncrystal, trg_ch=trg_ch)
    func_h5w.save_pd(OUTPATH=OUTPATH, runN=runN, subrunNum=subrunNum, pd_heat=heat_pd, pd_light=light_pd, ncrystal=ncrystal)

if __name__ == '__main__' :
    run()