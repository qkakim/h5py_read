import numpy as np
import h5py, os, glob, datetime, time, subprocess, threading, sys
import pandas as pd

'''Sort files'''
def SORTINGFILE(n_files, mergeN) :
    #files = sorted(glob.glob(n_files), key=os.path.getmtime)
    files = sorted(glob.glob(n_files))
    n = len(files)
    infiles = []
    tfileGrup = []
    i = 0
    for filename in files :
        infiles.append(filename)
        if(len(infiles) == mergeN) :
            i += 1
            tfileGrup.append(infiles)
            infiles = []
        elif((len(files)//mergeN)==i and (len(files)%mergeN == len(infiles))) :
            tfileGrup.append(infiles)
    return tfileGrup


def init_pd(nch):
    pd_heat = [pd.DataFrame({'time': [0], 'xhpeak': [0], 'hpeak': [0]\
                                , 'hrt90': [0], 'hrt70': [0], 'hrt50': [0], 'hrt30': [0],'hrt20': [0], 'hrt15': [0], 'hrt10': [0], 'hrt5': [0] \
                                , 'hrt90_h': [0], 'hrt70_h': [0], 'hrt50_h': [0], 'hrt30_h': [0],'hrt20_h': [0], 'hrt15_h': [0], 'hrt10_h': [0], 'hrt5_h': [0] \
                                , 'hdt90': [0], 'hdt70': [0], 'hdt50': [0], 'hdt30': [0], 'hdt10': [0] \
                                , 'hdt90_h': [0], 'hdt70_h': [0], 'hdt50_h': [0], 'hdt30_h': [0], 'hdt10_h': [0] \
                                , 'hpedestal': [0], 'hendline': [0], 'hendlineRMS': [0], 'slope' : [0] \
                                , 'hbaseline': [0], 'hbaselineRMS': [0] \
                                , 'qsum90' : [0], 'qsum70' : [0], 'qsum50' : [0], 'qsum30' : [0], 'qsum20' : [0], 'qsum15' : [0], 'qsum10' : [0], 'qsum5' : [0]\
                                , 'xhfpeak1': [0], 'hfpeak1': [0], 'hfmin1': [0], 'fit_hfpeak1': [0], 'fit_hfmin1': [0], 'fit_xhfpeak1': [0], 'fit_xhfmin1': [0], 'hfbaseline1': [0], 'hfbaselineRMS1': [0] \
                                , 'xhfpeak2': [0], 'hfpeak2': [0], 'hfmin2': [0], 'fit_hfpeak2': [0], 'fit_hfmin2': [0], 'fit_xhfpeak2': [0], 'fit_xhfmin2': [0], 'hfbaseline2': [0], 'hfbaselineRMS2': [0] \
                                , 'xhfpeak3': [0], 'hfpeak3': [0], 'hfmin3': [0], 'fit_hfpeak3': [0], 'fit_hfmin3': [0], 'fit_xhfpeak3': [0], 'fit_xhfmin3': [0], 'hfbaseline3': [0], 'hfbaselineRMS3': [0] \
                                , 'xhfpeak4': [0], 'hfpeak4': [0], 'hfmin4': [0], 'fit_hfpeak4': [0], 'fit_hfmin4': [0], 'fit_xhfpeak4': [0], 'fit_xhfmin4': [0], 'hfbaseline4': [0], 'hfbaselineRMS4': [0] \
                                , 'subrunNum': [0], 'n_arr' : [0], 'n_arr_x' : [0], 'nn' : [-1]}) for _ in range(nch)]

    pd_light = [pd.DataFrame({'time': [0], 'xlpeak': [0], 'lpeak': [0], 'lrt_90': [0], 'lrt_50': [0], 'lrt_10': [0] \
                                 , 'lrt90_x': [0], 'lrt50_x': [0], 'lrt10_x': [0] \
                                 , 'lpedestal': [0], 'lendline': [0], 'lendlineRMS': [0] \
                                 , 'lbaseline': [0], 'lbaselineRMS': [0] \
                                 , 'xlfpeak': [0], 'lfpeak': [0], 'xlfmin': [0], 'lfmin': [0], 'lfbaseline': [0]\
                                 , 'lfbaselineRMS': [0], 'subrunNum': [0], 'n_arr' : [0]}) for _ in range(nch)]
    return pd_heat, pd_light


def write_pd(t, h_ped, h_base, h_rms, l_base, l_rms, h_ana, h_band_ana1, h_band_ana2, h_band_ana3, l_ana, l_band_ana,
             subrunNum, n_arr, n_arr_x, nn):
    hrt90, hrt70, hrt50, hrt30, hrt20, hrt15, hrt10, hrt5 = h_ana[2]
    hrt90_h, hrt70_h, hrt50_h, hrt30_h, hrt20_h, hrt15_h, hrt10_h, hrt5_h = h_ana[3]
    hdt90, hdt70, hdt50, hdt30, hdt10 = h_ana[4]
    hdt90_h, hdt70_h, hdt50_h, hdt30_h, hdt10_h = h_ana[5]
    qsum90, qsum70, qsum50, qsum30, qsum20, qsum15, qsum10, qsum5 = h_ana[6]
    pd_h_ana = pd.DataFrame(
        {'time': [t], 'xhpeak': [h_ana[0]], 'hpeak': [h_ana[1]]
            , 'hrt90': [hrt90], 'hrt70': [hrt70], 'hrt50': [hrt50], 'hrt30': [hrt30], 'hrt20': [hrt20], 'hrt15': [hrt15], 'hrt10': [hrt10], 'hrt5': [hrt5] \
            , 'hdt90': [hdt90], 'hdt70': [hdt70], 'hdt50': [hdt50], 'hdt30': [hdt30], 'hdt10': [hdt10] \
            , 'hrt90_h': [hrt90_h], 'hrt70_h': [hrt70_h], 'hrt50_h': [hrt50_h], 'hrt30_h': [hrt30_h], 'hrt20_h': [hrt20_h], 'hrt15_h': [hrt15_h], 'hrt10_h': [hrt10_h], 'hrt5_h': [hrt5_h] \
            , 'hdt90_h': [hdt90_h], 'hdt70_h': [hdt70_h], 'hdt50_h': [hdt50_h], 'hdt30_h': [hdt30_h], 'hdt10_h': [hdt10_h] \
            , 'qsum90': [qsum90], 'qsum70': [qsum70], 'qsum50': [qsum50], 'qsum30': [qsum30], 'qsum20': [qsum20], 'qsum15': [qsum15], 'qsum10': [qsum10], 'qsum5': [qsum5] \
            , 'hpedestal': [h_ped], 'hendline': [h_ana[7]], 'hendlineRMS': [h_ana[8]], 'slope' : [h_ana[-1]] \
            , 'hbaseline': [h_base], 'hbaselineRMS': [h_rms] \
            , 'xhfpeak1': [h_band_ana1[0]], 'hfpeak1': [h_band_ana1[1]], 'xhfmin1': [h_band_ana1[4]], 'hfmin1': [h_band_ana1[5]], 'fit_xhfpeak1': [h_band_ana1[8]], 'fit_xhfmin1': [h_band_ana1[10]], 'fit_hfpeak1': [h_band_ana1[9]], 'fit_hfmin1': [h_band_ana1[11]], 'hfbaseline1': [h_band_ana1[2]], 'hfbaselineRMS1': [h_band_ana1[3]] \
            , 'xhfpeak2': [h_band_ana2[0]], 'hfpeak2': [h_band_ana2[1]], 'xhfmin2': [h_band_ana2[4]], 'hfmin2': [h_band_ana2[5]], 'fit_xhfpeak2': [h_band_ana2[8]], 'fit_xhfmin2': [h_band_ana2[10]], 'fit_hfpeak2': [h_band_ana2[9]], 'fit_hfmin2': [h_band_ana2[11]], 'hfbaseline2': [h_band_ana2[2]], 'hfbaselineRMS2': [h_band_ana2[3]] \
            , 'xhfpeak3': [h_band_ana3[0]], 'hfpeak3': [h_band_ana3[1]], 'xhfmin3': [h_band_ana3[4]], 'hfmin3': [h_band_ana3[5]], 'fit_xhfpeak3': [h_band_ana3[8]], 'fit_xhfmin3': [h_band_ana3[10]], 'fit_hfpeak3': [h_band_ana3[9]], 'fit_hfmin3': [h_band_ana3[11]], 'hfbaseline3': [h_band_ana3[2]], 'hfbaselineRMS3': [h_band_ana3[3]] \
            , 'subrunNum': [subrunNum], 'n_arr': [n_arr], 'n_arr_x' : [n_arr_x], 'nn' : [nn]})
    pd_l_ana = pd.DataFrame(
        {'time': [t], 'xlpeak': [l_ana[0]], 'lpeak': [l_ana[1]], 'lrt_90': [l_ana[2]], 'lrt_50': [l_ana[3]], 'lrt_10': [l_ana[4]] \
            , 'lrt90_x': [l_ana[5]], 'lrt50_x': [l_ana[6]], 'lrt10_x': [l_ana[7]] \
            , 'lpedestal': [l_ana[14]], 'lendline': [l_ana[11]], 'lendlineRMS': [l_ana[12]] \
            , 'lbaseline': [l_base], 'lbaselineRMS': [l_rms] \
            , 'xlfpeak': [l_band_ana[0]], 'lfpeak': [l_band_ana[1]], 'xlfmin': [l_band_ana[4]], 'lfmin': [l_band_ana[5]], 'lfbaseline': [l_band_ana[2]], 'lfbaselineRMS': [l_band_ana[3]] \
            , 'subrunNum': [subrunNum], 'n_arr' : [n_arr]})

    return pd_h_ana, pd_l_ana

def save_pd(OUTPATH, runN, subrunNum, pd_heat, pd_light, ncrystal) :
    for i in range(ncrystal) :
        pd_heat[i].to_hdf(OUTPATH+'{0}/PROD_ch{1}_{2}.h5'.format(runN, i, subrunNum), key='heat', mode='w')
    for i in range(ncrystal) :
        pd_light[i].to_hdf(OUTPATH+'{0}/PROD_ch{1}_{2}.h5'.format(runN, i, subrunNum), key='light')