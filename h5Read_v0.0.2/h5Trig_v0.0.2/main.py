import numpy as np
import sys, os, glob, datetime, time, subprocess, threading, h5py

import FuncLib_H5W as func_h5w

var1 = sys.argv[1]
runN = var1

thr = 100
ncrystal = 4
mergeN = int(10)

'''Input Run Number'''
#h5_path = '/data/AMoRE/users/kimwootae/RAW/2020_2nd_RODY/{0}/'.format(runN)
h5_path = '/data/AMoRE/users/kimwootae/RAW/2021_1st_RODY/{0}/'.format(runN)
h5files = h5_path+'*h5*'
path = '/data/AMoRE/users/kimwootae/work/h5py_read/h5Read_v0.0.2/h5Trig_v0.0.2'
exepath = path+'/Shell/'
OUTPATH = '/data/AMoRE/users/kimwootae/PROD/2021_1st/RODY/h5_v2TEST/'

def MakeBashSh(subrunNum) :
    shName = 'rody_{0}'.format(runN)
    shName = exepath+'{0}_{1}.sh'.format(shName, subrunNum)
    with open(shName, 'w') as rsh :
        rsh.write('''\

##!/bin/bash

path={1}
subrunNum={2}
source /share/amore/anaconda3/etc/profile.d/conda.sh
source /share/amore/ROOT/6.14.04-v01.01/bin/thisroot.sh
source /home/kimwootae/env/pyroot.sh
conda activate v01

log_file={0}/LOSG/test_{2}.log
python {0}/h5run.py {1} {2} {3} {4} {5} {6} {7}
'''.format(path, h5_path,subrunNum, runN, mergeN, thr, ncrystal, OUTPATH))
    return shName

'''run bash script of QJOB'''
def JobShell(execute) :
    subprocess.Popen(["qsub -q short {0}".format(execute)],shell=True)

def Threading(execute) :
    my_thread = threading.Thread(target=JobShell(execute))
    my_thread.start()

def run() :
    tfileGrup = func_h5w.SORTINGFILE(h5files, mergeN)
    print (h5files) 
    shell_array = []
    for i in range(len(tfileGrup)) :
        a = MakeBashSh(i)
        shell_array.append(a)
     
    for i in range((len(shell_array))) :
        Threading(shell_array[i])
    
    print (shell_array)
    
if __name__ == "__main__" : 
    run()
