import h5py
import numpy as np
import time
import sys

from scipy.signal import butter, lfilter

from Trigg import bw_Trig
start_time = time.time()

nnum = sys.argv[1]
nsub = sys.argv[2]

path = "/data/AMoRE/users/kimwootae/work/H5/20200401/"
input_file = path+"AMOREADCCONT_{0:06d}.h5.{1:05d}".format(int(nnum), int(nsub))
#input_file = path+"AMOREADCCONT_{0:06d}.h5.*".format(int(nnum))

lowcut = 30
highcut = 220
sampling_rate = 100e+3

def butter_bandpass(lowcut, highcut, fs, order=5) :
    nyq = 0.5*fs
    low = lowcut/nyq
    high = highcut/nyq
    b, a = butter(order, [low, high], btype='band')


with h5py.File(input_file, 'r') as f :
    print (list(f.keys()))

    dset = f['rawdata']
    #print (dset.dtype)
    #print (dset.shape)
    
    n_evt = int(dset.shape[0])
    n_evt = int(n_evt//2)*2

    nch = 16
    nmod = 3
    print ("total eventnumber : ", "n_evt")

    trg = [[False]*16 for _ in range(nmod)]
    t_adc = [[[] for _ in range(int(nch//2))] for _ in range(nmod)]
    rtime = [[[] for _ in range(int(nch//2))] for _ in range(nmod)]
    n_adc = [[[] for _ in range(int(nch//2))] for _ in range(nmod)]
    bw = [[[] for _ in range(int(nch))] for _ in range(nmod)]
    bw_x = [[[] for _ in range(int(nch))] for _ in range(nmod)]
    x = [[[] for _ in range(int(nch))] for _ in range(nmod)]
    y = [[[] for _ in range(int(nch))] for _ in range(nmod)]
    tt = [[[] for _ in range(int(nch))] for _ in range(nmod)]
    
    for i in range(n_evt) :
        data1 = np.array(dset[i][2])
        
        for j in range(nmod) :
            
            ##test plt
            fig, ax = plt.subplots(2,8, figsize=(4*2, 2*8))
            plt.ion()

            for k in range(1, nch, 2) :
                kk = int(k/2)
                t_adc[j][kk].append(data1[j][k-1])
                n_adc[j][kk].append(data1[j][k])
                
                if (len(n_adc[j][kk]) <= 3) :
                    continue

                if (len(n_adc[j][kk]) > 4) :
                    del n_adc[j][kk][0]
                    del t_adc[j][kk][0]

                bw[j][k] = butter_bandpass_filter(n_adc[j][kk][1]- n_adc[j][kk][1][0], lowcut, highcut, sampling_rate, order=1)
                bw_x[j][k] = bw_Trig.bw_Trig(bw[j][k])

                if((bw_x[j][k] > 0) and (trg[j][k] == False)) :
                    trg[j][k] == True
                    trgon[j][k][0] = 1
                    trgon[j][k-1][0] = 1
                    x[j][k] = np.reshape(np.array(n_adc[j][kk]), -1)
                    y[j][k] = np.reshape(np.array(t_adc[j][kk]), -1)
                    
                    trgpnt = bw_x[j][k] + 32768 - 8000
                    trgpnt = int(trgpnt)
                    x[j][k] = x[j][k][trgpnt:25000+trgpnt]
                    y[j][k] = y[j][k][trgpnt:25000+trgpnt]
                    
                    #test plt
                    ax[0][k].plot(x[j][k])
                    ax[1][k].plot(x[j][k])

                    
            #test plt
            plt.show()

            a = input()
            if (a == 'q') : break
            
            for k in range(1, nch, 2) :
                trg[j][k] = False
                bw_x[j][k] = 0
                x[j][k] = []
                y[j][k] = []




                    

