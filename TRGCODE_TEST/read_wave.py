import h5py
import numpy as np
import matplotlib.pyplot as plt
import time
import sys

nnum = sys.argv[1]
subnum = sys.argv[2]
path = "/data/AMoRE/users/kimwootae/work/H5/"
in_file = path+"20200401/AMOREADCCONT_{0:06d}.h5.{1:05d}".format(int(nnum), int(subnum))

def fft(ayy_tmp_ch3, sampling_rate, sampling_interval, idraw) :
    n_sample = len(ayy_tmp_ch3)
    t = np.arange(0,n_sample*sampling_interval, sampling_interval)
    k = np.arange(n_sample)
    T = n_sample/sampling_rate
    frq_bin = 1/T
    frq = k/T
    frq = frq[:n_sample//2]
    Yr = np.fft.fft(ayy_tmp_ch3)
    Y = Yr/n_sample
    Y = Y[:n_sample//2]
    if idraw ==1 :
        plt.loglog(frq, 2*abs(Y)/np.sqrt(frq_bin),'r')

    return frq, 2*abs(Y)/np.sqrt(frq_bin), Yr


sampling_rate = 100e+3
sampling_interval = 1./sampling_rate
idraw = 0
start_time = time.time()


with h5py.File(in_file.format(nnum), 'r') as f :
    print ("Keys : %s" % f.keys())
    print (list(f.keys()))

    dset = f['rawdata']
    print (dset.dtype)
    print (dset.shape)

    print ("Data(1) or FFT(2)???")
    Or = input()
    Or = int(Or)

    if (Or == 1) : 

        print ("what is the AMOREADC ID ???? (1~3)")
        nmod = input()
        nmod = int(nmod)
            
        print ("how many channel would you like to draw ?????? (max.16)")
        nch = input()
        nch = int(nch)
        
        for i in range(int(dset.shape[0])) : 
            
            fig, ax = plt.subplots(nch, figsize=(6, 2*nch))
            plt.ion()
            data1 = np.array(dset[i][2])
            time = np.array(dset[i][1]/8.)
            print (time)
            for j in range(nmod-1, nmod) :
                for k in range(nch) :
                    n1 = np.array(data1[j][k])
                    ax[k].plot(n1*10/2**18*1000, linewidth=0.3, label='{0}'.format(len(n1)))
                    ax[k].set_title("channel : {0}".format(k))
                    ax[k].set_ylabel('mV')
                    ax[k].legend()
                    ax[k].grid()
            plt.subplots_adjust(hspace=0.5, wspace=0.5)
            plt.show()
            
            print ("n // q??")
            isnq = input()
            
            if (isnq == "q") : break
            
            else : 
                plt.close()

    elif (Or == 2) :    
        
        print ("what is the AMOREADC ID ???? (1~3)")
        nmod = input()
        nmod = int(nmod)
        
        print ("how many channel would you like to draw ?????? (max.16)")
        nch = input()
        nch = int(nch)

        tot_evt = int(dset.shape[0]//2)*2
        tot_data = [[0]*32768 for _ in range(nch)]
    
        for i in range(1, int(tot_evt), 2) :
            if (i==1 or i%101==0 ): 
                mtime = time.time()
                print ("process : ....", i , ", time : {0}".format(mtime-start_time))
            data1 = np.array(dset[i-1][2])
            data2 = np.array(dset[i][2])
            for j in range(nmod-1, nmod) :
                for k in range(nch) :
                    n1 = np.array(data1[j][k])
                    n2 = np.array(data2[j][k])
                    Add_data = np.hstack((n1,n2))
                    a, b, c = fft(Add_data, sampling_rate, sampling_interval, idraw)
                    tot_data[k] +=  b*10./2**18
    
        for i in range(nch) :
            tot_data[i] = np.array(tot_data[i])/int(dset.shape[0]//2)
    
        fig, ax = plt.subplots(1, nch, figsize=(5*nch,3))
        for i in range(nch) : 
            ax[i].plot(a, tot_data[i], linewidth=0.7, label='{0}'.format(int(dset.shape[0]//2)))
            ax[i].set_xscale('log')
            ax[i].set_yscale('log')
            ax[i].set_ylim(np.min(tot_data[i]), np.max(tot_data[i][1:])*2)
            ax[i].set_title("channel : {0}".format(i))
            ax[i].set_ylabel(r'$\frac{V}{\sqrt{Hz}}$')
            ax[i].tick_params(axis='both', which='major', labelsize=6)
            ax[i].legend()
            ax[i].grid()

        plt.subplots_adjust(wspace=0.4, hspace=0.3)
        plt.show()

